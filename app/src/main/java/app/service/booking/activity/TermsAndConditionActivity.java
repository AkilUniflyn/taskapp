package app.service.booking.activity;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import app.service.booking.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;

public class TermsAndConditionActivity extends AppCompatActivity {


    @BindView(R.id.txt_titles)
    TextView txtTitle;
    @BindView(R.id.btn_back)
    Button btnback;
    @BindView(R.id.webview)
    WebView webview;
    String languageid;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tc);
        ButterKnife.bind(this);
        txtTitle.setText("Terms and Conditions");
        webview.getSettings().setJavaScriptEnabled(true);

        SharedPreferences prefs = getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        languageid = prefs.getString("languageid", "1");//"No name defined" is the default value.
        Log.e("tclanguid",languageid);


        String pdf = "http://mobwebapps.com/Taskapp/index.php/api/TermsPrivacy?language="+languageid+"&type=1";
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.e("i am in", "onPageStarted");
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.e("i am in", "onPageFinished");

            }
        });
        webview.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + pdf);

    }

    @OnClick({R.id.btn_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                onBackPressed();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
