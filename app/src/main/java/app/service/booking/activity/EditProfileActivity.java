package app.service.booking.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AddressComponent;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.model.api.ImageUploadResponse;
import app.service.booking.model.api.SocialSignupLoginResponse;
import app.service.booking.utils.GlobalMethods;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;

public class EditProfileActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    @BindView(R.id.img_camera)
    ImageView imgCamera;
    @BindView(R.id.edt_firstname)
    EditText edtFirstname;
    @BindView(R.id.edt_lastname)
    EditText edtLastname;
    @BindView(R.id.edt_emailaddress)
    EditText edtEmailaddress;
    @BindView(R.id.edt_mobileno)
    EditText edtMobileno;

    @BindView(R.id.btn_update)
    Button btnUpdate;
    Dialog camera_dialog;
    Context context;
    TextView txt_gallery, txt_take_photo, txt_photo;
    Button btn_cancel;
    String path;
    Uri yourUri;
    @BindView(R.id.places_autocomplete)
    PlacesAutocompleteTextView placesAutocomplete;
    @BindView(R.id.checkbox_earnmoney)
    CheckBox checkboxEarnmoney;
    @BindView(R.id.checkbox_workdone)
    CheckBox checkboxWorkdone;
    ProgressDialog progressDialog;
    /* @BindView(R.id.checkbox_earnmoney)
     ImageView checkboxEarnmoney;
     @BindView(R.id.checkbox_workdone)
     ImageView checkboxWorkdone;*/
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Uri uri;
    private Bitmap bitmap;

    int check_status = 0;
    int check_box = 0;
    String userid,token, location, latitude, longitude, fsname, lastname, email, mobilnumber, earmoney = "0", workdone = "0", imageurl,imagename,languageid;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    ApiService apiService;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        context = EditProfileActivity.this;
        txtTitle.setText("Edit Profile");
        apiService = RetrofitSingleton.creaservice(ApiService.class);
        setBookingLocation();
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            callMultiplePermissions();
        } else {
            // Pre-Marshmallow
        }
        try {
            token = FirebaseInstanceId.getInstance().getToken();
            Log.e("Tokenlogin", token + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        SharedPreferences prefs = getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        languageid = prefs.getString("languageid", "1");//"No name defined" is the default value.
        Log.e("signuplanguid",languageid);
        userid = PrefConnect.readString(EditProfileActivity.this,PrefConnect.USERID,"");
        try {
            fsname = PrefConnect.readString(EditProfileActivity.this, PrefConnect.FIRSTNAME, "");
            lastname = PrefConnect.readString(EditProfileActivity.this, PrefConnect.LASTNAME, "");
            email = PrefConnect.readString(EditProfileActivity.this, PrefConnect.EMAIL, "");
            location = PrefConnect.readString(EditProfileActivity.this, PrefConnect.LOCATION, "");
            mobilnumber = PrefConnect.readString(EditProfileActivity.this, PrefConnect.PHONENO, "");

            earmoney = PrefConnect.readString(EditProfileActivity.this, PrefConnect.EARNMONEY, "");
            workdone = PrefConnect.readString(EditProfileActivity.this, PrefConnect.WORKDONE, "");
            imagename = PrefConnect.readString(EditProfileActivity.this, PrefConnect.PROFILEIMAGE, "");

            edtFirstname.setText(fsname);
            edtLastname.setText(lastname);
            edtEmailaddress.setText(email);
            edtMobileno.setText(mobilnumber);
            placesAutocomplete.setText(location);

            Log.e("checkmoney",earmoney+"::"+workdone);
            
            
            
            if (earmoney.equalsIgnoreCase("1")) {

                checkboxEarnmoney.setChecked(true);
            }else {
                checkboxEarnmoney.setChecked(false);
            }

            if(workdone.equalsIgnoreCase("1")){

                checkboxWorkdone.setChecked(true);

            }else {

                checkboxWorkdone.setChecked(false);
            }
            
            checkboxWorkdone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        workdone ="1";
                    }else {
                        workdone = "0";
                    }
                }
            });
            
            checkboxEarnmoney.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        earmoney ="1";
                    }else {
                        earmoney = "0";
                    } 
                }
            });
            if(!TextUtils.isEmpty(imagename)){
                Glide.with(EditProfileActivity.this).load(imagename).into(imgProfile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setBookingLocation() {
        placesAutocomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("TAG", "Focus EDIT TEXT");
                placesAutocomplete.setFocusableInTouchMode(true);
                placesAutocomplete.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(placesAutocomplete, InputMethodManager.SHOW_IMPLICIT);
            }
        });
        placesAutocomplete.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(final Place place) {
                placesAutocomplete.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(final PlaceDetails details) {
                        Log.e("test", "details " + details.address_components);
                        //booking_location = details.address_components + "";
                        String address = "";
                        // Log.d("test", "details " + booking_location);
                        for (AddressComponent component : details.address_components) {
                            if (address.equals("")) {
                                address = component.long_name;
                            } else {
                                address += " ," + component.long_name;
                            }
                        }

                        location = address;
                        Geocoder coder = new Geocoder(EditProfileActivity.this);
                        try {
                            ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(location, 50);
                            for (Address add : adresses) {
                                if (add != null) {//Controls to ensure it is right address such as country etc.
                                    double longitude = add.getLongitude();
                                    double latitude = add.getLatitude();

                                    Log.e("latlong", latitude + ":::" + longitude);
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                        latitude = details.geometry.location.lat + "";
                        longitude = details.geometry.location.lng + "";

                        Log.e("testjjju", address + "::" + latitude);
                        // details.formatted_address.
                        View view = getCurrentFocus();
                        //If no view currently has focus, create a new one, just so we can grab a window token from it
                        if (view == null) {
                            view = new View(EditProfileActivity.this);
                        }
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    @Override
                    public void onFailure(final Throwable failure) {
                        Log.d("test", "failure " + failure);
                    }
                });
            }
        });


    }

    private void callMultiplePermissions() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("NETWORK STATE");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("WRITE EXTERNAL STORAGE");
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("READ EXTERNAL STORAGE");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add(" CAMERA");


        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                } else {
                    // Pre-Marshmallow
                }

                return;
            }
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            } else {
                // Pre-Marshmallow
            }

            return;
        }

    }

    /**
     * add Permissions
     *
     * @param permissionsList
     * @param permission
     * @return
     */
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        } else {
            // Pre-Marshmallow
        }

        return true;
    }

    /**
     * Permissions results
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                // Initial
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION and others

              /*  perms.get(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        &&*/

                if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted

                } else {
                    // Permission Denied
                    Toast.makeText(EditProfileActivity.this, "Permission is Denied", Toast.LENGTH_SHORT)
                            .show();

                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @OnClick({R.id.img_back, R.id.img_camera, R.id.btn_update})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.img_camera:
                cameradialog();
                break;
            case R.id.btn_update:
                callValidation();
               // finish();
                break;

        }
    }

    private void callValidation() {
        fsname = edtFirstname.getText().toString();
        lastname = edtLastname.getText().toString();
        email = edtEmailaddress.getText().toString();
        mobilnumber = edtMobileno.getText().toString();

        boolean cancel = false;
        if(TextUtils.isEmpty(imagename)){
            cancel = true;
            GlobalMethods.Toast(EditProfileActivity.this,"Profile image is empty");
        }else if(TextUtils.isEmpty(fsname)){
            cancel = true;

            GlobalMethods.Toast(EditProfileActivity.this,"First Name is empty");
        }else if(TextUtils.isEmpty(lastname)){
            cancel = true;

            GlobalMethods.Toast(EditProfileActivity.this,"First Name is empty");
        }else if(TextUtils.isEmpty(email)){
            cancel = true;

            GlobalMethods.Toast(EditProfileActivity.this,"Email is empty");
        }else if(!GlobalMethods.validateEmail(email)){
            cancel = true;

            GlobalMethods.Toast(EditProfileActivity.this,"Email is invalid");
        }else if(TextUtils.isEmpty(email)){
            cancel = true;

            GlobalMethods.Toast(EditProfileActivity.this,"Mobile Number is empty");
        }else if(TextUtils.isEmpty(location)){
            cancel = true;

            GlobalMethods.Toast(EditProfileActivity.this,"Location is empty");
        }
        if(!cancel){

            if(GlobalMethods.isNetworkAvailable(EditProfileActivity.this)){

                callUpdate();
            }else {
                GlobalMethods.Toast(EditProfileActivity.this,getString(R.string.internet));
            }
        }
    }

    private void callUpdate() {
        Log.e("Requestupdate",fsname+"::"+lastname+"::"+email+"::"+mobilnumber+"::"+location+"::"+latitude+":;"+longitude+"::"+imagename+"::");
        Log.e("New",earmoney+"::"+workdone+"::"+token+"::"+languageid);
        progressDialog = ProgressDialog.show(EditProfileActivity.this,"","Loading...",false);
        Call<SocialSignupLoginResponse> call = apiService.callProfileUpdate(userid,fsname,lastname,email,mobilnumber,imagename,location,latitude,longitude,earmoney,workdone,"1",token,languageid);
        call.enqueue(new Callback<SocialSignupLoginResponse>() {
            @Override
            public void onResponse(Call<SocialSignupLoginResponse> call, Response<SocialSignupLoginResponse> response) {
                Log.e("successRespo",new Gson().toJson(response.body()));
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    SocialSignupLoginResponse resp = response.body();
                    if(resp!=null){
                        String status = resp.getStatus();
                        if(status.equalsIgnoreCase("1")){
                            String name = resp.getData().getFirstName() +" "+resp.getData().getLastName();

                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.USERID,resp.getData().getId());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.USERNAME,name);
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.FIRSTNAME,resp.getData().getFirstName());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.LASTNAME,resp.getData().getLastName());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.EMAIL,resp.getData().getEmail());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.LOCATION,resp.getData().getLocation());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.LATUSER,resp.getData().getLat());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.LONGUSER,resp.getData().getLon());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.PHONENO,resp.getData().getMobileNumber());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.JOINEDDATE,resp.getData().getJoined());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.RATING,resp.getData().getRating());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.REGION,resp.getData().getRegion());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.EARNMONEY,resp.getData().getEarn());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.WORKDONE,resp.getData().getWork());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.PROFILEIMAGE,resp.getData().getProfie());
                            PrefConnect.writeString(EditProfileActivity.this,PrefConnect.PROFILESTATUS,resp.getData().getProfileStatus());

                            finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SocialSignupLoginResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
        

    }

    private void cameradialog() {
        camera_dialog = new Dialog(context);
        camera_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        camera_dialog.setContentView(R.layout.dialog_photo);
        camera_dialog.setCancelable(false);
        camera_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        camera_dialog.show();
        txt_gallery = (TextView) camera_dialog.findViewById(R.id.txt_gallery);
        txt_take_photo = (TextView) camera_dialog.findViewById(R.id.txt_take_photo);
        btn_cancel = (Button) camera_dialog.findViewById(R.id.btn_cancel);


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera_dialog.dismiss();

            }
        });

        txt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera_dialog.dismiss();
                galleryIntent();
            }
        });
        txt_take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera_dialog.dismiss();
                cameraIntent();
            }
        });

    }

    private void cameraIntent() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void galleryIntent() {
        try {

            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, SELECT_FILE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE) {
                    uri = data.getData();
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        //imgProfile.setImageBitmap(bitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Uri uri = GlobalMethods.getImageUri(getApplicationContext(), bitmap);
                    String filegallery = GlobalMethods.getRealPathFromURIPath(uri, EditProfileActivity.this);
                    Log.e("filePath", filegallery);
                    File file = new File(filegallery);
                    callFileUpload(file);
//                    onSelectFromGalleryResult(data);
                } else if (requestCode == REQUEST_CAMERA) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    imgProfile.setImageBitmap(photo);
                    uri = getImageUri(getApplicationContext(), photo);
                    Log.e("URI", uri.toString());
                    Uri uri = GlobalMethods.getImageUri(getApplicationContext(), photo);
                    String filepath = GlobalMethods.getRealPathFromURI(EditProfileActivity.this, uri);
                    File file2 = new File(filepath);
                    callFileUpload(file2);
                   // onCaptureImageResult(data);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void callFileUpload(File file) {

        progressDialog = ProgressDialog.show(EditProfileActivity.this, "", "Loading", true);
        String strContent = "1";
        String strCProfile = "1";


        Map<String, RequestBody> map = null;
        try {
            RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
            RequestBody rbProfile = RequestBody.create(MediaType.parse("text/plain"), strCProfile);
            RequestBody rbContent = RequestBody.create(MediaType.parse("text/plain"), strContent);
            RequestBody rbLanu = RequestBody.create(MediaType.parse("text/plain"), languageid);

            map = new HashMap<>();
            map.put("file_name\"; filename=\"" + file.getName(), mFile);
            map.put("profile", rbProfile);
            map.put("content", rbContent);
            map.put("language", rbLanu);


        } catch (Exception e) {
            e.printStackTrace();
        }
        Call<ImageUploadResponse> call = apiService.callImageUpload(map);
        call.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                Log.e("SignupimagRes", new Gson().toJson(response.body()));
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    ImageUploadResponse resp = response.body();
                    if (resp != null) {
                        String status = resp.getStatus();
                        if (status.equalsIgnoreCase("1")) {
                            imageurl = resp.getFileUrl();
                            imagename = resp.getFileName();


                            Glide.with(EditProfileActivity.this).load(imageurl).into(imgProfile);
                            GlobalMethods.Toast(EditProfileActivity.this, "Image Uploaded Success");

                        } else {
                            GlobalMethods.Toast(EditProfileActivity.this, resp.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("imageFail", t.getMessage());
            }
        });
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void onCaptureImageResult(Intent data) {
         /* uri = data.getData();
                    bitmap = (Bitmap) data.getExtras().get("data");*/
        Bitmap thumbnail = null;
        ByteArrayOutputStream bytes = null;
        File destination = null;
        FileOutputStream fo;
        try {
            thumbnail = (Bitmap) data.getExtras().get("data");
            bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");
            System.out.println("path capture" + destination.toString());

            Log.e("TAG", "Getting PAth" + destination.toString());

            yourUri = Uri.fromFile(destination);
            path = yourUri.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // imgLicense.setImageBitmap(thumbnail);
        try {
            sendThumbnail(thumbnail);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void sendThumbnail(Bitmap thumbnail) {
        try {


            String path = MediaStore.Images.Media.insertImage(getContentResolver(), thumbnail, "Title", null);
            Uri imageUri = Uri.parse(path);
            if (GlobalMethods.isNetworkAvailable(context)) {
            } else {
                GlobalMethods.Toast(context, getString(R.string.internet));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //  imgLicense.setImageBitmap(bm);
        try {
            sendBitmap(bm);
            Uri selectedImageURI = data.getData();
            path = selectedImageURI.toString();
            System.out.println("test gallery path : " + path);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void sendBitmap(Bitmap bm) {
        try {

            String path = MediaStore.Images.Media.insertImage(getContentResolver(), bm, "Title", null);
            Uri imageUri = Uri.parse(path);
            if (GlobalMethods.isNetworkAvailable(context)) {
            } else {
                GlobalMethods.Toast(context, getString(R.string.internet));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
