package app.service.booking.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.adapter.RegionAdapter;

import app.service.booking.model.RegionModel;
import app.service.booking.model.api.RegionlistResponse;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;

public class RegionActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.recycler_region)
    RecyclerView recyclerRegion;

    List<RegionModel> region_list;
    List<RegionlistResponse.Datum> regionlist;
    RegionAdapter adapter;
    Context context;
    ApiService apiService;
    String language;
    RegionAdapter.ItemClickListener itemClickListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_region);
        ButterKnife.bind(this);
        context=RegionActivity.this;
        apiService = RetrofitSingleton.creaservice(ApiService.class);

        SharedPreferences prefs = getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        language = prefs.getString("languageid", "0");//"No name defined" is the default value.
        Log.e("loginlanguid",language);
        regionlist();

        itemClickListener=new RegionAdapter.ItemClickListener() {
            @Override
            public void itemClick(String id) {

                Intent forgot = new Intent(context, SignUpActivity.class);
                forgot.putExtra("region",id);
                context.startActivity(forgot);
                finish();
            }
        };

        /*region_list=new ArrayList<>();
        region_list.add(new RegionModel(R.drawable.oman,"Oman"));
        region_list.add(new RegionModel(R.drawable.saudi_arabia,"Saudi"));
        region_list.add(new RegionModel(R.drawable.kuwait,"Kuwait"));
        region_list.add(new RegionModel(R.drawable.bahrain,"Bahrain"));
        region_list.add(new RegionModel(R.drawable.uae,"UAE"));*/


    }

    @OnClick({R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }

    public void regionlist() {
        regionlist=new ArrayList<>();
        Log.e("Language",language);
        Call<RegionlistResponse> call = apiService.callRegionlistAPI(language);
        call.enqueue(new Callback<RegionlistResponse>() {
            @Override
            public void onResponse(Call<RegionlistResponse> call, Response<RegionlistResponse> response) {
                Log.e("Region Responce", new Gson().toJson(response.body()));

                RegionlistResponse resp = response.body();
                String status = resp.getStatus();
                if (status.equals("1")) {
                    regionlist = resp.getData();
                   /* for (int i = 0; i < languagelistResponseList.size(); i++) {
                        languagelistResponseList.get(i).setStatus("0");
                    }
*/
                    adapter=new RegionAdapter(context,regionlist,itemClickListener);
                    recyclerRegion.setAdapter(adapter);
                    recyclerRegion.setLayoutManager(new LinearLayoutManager(context));
                }
            }

            @Override
            public void onFailure(Call<RegionlistResponse> call, Throwable t) {

            }
        });
    }


}
