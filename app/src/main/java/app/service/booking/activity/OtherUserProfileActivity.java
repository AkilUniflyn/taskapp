package app.service.booking.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.adapter.MoreRewiesAdapter;
import app.service.booking.model.ReviewsModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OtherUserProfileActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.btn_back)
    Button btnBack;
    @BindView(R.id.txt_titles)
    TextView txtTitles;
    @BindView(R.id.recycleREviews)
    RecyclerView recycleREviews;
    @BindView(R.id.linearmorereviews)
    LinearLayout linearmorereviews;
    List<ReviewsModel> reviewsModels;
    MoreRewiesAdapter moreRewiesAdapter;
    @BindView(R.id.txt_as_tasker)
    TextView txtAsTasker;
    @BindView(R.id.linear_as_tassker)
    LinearLayout linearAsTassker;
    @BindView(R.id.txt_as_poster)
    TextView txtAsPoster;
    @BindView(R.id.linear_as_poster)
    LinearLayout linearAsPoster;
    @BindView(R.id.img_message)
    ImageView img_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_user_profile);
        ButterKnife.bind(this);
        reviewsModels = new ArrayList<>();
        reviewsModels.add(new ReviewsModel("James Johnson", "20 mins ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry."));
        reviewsModels.add(new ReviewsModel("James Johnson", "20 mins ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry."));
        moreRewiesAdapter = new MoreRewiesAdapter(reviewsModels, OtherUserProfileActivity.this);
        recycleREviews.setAdapter(moreRewiesAdapter);
        recycleREviews.setLayoutManager(new LinearLayoutManager(OtherUserProfileActivity.this));
    }

    @OnClick({R.id.btn_back, R.id.linearmorereviews,R.id.linear_as_tassker,R.id.linear_as_poster,R.id.img_message})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.linearmorereviews:
                Intent intent = new Intent(OtherUserProfileActivity.this, MoreReviewActivity.class);
                startActivity(intent);
                break;

            case R.id.linear_as_tassker:
                linearAsTassker.setBackground(getResources().getDrawable(R.drawable.selcted_blue_fill_left));
                linearAsPoster.setBackground(getResources().getDrawable(R.drawable.unselcted_blue_line_right));
                txtAsTasker.setTextColor(getResources().getColor(R.color.colorWhite));
                txtAsPoster.setTextColor(getResources().getColor(R.color.colorBlack));
                break;
            case R.id.linear_as_poster:
                linearAsTassker.setBackground(getResources().getDrawable(R.drawable.unselcted_blue_line_left));
                linearAsPoster.setBackground(getResources().getDrawable(R.drawable.selcted_blue_fill_right));
                txtAsTasker.setTextColor(getResources().getColor(R.color.colorBlack));
                txtAsPoster.setTextColor(getResources().getColor(R.color.colorWhite));
                break;
            case R.id.img_message:
                Intent next=new Intent(OtherUserProfileActivity.this,MessageActivity.class);
                startActivity(next);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
