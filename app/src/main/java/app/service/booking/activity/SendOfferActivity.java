package app.service.booking.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.adapter.AttachmentAdapter;
import app.service.booking.adapter.ImageAdapter;
import app.service.booking.adapter.OfferAdapter;
import app.service.booking.model.ImageModel;
import app.service.booking.model.OfferModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SendOfferActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
   /* @BindView(R.id.viewPager)
    ViewPager viewPager;*/
    @BindView(R.id.txt_send_offer)
    TextView txtSendOffer;
    @BindView(R.id.txt_username)
    TextView txtUsername;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.txt_location)
    TextView txtLocation;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_any)
    TextView txtAny;
    @BindView(R.id.txt_requirement)
    TextView txtRequirement;
    /*@BindView(R.id.txt_req2)
    TextView txtReq2;
    @BindView(R.id.txt_req3)
    TextView txtReq3;*/
    @BindView(R.id.recycler_home)
    RecyclerView recyclerHome;
    @BindView(R.id.recycler_offer)
    RecyclerView recyclerOffer;
    @BindView(R.id.edt_question)
    EditText edtQuestion;
    @BindView(R.id.btn_send)
    Button btnSend;
    Context context;
    List<OfferModel> offer_list;
    OfferAdapter offerAdapter;
    @BindView(R.id.prfImage)
    ImageView prfImage;
    List<ImageModel> image_list;
    AttachmentAdapter adapter;
    Dialog senddialog;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_offer);
        ButterKnife.bind(this);
        context=SendOfferActivity.this;
        txtTitle.setText("Task Details");
        ImageAdapter adapterView = new ImageAdapter(this);
        //viewPager.setAdapter(adapterView);
        image_list = new ArrayList<>();
        image_list.add(new ImageModel(R.drawable.seat_belts));
        image_list.add(new ImageModel(R.drawable.seat_belts));
        image_list.add(new ImageModel(R.drawable.seat_belts));

        adapter = new AttachmentAdapter(context, image_list);
        recyclerHome.setAdapter(adapter);
        recyclerHome.setLayoutManager(new GridLayoutManager(SendOfferActivity.this, 1, LinearLayout.HORIZONTAL, false));
        offer_list = new ArrayList<>();
        offer_list.add(new OfferModel(R.drawable.placeholder_profile, "James Johnson", "20 mins ago", "Need front and rear seat belts installed into my baby HSV Grange."));
        offer_list.add(new OfferModel(R.drawable.placeholder_profile, "James Johnson", "20 mins ago", "Need front and rear seat belts installed into my baby HSV Grange."));
        offerAdapter = new OfferAdapter(context, offer_list);
        recyclerOffer.setAdapter(offerAdapter);
        recyclerOffer.setLayoutManager(new LinearLayoutManager(context));
    }

    @OnClick({R.id.prfImage,R.id.img_back, R.id.btn_send,R.id.txt_send_offer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_send:
                break;

            case R.id.txt_send_offer:
                sendofferdialog();
                break;
            case R.id.prfImage:
                Intent intent = new Intent(SendOfferActivity.this,OtherUserProfileActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void sendofferdialog() {
        senddialog = new Dialog(context);
        senddialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        senddialog.setContentView(R.layout.dialog_send_offer);
        senddialog.setCancelable(true);
        senddialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button send=(Button)senddialog.findViewById(R.id.btn_send_offer);
        senddialog.show();


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                senddialog.dismiss();
            }
        });

    }
}
