package app.service.booking.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import app.service.booking.MainActivity;
import app.service.booking.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PostTaskStep3 extends AppCompatActivity {

    Dialog dialog_done;
    Context context;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.btn_done)
    Button btnDone;
    @BindView(R.id.img_total)
    ImageView imgTotal;
    @BindView(R.id.img_hourly_rate)
    ImageView imgHourlyRate;
    @BindView(R.id.edt_price)
    EditText edtPrice;

    @BindView(R.id.linear_hourly_rate)
    LinearLayout linear_hourly_rate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_task_step3);
        ButterKnife.bind(this);
        context = PostTaskStep3.this;
        imgHourlyRate.setImageResource(R.drawable.empty_circle);

        /*edtPrice.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/

        setDoneDialog();
    }

    @OnClick({R.id.img_back, R.id.btn_done, R.id.img_total, R.id.img_hourly_rate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_done:
                final Dialog feedbackdialog = new Dialog(context);
                feedbackdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                feedbackdialog.setContentView(R.layout.dialog_add_cart);
                feedbackdialog.setCancelable(false);
                feedbackdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Button submit = (Button) feedbackdialog.findViewById(R.id.btn_ok);
                feedbackdialog.show();
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        feedbackdialog.dismiss();
                        Intent intent = new Intent(PostTaskStep3.this,AddCardActivity.class);
                        intent.putExtra("page","0");
                        startActivity(intent);
                    }
                });

               /* dialog_done.show();*/
                break;
            case R.id.img_total:
                imgTotal.setImageResource(R.drawable.blue_circle);
                imgHourlyRate.setImageResource(R.drawable.empty_circle);
                edtPrice.setVisibility(View.VISIBLE);
                linear_hourly_rate.setVisibility(View.GONE);
                break;

            case R.id.img_hourly_rate:
                imgTotal.setImageResource(R.drawable.empty_circle);
                imgHourlyRate.setImageResource(R.drawable.blue_circle);
                edtPrice.setVisibility(View.GONE);
                linear_hourly_rate.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setDoneDialog() {

        dialog_done = new Dialog(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);
        dialog_done.setContentView(R.layout.dialog_done);
        dialog_done.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_done.setCancelable(false);
        Button btn_view_ur_task = (Button) dialog_done.findViewById(R.id.btn_view_ur_task);
        btn_view_ur_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_done.dismiss();

                Intent verification = new Intent(context, MainActivity.class);
                verification.putExtra("page","3");
                verification.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(verification);
                finish();
            }
        });


    }

}
