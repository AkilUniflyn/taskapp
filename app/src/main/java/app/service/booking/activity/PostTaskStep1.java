package app.service.booking.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.adapter.MustHaveAdapter;
import app.service.booking.adapter.MustHavesAdapter;
import app.service.booking.adapter.NewImageAddAdapter;
import app.service.booking.model.ImageAddModel;
import app.service.booking.model.MustHavesModel;
import app.service.booking.model.api.ImageUploadResponse;
import app.service.booking.utils.GlobalMethods;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostTaskStep1 extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.img_one)
    ImageView imgOne;
    @BindView(R.id.plus_one)
    ImageView plusOne;
    @BindView(R.id.img_two)
    ImageView imgTwo;
    @BindView(R.id.plus_two)
    ImageView plusTwo;
    @BindView(R.id.img_three)
    ImageView imgThree;
    @BindView(R.id.plus_three)
    ImageView plusThree;
    @BindView(R.id.btn_continue)
    Button btnContinue;
    Context context;
    Dialog camera_dialog;
    TextView txt_gallery, txt_take_photo, txt_image,txt_video,txt_audio,txt_document;
    Button btn_cancel;
    String path;
    Uri yourUri;
    @BindView(R.id.relativeOne)
    RelativeLayout relativeOne;
    @BindView(R.id.relativeTwo)
    RelativeLayout relativeTwo;
    @BindView(R.id.relativeThree)
    RelativeLayout relativeThree;
    @BindView(R.id.txt_must_have)
    TextView txtMustHave;
    @BindView(R.id.must_have_layout)
    LinearLayout mustHaveLayout;
    @BindView(R.id.recycleAddmust)
    RecyclerView recycleAddmust;
    @BindView(R.id.linearRecyclemusthave)
    LinearLayout linearRecyclemusthave;
    @BindView(R.id.edt_title)
    EditText edtTitle;
    /* @BindView(R.id.edt_abt_task)
     EditText edtAbtTask;*/
    @BindView(R.id.edt_descripe)
    EditText edtDescripe;
    @BindView(R.id.recycleImage)
    RecyclerView recycleImage;

    @BindView(R.id.horizantalSCroll)
    HorizontalScrollView horizantalSCroll;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1,PICKFILE_RESULT_CODE=4;
    private Uri uri;
    private Bitmap bitmap;
    int status;
    Dialog musthavedialog;
    MustHaveAdapter mustHaveAdapter;
    ArrayList<String> stringArrayList;
    List<ImageAddModel> imageAddModelList;
    NewImageAddAdapter newImageAddAdapter;

    ImageView close;
    EditText edtcomment;
    Button btnsubmit, btn_add;
    RecyclerView recycler_must_haves;

    List<MustHavesModel> must_haves_list;
    MustHavesAdapter mustHavesAdapter;
    MustHavesAdapter.ItemClickListener itemClickListener;
    ApiService apiService;
    ProgressDialog progressDialog;
    String medianame,languageid ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_task_step1);
        ButterKnife.bind(this);
        context = PostTaskStep1.this;
        apiService = RetrofitSingleton.creaservice(ApiService.class);
        stringArrayList = new ArrayList<>();
        imageAddModelList = new ArrayList<>();
        must_haves_list = new ArrayList<>();
        musthavedialog();

        itemClickListener = new MustHavesAdapter.ItemClickListener() {
            @Override
            public void itemClick() {

                Log.e("ITEM CLICK",must_haves_list.size()+" "+edtcomment.isEnabled());

                if (must_haves_list.size()<3){
                    if (edtcomment.isEnabled()){

                    }else {
                        edtcomment.setEnabled(true);
                        btn_add.setClickable(true);
                    }
                }
            }
        };
       /* edtTitle.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/
        /*edtAbtTask.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/
        /*edtDescripe.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/
    }

    @OnClick({R.id.img_back, R.id.plus_one, R.id.plus_two, R.id.plus_three, R.id.btn_continue, R.id.relativeOne, R.id.must_have_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.plus_one:
                Log.e("imgone", "yes");
                status = 1;
                cameradialog();
                break;
            case R.id.plus_two:
                status = 2;
                cameradialog();
                break;
            case R.id.plus_three:
                status = 3;
                cameradialog();
                break;
            case R.id.btn_continue:
                Intent new_task = new Intent(context, PostTaskStep2.class);
                startActivity(new_task);
                break;
            case R.id.relativeOne:
                Log.e("imgone", "yes");
                status = 1;
                cameradialog();
                break;
            case R.id.must_have_layout:
                musthavedialog.show();
                break;
        }
    }

    private void musthavedialog() {

        musthavedialog = new Dialog(context);
        musthavedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        musthavedialog.setContentView(R.layout.dialog_must_haves);
        musthavedialog.setCancelable(false);
        musthavedialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        close = musthavedialog.findViewById(R.id.img_close);
        edtcomment = (EditText) musthavedialog.findViewById(R.id.edt_comment);
        btnsubmit = (Button) musthavedialog.findViewById(R.id.btn_submit);
        btn_add = (Button) musthavedialog.findViewById(R.id.btn_add);
        recycler_must_haves = (RecyclerView) musthavedialog.findViewById(R.id.recycler_must_haves);


        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String enter_value = edtcomment.getText().toString().trim();
                if (!enter_value.equals("")) {
                    must_haves_list.add(new MustHavesModel(enter_value));
                    edtcomment.setText("");
                    mustHavesAdapter = new MustHavesAdapter(context, must_haves_list, itemClickListener);
                    recycler_must_haves.setAdapter(mustHavesAdapter);
                    recycler_must_haves.setLayoutManager(new LinearLayoutManager(context));
                    if (must_haves_list.size() >= 3) {
                        btn_add.setClickable(false);
                        edtcomment.setEnabled(false);
                    }
                } else {
                    GlobalMethods.Toast(context, "Enter text");
                }
            }
        });

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                musthavedialog.dismiss();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                musthavedialog.dismiss();
            }
        });


    }

    private void cameradialog() {
        camera_dialog = new Dialog(context);
        camera_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        camera_dialog.setContentView(R.layout.dialog_media);
        camera_dialog.setCancelable(false);
        camera_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        camera_dialog.show();

        txt_image = (TextView)camera_dialog.findViewById(R.id.txt_image);
        txt_video = (TextView)camera_dialog.findViewById(R.id.txt_take_video);
        txt_audio = (TextView)camera_dialog.findViewById(R.id.txt_take_audio);
        txt_document = (TextView)camera_dialog.findViewById(R.id.txt_take_doument);
        final LinearLayout linearimagechoose = (LinearLayout)camera_dialog.findViewById(R.id.linear_image_choose);
        final LinearLayout linearVideo = (LinearLayout)camera_dialog.findViewById(R.id.video_choose);

        txt_gallery = (TextView) camera_dialog.findViewById(R.id.txt_gallery);
        txt_take_photo = (TextView) camera_dialog.findViewById(R.id.txt_take_photo);


        btn_cancel = (Button) camera_dialog.findViewById(R.id.btn_cancel);


        txt_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearimagechoose.setVisibility(View.VISIBLE);
                linearVideo.setVisibility(View.GONE);
            }
        });

        txt_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearimagechoose.setVisibility(View.GONE);
                linearVideo.setVisibility(View.VISIBLE);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera_dialog.dismiss();

            }
        });

        txt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera_dialog.dismiss();
                galleryIntent();
            }
        });
        txt_take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera_dialog.dismiss();
                cameraIntent();
            }
        });

        txt_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        txt_document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.setType("*/*");
                chooseFile = Intent.createChooser(chooseFile, "Choose a file");
                startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);

            }
        });

    }

    private void cameraIntent() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void galleryIntent() {
        try {

            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, SELECT_FILE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE) {
                    uri = data.getData();
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                        Uri uri = GlobalMethods.getImageUri(getApplicationContext(), bitmap);
                        String filegallery = GlobalMethods.getRealPathFromURIPath(uri, PostTaskStep1.this);
                        Log.e("filePath", filegallery);
                        File file = new File(filegallery);
                        callFileUpload(file);

                        //imageAddModelList.add(new ImageAddModel(bitmap, "0"));


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    onSelectFromGalleryResult(data);
                } else if (requestCode == REQUEST_CAMERA) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");

                    Uri uri = GlobalMethods.getImageUri(getApplicationContext(), photo);
                    String filepath = GlobalMethods.getRealPathFromURI(PostTaskStep1.this, uri);
                    File file2 = new File(filepath);
                    callFileUpload(file2);

                    //imageAddModelList.add(new ImageAddModel(photo, "0"));


                }
                else if(requestCode == PICKFILE_RESULT_CODE){

                    Uri returnUri = data.getData();
                    String filepath = getPath(returnUri);

                    File file2 = new File(filepath);
                    Log.e("upload",file2+"::");
                    //callFileUpload(file2);
                }

/*
                if (imageAddModelList.size() > 0) {
                    recycleImage.setVisibility(View.VISIBLE);
                    newImageAddAdapter = new NewImageAddAdapter(PostTaskStep1.this, imageAddModelList);

                    recycleImage.setAdapter(newImageAddAdapter);
                    recycleImage.setLayoutManager(new GridLayoutManager(PostTaskStep1.this, 1, LinearLayoutManager.HORIZONTAL, false));
                    recycleImage.smoothScrollToPosition(newImageAddAdapter.getItemCount() - 1);
                    horizantalSCroll.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                }*/
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getPath(Uri uri) {

        String path = null;
        String[] projection = { MediaStore.Files.FileColumns.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        if(cursor == null){
            path = uri.getPath();
        }
        else{
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(projection[0]);
            path = cursor.getString(column_index);
            cursor.close();
        }

        return ((path == null || path.isEmpty()) ? (uri.getPath()) : path);
    }
    private void callFileUpload(File file) {
        progressDialog = ProgressDialog.show(PostTaskStep1.this, "", "Loading", true);
        String strContent = "1";
        String strCProfile = "1";


        Map<String, RequestBody> map = null;
        try {
            RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
            RequestBody rbProfile = RequestBody.create(MediaType.parse("text/plain"), strCProfile);
            RequestBody rbContent = RequestBody.create(MediaType.parse("text/plain"), strContent);
            RequestBody rbLanu = RequestBody.create(MediaType.parse("text/plain"), languageid);

            map = new HashMap<>();
            map.put("file_name\"; filename=\"" + file.getName(), mFile);
            map.put("profile", rbProfile);
            map.put("content", rbContent);
            map.put("language", rbLanu);


        } catch (Exception e) {
            e.printStackTrace();
        }
        Call<ImageUploadResponse> call = apiService.callImageUpload(map);
        call.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                Log.e("SignupimagRes", new Gson().toJson(response.body()));
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    ImageUploadResponse resp = response.body();
                    if (resp != null) {
                        String status = resp.getStatus();
                        if (status.equalsIgnoreCase("1")) {
                           /* imageurl = resp.getFileUrl();
                            imagename = resp.getFileName();


                            Glide.with(PostTaskStep1.this).load(imageurl).into(imgProfile);*/
                            GlobalMethods.Toast(PostTaskStep1.this, "Image Uploaded Success");

                        } else {
                            GlobalMethods.Toast(PostTaskStep1.this, resp.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("imageFail", t.getMessage());
            }
        });


    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void onCaptureImageResult(Intent data) {
         /* uri = data.getData();
                    bitmap = (Bitmap) data.getExtras().get("data");*/
        Bitmap thumbnail = null;
        ByteArrayOutputStream bytes = null;
        File destination = null;
        FileOutputStream fo;
        try {
            thumbnail = (Bitmap) data.getExtras().get("data");
            bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");
            System.out.println("path capture" + destination.toString());

            Log.e("TAG", "Getting PAth" + destination.toString());

            yourUri = Uri.fromFile(destination);
            path = yourUri.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // imgLicense.setImageBitmap(thumbnail);
        try {
            sendThumbnail(thumbnail);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void sendThumbnail(Bitmap thumbnail) {
        try {


            String path = MediaStore.Images.Media.insertImage(getContentResolver(), thumbnail, "Title", null);
            Uri imageUri = Uri.parse(path);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //  imgLicense.setImageBitmap(bm);
        try {
            sendBitmap(bm);
            Uri selectedImageURI = data.getData();
            path = selectedImageURI.toString();
            System.out.println("test gallery path : " + path);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void sendBitmap(Bitmap bm) {
        try {

            String path = MediaStore.Images.Media.insertImage(getContentResolver(), bm, "Title", null);
            Uri imageUri = Uri.parse(path);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
