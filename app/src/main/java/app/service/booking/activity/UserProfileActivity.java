package app.service.booking.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.adapter.MoreRewiesAdapter;
import app.service.booking.model.ReviewsModel;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfileActivity extends AppCompatActivity {
    @BindView(R.id.btn_back)
    Button btnBack;
    @BindView(R.id.txt_titles)
    TextView txtTitles;
    @BindView(R.id.btn_edit)
    Button btnEdit;
    @BindView(R.id.recycleREviews)
    RecyclerView recycleREviews;
    @BindView(R.id.linearmorereviews)
    LinearLayout linearmorereviews;
    List<ReviewsModel> reviewsModels;
    MoreRewiesAdapter moreRewiesAdapter;
    @BindView(R.id.txt_as_tasker)
    TextView txtAsTasker;
    @BindView(R.id.linear_as_tassker)
    LinearLayout linearAsTassker;
    @BindView(R.id.txt_as_poster)
    TextView txtAsPoster;
    @BindView(R.id.linear_as_poster)
    LinearLayout linearAsPoster;
    @BindView(R.id.userImage)
    CircleImageView userImage;
    @BindView(R.id.txt_email)
    TextView txtEmail;
    @BindView(R.id.txt_phonno)
    TextView txtPhonno;
    @BindView(R.id.txt_location)
    TextView txtLocation;
    @BindView(R.id.txt_joined_date)
    TextView txtJoinedDate;
    @BindView(R.id.ratingbar)
    RatingBar ratingbar;
    @BindView(R.id.txt_rating)
    TextView txtRating;
    String fsname,lsname,email,location,phonno,joineddate,rating,profileStatus;
    Dialog dialogProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);


        fsname=  PrefConnect.readString(UserProfileActivity.this,PrefConnect.FIRSTNAME,"");
        lsname =  PrefConnect.readString(UserProfileActivity.this,PrefConnect.LASTNAME,"");
        email = PrefConnect.readString(UserProfileActivity.this,PrefConnect.EMAIL,"");
        location = PrefConnect.readString(UserProfileActivity.this,PrefConnect.LOCATION,"");
        phonno = PrefConnect.readString(UserProfileActivity.this,PrefConnect.PHONENO,"");
        joineddate= PrefConnect.readString(UserProfileActivity.this,PrefConnect.JOINEDDATE,"");
        rating = PrefConnect.readString(UserProfileActivity.this,PrefConnect.RATING,"");
        PrefConnect.readString(UserProfileActivity.this,PrefConnect.REGION,"");
        profileStatus = PrefConnect.readString(UserProfileActivity.this,PrefConnect.PROFILESTATUS,"");

        Log.e("Profilestatus",profileStatus);



        if(profileStatus.equalsIgnoreCase("1")) {
            try {
                txtTitles.setText(fsname + " " + lsname);
                txtEmail.setText(email);
                txtPhonno.setText("Phone: " + phonno);
                txtLocation.setText(location);
                txtJoinedDate.setText("Joined Date: " + joineddate);

                if (!TextUtils.isEmpty(rating))
                    txtRating.setText(rating + "/5");
                ratingbar.setRating(Float.parseFloat(rating));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }else {
            txtTitles.setText("Profile");
            txtEmail.setText(email);
            txtPhonno.setText("Phone Number ");
            txtLocation.setText("Location");
            txtJoinedDate.setText("");
            txtRating.setText("0/5");
            ratingbar.setRating(0);

            dialogProfile = new Dialog(UserProfileActivity.this);
            dialogProfile.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogProfile.setContentView(R.layout.dialog_profile_update);
            dialogProfile.setCancelable(false);
            dialogProfile.show();
            dialogProfile.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button btn_move = (Button)dialogProfile.findViewById(R.id.btn_edi_prf);
            ImageView imageView = (ImageView)dialogProfile.findViewById(R.id.img_cross);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogProfile.dismiss();
                }
            });
            btn_move.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(UserProfileActivity.this, EditProfileActivity.class);
                    startActivity(intent1);

                }
            });
        }


        reviewsModels = new ArrayList<>();
        reviewsModels.add(new ReviewsModel("James Johnson", "20 mins ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry."));
        reviewsModels.add(new ReviewsModel("James Johnson", "20 mins ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry."));
        moreRewiesAdapter = new MoreRewiesAdapter(reviewsModels, UserProfileActivity.this);
        recycleREviews.setAdapter(moreRewiesAdapter);
        recycleREviews.setLayoutManager(new LinearLayoutManager(UserProfileActivity.this));

    }

    @OnClick({R.id.btn_back, R.id.btn_edit, R.id.linearmorereviews, R.id.linear_as_tassker, R.id.linear_as_poster})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.btn_edit:
                Intent intent1 = new Intent(UserProfileActivity.this, EditProfileActivity.class);
                startActivity(intent1);
                break;
            case R.id.linearmorereviews:
                Intent intent = new Intent(UserProfileActivity.this, MoreReviewActivity.class);
                startActivity(intent);
                break;
            case R.id.linear_as_tassker:
                linearAsTassker.setBackground(getResources().getDrawable(R.drawable.selcted_blue_fill_left));
                linearAsPoster.setBackground(getResources().getDrawable(R.drawable.unselcted_blue_line_right));
                txtAsTasker.setTextColor(getResources().getColor(R.color.colorWhite));
                txtAsPoster.setTextColor(getResources().getColor(R.color.colorBlack));
                break;
            case R.id.linear_as_poster:
                linearAsTassker.setBackground(getResources().getDrawable(R.drawable.unselcted_blue_line_left));
                linearAsPoster.setBackground(getResources().getDrawable(R.drawable.selcted_blue_fill_right));
                txtAsTasker.setTextColor(getResources().getColor(R.color.colorBlack));
                txtAsPoster.setTextColor(getResources().getColor(R.color.colorWhite));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
