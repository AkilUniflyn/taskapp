package app.service.booking.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AddressComponent;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import java.util.Calendar;

import app.service.booking.R;
import app.service.booking.activity.MapViewActivity;
import app.service.booking.activity.PostTaskStep3;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PostTaskStep2 extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.img_person)
    ImageView imgPerson;
    @BindView(R.id.txt_person)
    TextView txtPerson;
    @BindView(R.id.linear_person)
    LinearLayout linearPerson;
    @BindView(R.id.img_check_person)
    ImageView imgCheckPerson;
    @BindView(R.id.img_online)
    ImageView imgOnline;
    @BindView(R.id.txt_online)
    TextView txtOnline;
    @BindView(R.id.linear_online)
    LinearLayout linearOnline;
    @BindView(R.id.img_check_online)
    ImageView imgCheckOnline;
    @BindView(R.id.btn_continue)
    Button btnContinue;

    Context context;
    @BindView(R.id.edt_date)
    EditText edtDate;
    Calendar calendar;
    int year, month, day, hour, minute;
    String selected_date, selected_time;
    @BindView(R.id.img_calendar)
    ImageView imgCalendar;
    @BindView(R.id.fromtime_layout)
    LinearLayout fromtimeLayout;
    @BindView(R.id.totime_layout)
    LinearLayout totimeLayout;
    @BindView(R.id.edt_fromtime)
    EditText edtFromtime;
    @BindView(R.id.edt_totime)
    EditText edtTotime;
    @BindView(R.id.img_timecheck)
    ImageView imgTimecheck;
    @BindView(R.id.timelayout)
    LinearLayout timelayout;
    int img_status = 0;
    @BindView(R.id.locationlayout)
    LinearLayout locationlayout;

    @BindView(R.id.places_autocomplete)
    PlacesAutocompleteTextView placesAutocomplete;
    String booking_location="0", latitude="0", longitude="0";
    @BindView(R.id.btn_place)
    Button btnPlace;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_task_step2);
        ButterKnife.bind(this);
        context = PostTaskStep2.this;
        calendar = Calendar.getInstance();
        setBookingLocation();
        /*edtLocation.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });
        edtDate.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/
    }

    private void setBookingLocation() {
        placesAutocomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("TAG", "Focus EDIT TEXT");
                placesAutocomplete.setFocusableInTouchMode(true);
                placesAutocomplete.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(placesAutocomplete, InputMethodManager.SHOW_IMPLICIT);
            }
        });
        placesAutocomplete.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(final Place place) {
                placesAutocomplete.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(final PlaceDetails details) {
                        Log.e("test", "details " + details.address_components);
                        //booking_location = details.address_components + "";
                        String address = "";
                        // Log.d("test", "details " + booking_location);
                        for (AddressComponent component : details.address_components) {
                            if (address.equals("")) {
                                address = component.long_name;
                            } else {
                                address += " ," + component.long_name;
                            }
                        }
                        Log.e("testjjju", address);
                        booking_location = address;
                        latitude = details.geometry.location.lat + "";
                        longitude = details.geometry.location.lng + "";
                        // details.formatted_address.
                        View view = getCurrentFocus();
                        //If no view currently has focus, create a new one, just so we can grab a window token from it
                        if (view == null) {
                            view = new View(PostTaskStep2.this);
                        }
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    @Override
                    public void onFailure(final Throwable failure) {
                        Log.d("test", "failure " + failure);
                    }
                });
            }
        });


    }

    @OnClick({R.id.btn_place,R.id.img_back, R.id.linear_person, R.id.linear_online, R.id.btn_continue, R.id.edt_date, R.id.img_calendar, R.id.fromtime_layout, R.id.totime_layout, R.id.img_timecheck})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_place:

                Intent intent = new Intent(PostTaskStep2.this, MapViewActivity.class);
                intent.putExtra("lat",latitude);
                intent.putExtra("long",longitude);
                intent.putExtra("location",booking_location);
                startActivity(intent);
                break;
            case R.id.img_back:
                finish();
                break;
            case R.id.linear_person:
                txtPerson.setTextColor(getResources().getColor(R.color.red));
                txtOnline.setTextColor(getResources().getColor(R.color.black));
                linearPerson.setBackgroundDrawable(getResources().getDrawable(R.drawable.red_stroke_min));
                linearOnline.setBackgroundDrawable(getResources().getDrawable(R.drawable.gray_stroke_min));
                imgPerson.setImageDrawable(getResources().getDrawable(R.drawable.man_red_icon));
                imgOnline.setImageDrawable(getResources().getDrawable(R.drawable.web_black_icon));
                imgCheckPerson.setVisibility(View.VISIBLE);
                imgCheckOnline.setVisibility(View.GONE);
                locationlayout.setVisibility(View.VISIBLE);
                break;
            case R.id.linear_online:
                txtPerson.setTextColor(getResources().getColor(R.color.black));
                txtOnline.setTextColor(getResources().getColor(R.color.red));
                linearPerson.setBackgroundDrawable(getResources().getDrawable(R.drawable.gray_stroke_min));
                linearOnline.setBackgroundDrawable(getResources().getDrawable(R.drawable.red_stroke_min));
                imgPerson.setImageDrawable(getResources().getDrawable(R.drawable.man_black_icon));
                imgOnline.setImageDrawable(getResources().getDrawable(R.drawable.web_red_icon));
                imgCheckPerson.setVisibility(View.GONE);
                imgCheckOnline.setVisibility(View.VISIBLE);
                locationlayout.setVisibility(View.GONE);
                break;
            case R.id.btn_continue:
                PrefConnect.clearAllPrefs(PostTaskStep2.this);
                Intent new_task = new Intent(context, PostTaskStep3.class);
                startActivity(new_task);
                break;

            case R.id.edt_date:
                break;
            case R.id.img_calendar:
                setdatepickerdialog();
                break;
            case R.id.fromtime_layout:
                settimepickerDialog();
                break;
            case R.id.totime_layout:
                settimepickerDialog1();
                break;
            case R.id.img_timecheck:
                if (img_status == 0) {
                    imgTimecheck.setImageResource(R.drawable.check_icon);
                    timelayout.setVisibility(View.VISIBLE);
                    img_status = 1;
                } else {
                    imgTimecheck.setImageResource(R.drawable.uncheck_icon);
                    timelayout.setVisibility(View.GONE);
                    img_status = 0;

                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
/*
        String address= PrefConnect.readString(PostTaskStep2.this,PrefConnect.latitudepick,"");

        Log.e("adddres",address);
        if(TextUtils.isEmpty(address)){
            placesAutocomplete.setHint("Search Location");
        }else {
            placesAutocomplete.setText(address);
        }*/
    }

    private void settimepickerDialog() {

        TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minut) {
                String am_pm = "";
                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                datetime.set(Calendar.MINUTE, minut);
                hour = hourOfDay;
                minute = minut;
                if (datetime.get(Calendar.AM_PM) == Calendar.AM)
                    am_pm = "AM";
                else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
                    am_pm = "PM";
                String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ? "12" : datetime.get(Calendar.HOUR) + "";
                edtFromtime.setText(strHrsToShow + ":" + datetime.get(Calendar.MINUTE) + " " + am_pm);

            }
        }, hour, minute, false);

        timePickerDialog.show();

    }

    private void settimepickerDialog1() {

        TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minut) {
                String am_pm = "";
                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                datetime.set(Calendar.MINUTE, minut);
                hour = hourOfDay;
                minute = minut;
                if (datetime.get(Calendar.AM_PM) == Calendar.AM)
                    am_pm = "AM";
                else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
                    am_pm = "PM";
                String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ? "12" : datetime.get(Calendar.HOUR) + "";
                edtTotime.setText(strHrsToShow + ":" + datetime.get(Calendar.MINUTE) + " " + am_pm);

            }
        }, hour, minute, false);

        timePickerDialog.show();

    }

    private void setdatepickerdialog() {
        Log.e("TAG", "INSIDE DATE DIALOG");
        DatePickerDialog dpd = new DatePickerDialog(context, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year1,
                                          int monthOfYear, int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year1);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        year = year1;
                        month = monthOfYear + 1;
                        day = dayOfMonth;

                        selected_date = day + "-" + month + "-" + year;
                        Log.e("TAG", "Selected Date:" + selected_date);

                        edtDate.setText(selected_date);

                    }
                }, year, month, day);
        dpd.getDatePicker().setMinDate(System.currentTimeMillis());
        dpd.show();

    }

}
