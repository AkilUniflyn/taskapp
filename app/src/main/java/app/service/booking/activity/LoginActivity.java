package app.service.booking.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.testfairy.TestFairy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import app.service.booking.Instagram.InstagramApp;
import app.service.booking.MainActivity;
import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.adapter.InvideFriendsAdapter;
import app.service.booking.model.api.SocialSignupLoginResponse;
import app.service.booking.utils.GlobalMethods;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.logo)
    LinearLayout logo;
    @BindView(R.id.edt_username)
    EditText edtUsername;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.facebook)
    ImageView facebook;
    @BindView(R.id.google)
    ImageView google;
    @BindView(R.id.txt_forgot)
    TextView txtForgot;
    @BindView(R.id.txt_signup)
    TextView txtSignup;
     @BindView(R.id.login_button_fb)
     LoginButton loginButtonFb;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    private CallbackManager callbackManager;
    Dialog countrydialog;
    Context context;

    ApiService apiService;
    String email,password;

    String google_name, google_email_address, google_gmail_id;
    String token, fb_your_name, fb_email_address, fb_id,languageid;
    private static final String[] PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,


    };




    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
     /*  FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();*/
        setContentView(R.layout.activity_login);
        apiService = RetrofitSingleton.creaservice(ApiService.class);
        ButterKnife.bind(this);
        TestFairy.begin(this, "SDK-CBWoABNL");
        context = LoginActivity.this;
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            callMultiplePermissions();
        } else {
            // Pre-Marshmallow
        }


        SharedPreferences prefs = getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        languageid = prefs.getString("languageid", "1");//"No name defined" is the default value.
        Log.e("loginlanguid",languageid);




        try {
            token = FirebaseInstanceId.getInstance().getToken();
            Log.e("Tokenlogin", token + "");
        } catch (Exception e) {
            e.printStackTrace();
        }


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
                .enableAutoManage(LoginActivity.this, LoginActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        new LoginActivity.Login().execute();
    }
    private void callMultiplePermissions() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
       /* if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("NETWORK STATE");
       */ if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("WRITE EXTERNAL STORAGE");
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("READ EXTERNAL STORAGE");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add(" CAMERA");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("ACCESS COARSE LOCATION");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("ACCESS FINE LOCATION");


        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                } else {
                    // Pre-Marshmallow
                }

                return;
            }
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            } else {
                // Pre-Marshmallow
            }

            return;
        }

    }

    /**
     * add Permissions
     *
     * @param permissionsList
     * @param permission
     * @return
     */
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        } else {
            // Pre-Marshmallow
        }

        return true;
    }


    public boolean hasPermissions(Context context, String... permissions) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
                Log.d("return==", "permission");
            }

        }
        return true;
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CLEAR_APP_CACHE,
                Manifest.permission.ACCESS_FINE_LOCATION}, 101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            /*case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                break;*/
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
            Log.e("result", String.valueOf(result.getStatus()));
            Log.e("result", String.valueOf(result.getSignInAccount()));

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }


    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.e("GoogleSignInResult ", String.valueOf(result));
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            google_name = acct.getDisplayName();
            google_gmail_id = acct.getId();
            google_email_address = acct.getEmail();


            Log.e("str_google_name ", google_name);
            Log.e("str_google_gmail_id ", google_gmail_id);
            Log.e("_email_address", google_email_address);

            StringTokenizer tokens = new StringTokenizer(google_name, " ");
            String fsname= tokens.nextToken();// this will contain "Fruit"
            String lsname = tokens.nextToken();
            callSocialSignup(google_email_address,fsname,lsname);

        } else {
            GlobalMethods.Toast(LoginActivity.this,"Google Syncing, Try again");
            Log.e("login failed", "google");
        }
    }

    public class Login extends AsyncTask {

        @SuppressLint("WrongThread")
        @Override
        protected Object doInBackground(Object[] objects) {
            loginButtonFb.setReadPermissions(Arrays.asList("email"));
            // FirebaseInstanceId.getInstance();
            loginButtonFb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {

                    if (AccessToken.getCurrentAccessToken() != null) {
                        RequestData();
                    }
                }

                @Override
                public void onCancel() {
                    LoginManager.getInstance().logOut();

                }

                @Override
                public void onError(FacebookException exception) {
                }
            });

            return "";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
        }
    }
    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("Login response", new Gson().toJson(response.getJSONObject()));
                        Log.e("Login response getError", new Gson().toJson(response.getError()));

                        JSONObject json = response.getJSONObject();
                        Log.e("json", String.valueOf(json));
                        try {
                            if (json != null) {

                                fb_email_address = json.getString("email");
                                fb_your_name = json.getString("name");
                                fb_id = json.getString("id");
                                String fsname = json.getString("first_name");
                                String lsname = json.getString("last_name");
                                Log.e("name", fb_your_name);
                                Log.e("email", fb_email_address);
                                Log.e("id", fb_id);

                                callSocialSignup(fb_email_address,fsname,lsname);




                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,first_name,last_name,email,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();


    }

    private void callSocialSignup(String email_address, String fsname, String lsname) {
        Log.e("ReqSocialsignup",email_address+"::"+fsname+"::"+lsname+"::"+token);

        Call<SocialSignupLoginResponse> call = apiService.SOCIAL_SIGNUP_LOGIN_RESPONSE_CALL(fsname,lsname,"",email_address,"1",token,"1","");
        call.enqueue(new Callback<SocialSignupLoginResponse>() {
            @Override
            public void onResponse(Call<SocialSignupLoginResponse> call, Response<SocialSignupLoginResponse> response) {
                Log.e("sucesssocial",new Gson().toJson(response.body()));
                if(response.isSuccessful()){
                    SocialSignupLoginResponse resp = response.body();
                    if(resp!=null){
                        String status = resp.getStatus();
                        if(status.equals("1")){
                            String name = resp.getData().getFirstName() +" "+resp.getData().getLastName();

                            PrefConnect.writeString(LoginActivity.this,PrefConnect.USERID,resp.getData().getId());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.USERNAME,name);
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.FIRSTNAME,resp.getData().getFirstName());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.LASTNAME,resp.getData().getLastName());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.EMAIL,resp.getData().getEmail());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.LOCATION,resp.getData().getLocation());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.LATUSER,resp.getData().getLat());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.LONGUSER,resp.getData().getLon());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.PHONENO,resp.getData().getMobileNumber());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.JOINEDDATE,resp.getData().getJoined());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.RATING,resp.getData().getRating());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.REGION,resp.getData().getRegion());

                            PrefConnect.writeString(LoginActivity.this,PrefConnect.EARNMONEY,resp.getData().getEarn());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.WORKDONE,resp.getData().getWork());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.PROFILEIMAGE,resp.getData().getProfie());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.PROFILESTATUS,resp.getData().getProfileStatus());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.NOTIFICATION,resp.getData().getNotification());

                            Intent login = new Intent(LoginActivity.this, MainActivity.class);
                            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(login);
                            finish();
                        }else {
                            GlobalMethods.Toast(LoginActivity.this,resp.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SocialSignupLoginResponse> call, Throwable t) {

            }
        });

    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @OnClick({R.id.btn_login, R.id.facebook, R.id.google, R.id.twitter,R.id.txt_forgot, R.id.txt_signup})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btn_login:
                callValidation();
              /*  Intent login = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(login);
                finish();*/
                break;
            case R.id.facebook:
                if (GlobalMethods.isNetworkAvailable(LoginActivity.this)) {
                    loginButtonFb.performClick();
                } else {
                    GlobalMethods.Toast(LoginActivity.this, getString(R.string.internet));
                }
              /*  Dialog dialog = new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_invide_friends);
                // camera_dialog.setCancelable(false);
                dialog.show();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                RecyclerView recyclerView = (RecyclerView)dialog.findViewById(R.id.recycleView);
                InvideFriendsAdapter invideFriendsAdapter = new InvideFriendsAdapter(LoginActivity.this);
                recyclerView.setAdapter(invideFriendsAdapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(LoginActivity.this));*/

                 loginButtonFb.performClick();
                break;
            case R.id.google:
                if (GlobalMethods.isNetworkAvailable(LoginActivity.this)) {
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                } else {
                    GlobalMethods.Toast(LoginActivity.this, getString(R.string.internet));
                }
                    /*Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);*/
                break;
            case R.id.twitter:

                break;
            case R.id.txt_forgot:
                Log.e("forgot", "password");
                Intent forgot = new Intent(LoginActivity.this, ForgotActivity.class);
                startActivity(forgot);
                break;
            case R.id.txt_signup:
                //countrydialog();
                Log.e("txt_signup", "txt_signup");
                Intent signup = new Intent(LoginActivity.this, RegionActivity.class);
                startActivity(signup);
               

                break;

        }
    }

    private void callValidation() {
        email = edtUsername.getText().toString();
        password = edtPassword.getText().toString();
        boolean cancel = false;

        if(TextUtils.isEmpty(email)){
            GlobalMethods.Toast(LoginActivity.this,"Email is Empty");
            cancel = true;
        }else if (!GlobalMethods.validateEmail(email)) {
            cancel = true;
            GlobalMethods.Toast(LoginActivity.this, "Email is Invalid");
        }else if(TextUtils.isEmpty(password)){
            cancel = true;
            GlobalMethods.Toast(LoginActivity.this,"Passwork is Empty");
        }
        if(!cancel){
            if(GlobalMethods.isNetworkAvailable(LoginActivity.this)){
                calllogin();
            }else {
                GlobalMethods.Toast(LoginActivity.this,getString(R.string.internet));
            }
        }

    }

    private void calllogin() {
Log.e("loginrequest",email+"::"+"1"+"::"+token+"::"+"1"+"::"+password);
        Call<SocialSignupLoginResponse> call = apiService.LOGIN_RESPONSE_CALL(email,"1",token,"1",password);
        call.enqueue(new Callback<SocialSignupLoginResponse>() {
            @Override
            public void onResponse(Call<SocialSignupLoginResponse> call, Response<SocialSignupLoginResponse> response) {
                Log.e("sucessrespo",new Gson().toJson(response.body()));
                if(response.isSuccessful()){
                    SocialSignupLoginResponse resp = response.body();
                    if(resp!=null){
                        String status  = resp.getStatus();
                        if(status.equals("1")){
                            String name = resp.getData().getFirstName() +" "+resp.getData().getLastName();

                            PrefConnect.writeString(LoginActivity.this,PrefConnect.USERID,resp.getData().getId());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.USERNAME,name);
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.FIRSTNAME,resp.getData().getFirstName());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.LASTNAME,resp.getData().getLastName());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.EMAIL,resp.getData().getEmail());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.LOCATION,resp.getData().getLocation());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.LATUSER,resp.getData().getLat());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.LONGUSER,resp.getData().getLon());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.PHONENO,resp.getData().getMobileNumber());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.JOINEDDATE,resp.getData().getJoined());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.RATING,resp.getData().getRating());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.REGION,resp.getData().getRegion());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.EARNMONEY,resp.getData().getEarn());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.WORKDONE,resp.getData().getWork());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.PROFILEIMAGE,resp.getData().getProfie());
                            PrefConnect.writeString(LoginActivity.this,PrefConnect.PROFILESTATUS,resp.getData().getProfileStatus());

                            Intent login = new Intent(LoginActivity.this, MainActivity.class);
                            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(login);
                            finish();
                        }else {
                            GlobalMethods.Toast(LoginActivity.this,resp.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SocialSignupLoginResponse> call, Throwable t) {

                Log.e("failure",t.getMessage());
            }
        });
    }

    private void countrydialog() {
        countrydialog = new Dialog(context);
        countrydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        countrydialog.setContentView(R.layout.dialog_language);
        countrydialog.setCancelable(false);
        countrydialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        countrydialog.show();
        TextView oman = countrydialog.findViewById(R.id.txt_oman);
        TextView bahrain = countrydialog.findViewById(R.id.txt_bahrain);
        TextView saudi = countrydialog.findViewById(R.id.txt_saudi);
        TextView uae = countrydialog.findViewById(R.id.txt_uae);
        TextView kuwait = countrydialog.findViewById(R.id.txt_kuwait);
        ImageView close = countrydialog.findViewById(R.id.img_close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrydialog.dismiss();
            }
        });
        oman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signup = new Intent(LoginActivity.this, LoginActivity.class);
                startActivity(signup);
                countrydialog.dismiss();

            }
        });

        bahrain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signup = new Intent(LoginActivity.this, LoginActivity.class);
                startActivity(signup);
                countrydialog.dismiss();
            }
        });

        saudi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signup = new Intent(LoginActivity.this, LoginActivity.class);
                startActivity(signup);
                countrydialog.dismiss();

            }
        });
        uae.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signup = new Intent(LoginActivity.this, LoginActivity.class);
                startActivity(signup);
                countrydialog.dismiss();

            }
        });
        kuwait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signup = new Intent(LoginActivity.this, LoginActivity.class);
                startActivity(signup);
                countrydialog.dismiss();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
