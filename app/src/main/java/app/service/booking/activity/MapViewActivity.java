package app.service.booking.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AddressComponent;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.service.booking.R;
import app.service.booking.utils.PrefConnect;
import app.service.booking.utils.Tracker;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapViewActivity extends AppCompatActivity implements OnMapReadyCallback,Tracker.LocationCallback {
    String booking_location, latitude, longitude, currentlat, currentlong;

    GoogleMap map;
    Marker markerfrom;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
   // PlaceAutocompleteFragment autocompleteFragmentMapFrom;
    Tracker tracker;
    @BindView(R.id.places_autocomplete)
    PlacesAutocompleteTextView placesAutocomplete;
    public static  String MY_PREFS_NAME = "MyPref";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_view);
        ButterKnife.bind(this);
        tracker = new Tracker(this, this);
        setBookingLocation();

       /* autocompleteFragmentMapFrom = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.places_autocomplete);
        autocompleteFragmentMapFrom.setHint("Search location");
        ((EditText)autocompleteFragmentMapFrom.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(12.5f);

        callPlacesFragment();*/
        /*if (getIntent() != null) {
            booking_location = getIntent().getStringExtra("location");
            latitude = getIntent().getStringExtra("lat");
            longitude = getIntent().getStringExtra("long");
            Log.e("location", longitude);


        }*/
        ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

        // Enable MyLocation Button in the Map
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

    }

/*
    private void callPlacesFragment() {
        LatLng south = (new LatLng(18.00249917010728, -76.78180582989273));
        LatLng north = (new LatLng(18.00519882989272, -76.77910617010728));
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build();
        autocompleteFragmentMapFrom.setBoundsBias(new LatLngBounds(south, north));
        autocompleteFragmentMapFrom.setFilter(typeFilter);
        autocompleteFragmentMapFrom.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.e("Address", place.getAddress().toString());
                Log.e("Name", place.getName().toString());
                Log.e("LatitudeSearch", place.getLatLng().latitude + "");
                Log.e("LongitudeSearch", place.getLatLng().longitude + "");
                latitude = place.getLatLng().latitude + "";
                longitude = place.getLatLng().longitude + "";                //  location_name = place.getName().toString();
                booking_location = place.getAddress().toString();

                PrefConnect.writeString(MapViewActivity.this,PrefConnect.latitudepick,booking_location);

               */
/* lat1 = place.getLatLng().latitude;
                lon1 = place.getLatLng().longitude;*//*

                LatLng latLngstart = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));


                markerfrom = map.addMarker(new MarkerOptions()
                        .position(latLngstart)
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(
                                        BitmapDescriptorFactory.HUE_GREEN)));

                markerfrom.showInfoWindow();


                map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngstart, 15.0f));


            }

            @Override
            public void onError(Status status) {

            }
        });
    }
*/






    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(MapViewActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapViewActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        if (googleMap != null) {
            googleMap.getUiSettings().setMapToolbarEnabled(false);
            googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location arg0) {
                    // if(initialocation==0){
                    Location newHotSpot = new Location("My Location");
                    newHotSpot.setLatitude(arg0.getLatitude());
                    newHotSpot.setLongitude(arg0.getLongitude());
                    LatLng latLng = new LatLng(arg0.getLatitude(), arg0.getLongitude());
                    //  }
                }
            });
            try {

                map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {


                        double lat = latLng.latitude;
                        double lng = latLng.longitude;

                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(MapViewActivity.this, Locale.getDefault());

                        try {
                            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();
                            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
                            Log.e("place",address);

                            placesAutocomplete.setText(address);
                            placesAutocomplete.setCompletionEnabled(false);

                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                            editor.putString("name", "Elena");
                            editor.putString("name", "Elena");
                            editor.putString("name", "Elena");

                            editor.apply();

                          //  PrefConnect.writeString(MapViewActivity.this,PrefConnect.latitudepick,address);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                        Log.e("latlongclick", lat + "::" + lng);
                        LatLng latLngstart = new LatLng(lat, lng);

                        try {
                            markerfrom.remove();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        markerfrom = map.addMarker(new MarkerOptions()
                                .position(latLngstart)
                                .icon(BitmapDescriptorFactory
                                        .defaultMarker(
                                                BitmapDescriptorFactory.HUE_GREEN)));

                        markerfrom.showInfoWindow();

                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngstart, 15.0f));


                    }
                });
                //}

            } catch (NumberFormatException e) {
                e.printStackTrace();
            }


        }

    }
    private void setBookingLocation() {
        placesAutocomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("TAG", "Focus EDIT TEXT");
                placesAutocomplete.setFocusableInTouchMode(true);
                placesAutocomplete.requestFocus();
                placesAutocomplete.setCompletionEnabled(true);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(placesAutocomplete, InputMethodManager.SHOW_IMPLICIT);
            }
        });
        placesAutocomplete.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(final Place place) {
                placesAutocomplete.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(final PlaceDetails details) {
                        Log.e("test", "details " + details.address_components);
                        //booking_location = details.address_components + "";
                        String address = "";
                        // Log.d("test", "details " + booking_location);
                        for (AddressComponent component : details.address_components) {
                            if (address.equals("")) {
                                address = component.long_name;
                            } else {
                                address += " ," + component.long_name;
                            }
                        }
                        Log.e("testjjju", address);
                        booking_location = address;
                       // PrefConnect.writeString(MapViewActivity.this,PrefConnect.latitudepick,booking_location);

                        latitude = details.geometry.location.lat + "";
                        longitude = details.geometry.location.lng + "";
                        Geocoder coder = new Geocoder(MapViewActivity.this);
                        try {
                            ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(booking_location, 50);
                            for(Address add : adresses){
                                if (add!=null) {//Controls to ensure it is right address such as country etc.
                                    double longitude = add.getLongitude();
                                    double latitude = add.getLatitude();

                                    Log.e("latlong",latitude+":::"+longitude);



                                    LatLng latLngstart = new LatLng(latitude, longitude);

                                    try {
                                        markerfrom.remove();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                    markerfrom = map.addMarker(new MarkerOptions()
                                            .position(latLngstart)
                                            .icon(BitmapDescriptorFactory
                                                    .defaultMarker(
                                                            BitmapDescriptorFactory.HUE_GREEN)));

                                    markerfrom.showInfoWindow();

                                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngstart, 15.0f));

                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                        // details.formatted_address.
                        View view = getCurrentFocus();
                        //If no view currently has focus, create a new one, just so we can grab a window token from it
                        if (view == null) {
                            view = new View(MapViewActivity.this);
                        }
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    @Override
                    public void onFailure(final Throwable failure) {
                        Log.d("test", "failure " + failure);
                    }
                });
            }
        });


    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    protected void onResume() {
        super.onResume();
        tracker.connectClient();
    }

    @Override
    protected void onPause() {
        super.onPause();
        tracker.disConnectClient();
    }

    @Override
    public void onLocationReceived(Location location) {
        if (location != null) {
            currentlat = String.valueOf(location.getLatitude());
            currentlong = String.valueOf(location.getLongitude());

            Log.e("locationrecivere",currentlong+"::"+currentlat);
        }
    }
}
