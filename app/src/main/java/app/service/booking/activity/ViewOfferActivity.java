package app.service.booking.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.adapter.AttachmentAdapter;
import app.service.booking.adapter.ImageAdapter;
import app.service.booking.adapter.QuestionAdapter;
import app.service.booking.model.ImageModel;
import app.service.booking.model.QuestionModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewOfferActivity extends AppCompatActivity {

    Context context;
    List<QuestionModel> question_list;
    QuestionAdapter questionAdapter;

    List<ImageModel> image_list;
    AttachmentAdapter adapter;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    /*@BindView(R.id.viewPager)
    ViewPager viewPager;*/
    @BindView(R.id.txt_view_offer)
    TextView txtSendOffer;
    @BindView(R.id.txt_username)
    TextView txtUsername;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.txt_location)
    TextView txtLocation;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_any)
    TextView txtAny;
    @BindView(R.id.txt_requirement)
    TextView txtRequirement;
    /* @BindView(R.id.txt_req2)
     TextView txtReq2;
     @BindView(R.id.txt_req3)
     TextView txtReq3;*/
    @BindView(R.id.btn_complete)
    TextView btnComplete;
    @BindView(R.id.recycler_home)
    RecyclerView recyclerHome;
    @BindView(R.id.recycler_question)
    RecyclerView recyclerQuestion;
    @BindView(R.id.prfImage)
    ImageView prfImage;
    Dialog feedbackdialog;
    @BindView(R.id.btn_delete)
    Button btnDelete;
    @BindView(R.id.scrollview)
    ScrollView scrollview;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_offer);
        ButterKnife.bind(this);
        context = ViewOfferActivity.this;
        txtTitle.setText("Task Details");

        ImageAdapter adapterView = new ImageAdapter(this);
        // viewPager.setAdapter(adapterView);
        image_list = new ArrayList<>();
        image_list.add(new ImageModel(R.drawable.seat_belts));
        image_list.add(new ImageModel(R.drawable.seat_belts));
        image_list.add(new ImageModel(R.drawable.seat_belts));

        adapter = new AttachmentAdapter(context, image_list);
        recyclerHome.setAdapter(adapter);
        recyclerHome.setLayoutManager(new GridLayoutManager(ViewOfferActivity.this, 1, LinearLayout.HORIZONTAL, false));

        question_list = new ArrayList<>();
        question_list.add(new QuestionModel(R.drawable.placeholder_profile, "James Johnson", "20 mins ago", "Need front and rear seat belts installed into my baby HSV Grange.", "REPLY"));
        question_list.add(new QuestionModel(R.drawable.placeholder_profile, "James Johnson", "20 mins ago", "Need front and rear seat belts installed into my baby HSV Grange.", "REPLY"));
        questionAdapter = new QuestionAdapter(context, question_list);
        recyclerQuestion.setAdapter(questionAdapter);
        recyclerQuestion.setLayoutManager(new LinearLayoutManager(context));
    }


    @OnClick({R.id.btn_delete, R.id.prfImage, R.id.img_back, R.id.btn_complete, R.id.txt_view_offer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_delete:
                Dialog dialoglogout = new AlertDialog.Builder(ViewOfferActivity.this)

                        .setMessage(getString(R.string.delete_msg))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                onBackPressed();

                            }
                        })
                        .setNegativeButton(getString(R.string.no), null)
                        .show();
                break;
            case R.id.prfImage:
                Intent intent = new Intent(ViewOfferActivity.this, OtherUserProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_complete:
                openfeedbackdialog();
                break;
            case R.id.txt_view_offer:
                Intent intent1 = new Intent(ViewOfferActivity.this, OfferListActivity.class);
                startActivity(intent1);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void openfeedbackdialog() {

        feedbackdialog = new Dialog(context);
        feedbackdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        feedbackdialog.setContentView(R.layout.dialog_rating);
        feedbackdialog.setCancelable(false);
        feedbackdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button submit = (Button) feedbackdialog.findViewById(R.id.btn_submit);
        feedbackdialog.show();


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feedbackdialog.dismiss();
            }
        });
    }
}

