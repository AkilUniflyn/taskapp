package app.service.booking.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AddressComponent;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.service.booking.R;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BrowsFilterActivity extends AppCompatActivity {
    Context context;
    @BindView(R.id.txt_search)
    TextView txtSearch;
    @BindView(R.id.txt_clear)
    TextView txtClear;

    @BindView(R.id.txt_min)
    TextView txtMin;
    @BindView(R.id.txt_max)
    TextView txtMax;
    @BindView(R.id.rangeSeekbar2)
    CrystalRangeSeekbar rangeSeekbar2;
    @BindView(R.id.txt_min_price)
    TextView txtMinPrice;
    @BindView(R.id.txt_max_price)
    TextView txtMaxPrice;
    @BindView(R.id.checkbox_price)
    ImageView checkboxPrice;
    @BindView(R.id.checkbox_distance)
    ImageView checkboxDistance;
    @BindView(R.id.rangeSeekbar1)
    CrystalSeekbar rangeSeekbar1;
    @BindView(R.id.distance_layout)
    LinearLayout distanceLayout;
    @BindView(R.id.pricelayout)
    LinearLayout pricelayout;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_apply)
    Button btnApply;
    @BindView(R.id.places_autocomplete)
    PlacesAutocompleteTextView placesAutocomplete;
    @BindView(R.id.linearClear)
    LinearLayout linearClear;

    String booking_location, latitude, longitude;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    @BindView(R.id.switchone)
    Switch switchone;
    @BindView(R.id.btn_place)
    Button btnPlace;
    public static  String  lat,log,location, distance = "1000", minprice = "0", maxprice = "1000", sortype = "1", opentask = "0", doneby = "1", userid, page = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brows_filter);
        ButterKnife.bind(this);
        context = BrowsFilterActivity.this;
        setBookingLocation();



        rangeSeekbar1.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                distance = String.valueOf(value);
                txtMax.setText(String.valueOf(value) + "km");
            }
        });
        rangeSeekbar2.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            public void valueChanged(Number minValue, Number maxValue) {
                txtMinPrice.setText("$" + String.valueOf(minValue));
                txtMaxPrice.setText("$" + String.valueOf(maxValue));
                maxprice = String.valueOf(maxValue);
            }
        });

        rangeSeekbar2.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
            }
        });
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            callMultiplePermissions();
        } else {
            // Pre-Marshmallow
        }

        rangeSeekbar2.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
            }
        });
        switchone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    opentask = "1";

                }else
                {
                    opentask = "0";

                }
            }
        });

        rangeSeekbar1.setMinStartValue(1000).apply();
        txtMin.setText("0Km");


    }

    private void callMultiplePermissions() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
       /* if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("NETWORK STATE");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("WRITE EXTERNAL STORAGE");*/

        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("ACCESS COARSE LOCATION");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("ACCESS FINE LOCATION");


        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                } else {
                    // Pre-Marshmallow
                }

                return;
            }
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            } else {
                // Pre-Marshmallow
            }

            return;
        }

    }

    /**
     * add Permissions
     *
     * @param permissionsList
     * @param permission
     * @return
     */
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        } else {
            // Pre-Marshmallow
        }

        return true;
    }

    /**
     * Permissions results
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                // Initial
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION and others

              /*  perms.get(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        &&*/

                if (perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted

                } else {
                    // Permission Denied
                    Toast.makeText(BrowsFilterActivity.this, "Permission is Denied", Toast.LENGTH_SHORT)
                            .show();

                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();

       /* String address= PrefConnect.readString(BrowsFilterActivity.this,PrefConnect.latitudepick,"");

        Log.e("adddres",address);
        if(TextUtils.isEmpty(address)){
            placesAutocomplete.setHint("Search Location");
        }else {
            placesAutocomplete.setText(address);
        }*/
    }

    private void setBookingLocation() {
        placesAutocomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("TAG", "Focus EDIT TEXT");
                placesAutocomplete.setFocusableInTouchMode(true);
                placesAutocomplete.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(placesAutocomplete, InputMethodManager.SHOW_IMPLICIT);
            }
        });
        placesAutocomplete.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(final Place place) {
                placesAutocomplete.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(final PlaceDetails details) {
                        Log.e("test", "details " + details.address_components);
                        //booking_location = details.address_components + "";
                        String address = "";
                        // Log.d("test", "details " + booking_location);
                        for (AddressComponent component : details.address_components) {
                            if (address.equals("")) {
                                address = component.long_name;
                            } else {
                                address += " ," + component.long_name;
                            }
                        }

                        location = address;
                        booking_location = address;
                        Geocoder coder = new Geocoder(BrowsFilterActivity.this);
                        try {
                            ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(booking_location, 50);
                            for(Address add : adresses){
                                if (add!=null) {//Controls to ensure it is right address such as country etc.
                                    double longitude = add.getLongitude();
                                    double latitude = add.getLatitude();

                                    Log.e("latlong",latitude+":::"+longitude);
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                        lat = details.geometry.location.lat + "";
                        log = details.geometry.location.lng + "";

                        Log.e("testjjju", address+"::"+latitude);
                        // details.formatted_address.
                        View view = getCurrentFocus();
                        //If no view currently has focus, create a new one, just so we can grab a window token from it
                        if (view == null) {
                            view = new View(BrowsFilterActivity.this);
                        }
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    @Override
                    public void onFailure(final Throwable failure) {
                        Log.d("test", "failure " + failure);
                    }
                });
            }
        });


    }


    @OnClick({R.id.btn_place,R.id.linearClear, R.id.distance_layout, R.id.pricelayout, R.id.btn_cancel, R.id.btn_apply, R.id.checkbox_distance, R.id.checkbox_price, R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_place:
                //PrefConnect.clearAllPrefs(BrowsFilterActivity.this);
                Intent intent = new Intent(BrowsFilterActivity.this, MapViewActivity.class);
                startActivity(intent);
                break;
            case R.id.linearClear:
                Log.e("linearcle", "yes");
                finish();
                startActivity(getIntent());
                break;
            case R.id.distance_layout:
                break;
            case R.id.pricelayout:
                break;
            case R.id.btn_cancel:

                rangeSeekbar1.setMinStartValue(1000).apply();
                txtMin.setText("0Km");
                rangeSeekbar2.setMinStartValue(0).setMaxStartValue(1000).apply();
                placesAutocomplete.setText("");
                placesAutocomplete.setHint("Search Booking Location");
                checkboxPrice.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
                checkboxDistance.setImageDrawable(getResources().getDrawable(R.drawable.spuncheck));
                switchone.setChecked(false);
                placesAutocomplete.setCompletionEnabled(false);
                /*finish();
                startActivity(getIntent());*/
                //onBackPressed();
                break;
            case R.id.btn_apply:
              /*  maxprice = txtMaxPrice.getText().toString();
                distance = txtMax.getText().toString();*/

                Log.e("locationvalue",location+"::"+lat+"::"+log+"::"+maxprice+"::"+distance+"::"+sortype+"::"+opentask);


                onBackPressed();
                break;
            case R.id.checkbox_distance:
                checkboxDistance.setImageResource(R.drawable.check_icon);
               /* pricelayout.setVisibility(View.GONE);
                distanceLayout.setVisibility(View.VISIBLE);*/
                checkboxPrice.setImageResource(R.drawable.uncheck_icon);
                sortype="2";
                break;
            case R.id.checkbox_price:
                checkboxPrice.setImageResource(R.drawable.check_icon);
               /* pricelayout.setVisibility(View.VISIBLE);
                distanceLayout.setVisibility(View.GONE);*/
                checkboxDistance.setImageResource(R.drawable.uncheck_icon);
                sortype="1";
                break;
            case R.id.img_back:
               // PrefConnect.clearAllPrefs(BrowsFilterActivity.this);
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
