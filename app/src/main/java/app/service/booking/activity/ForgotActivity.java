package app.service.booking.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.model.api.CommonResponse;
import app.service.booking.utils.GlobalMethods;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotActivity extends AppCompatActivity {

    @BindView(R.id.edt_username)
    EditText edtUsername;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.txt_backto_login)
    TextView txtBacktoLogin;
    @BindView(R.id.img_back)
    ImageView img_back;
    ApiService apiService;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_forgot);
        ButterKnife.bind(this);
        apiService = RetrofitSingleton.creaservice(ApiService.class);
       /* edtUsername.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/

    }

    @OnClick({R.id.btn_submit, R.id.txt_backto_login,R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btn_submit:
                callValidation();
               /* Intent login = new Intent(LoginActivity.this, WelcomeActivity.class);
                startActivity(login);*/

                break;
            case R.id.txt_backto_login:
                finish();
               /* Intent facebook = new Intent(ForgotActivity.this, LoginActivity.class);
                startActivity(facebook);*/
                break;
            case R.id.img_back:
                finish();
                break;

        }
    }

    private void callValidation() {
        email = edtUsername.getText().toString();
        boolean cancel = false;
        
        if(TextUtils.isEmpty(email)){
            GlobalMethods.Toast(ForgotActivity.this,"Email is Empty");
            cancel = true;
        }else if (!GlobalMethods.validateEmail(email)) {
            cancel = true;
            GlobalMethods.Toast(ForgotActivity.this, "Email is Invalid");
        }
        if(!cancel){
            if(GlobalMethods.isNetworkAvailable(ForgotActivity.this)){
                callForgot();
            }else {
                GlobalMethods.Toast(ForgotActivity.this,getString(R.string.internet));
            }
        }
    }

    private void callForgot() {
        Call<CommonResponse> call = apiService.calForgot(email,"1");
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Log.e("successRespo",new Gson().toJson(response.body()));
                if(response.isSuccessful()){
                    CommonResponse resp = response.body();
                    if(resp!=null){
                        String status  =resp.getStatus();
                        if(status.equalsIgnoreCase("1")){
                            finish();
                        }else {
                            GlobalMethods.Toast(ForgotActivity.this,resp.getMessage());

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

            }
        });
    }
}