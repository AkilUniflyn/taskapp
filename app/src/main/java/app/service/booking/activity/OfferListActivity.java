package app.service.booking.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.adapter.OfferListAdapter;
import app.service.booking.model.OfferListModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OfferListActivity extends AppCompatActivity {

    @BindView(R.id.btn_back)
    Button btnBack;
    @BindView(R.id.txt_titles)
    TextView txtTitles;
    @BindView(R.id.recycleOfferList)
    RecyclerView recycleOfferList;
    List<OfferListModel> offerModels;
    OfferListAdapter offerListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_list);
        ButterKnife.bind(this);
        offerModels = new ArrayList<>();
        offerModels.add(new OfferListModel("James Johnson", "20 mins ago", "Lorem Ipsum is simply dummy text", "40"));
        offerModels.add(new OfferListModel("James Johnson", "20 mins ago", "Lorem Ipsum is simply dummy text", "40"));
        offerModels.add(new OfferListModel("James Johnson", "20 mins ago", "Lorem Ipsum is simply dummy text", "40"));
        offerListAdapter = new OfferListAdapter(offerModels, OfferListActivity.this);
        recycleOfferList.setAdapter(offerListAdapter);
        recycleOfferList.setLayoutManager(new LinearLayoutManager(OfferListActivity.this));
    }

    @OnClick(R.id.btn_back)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
