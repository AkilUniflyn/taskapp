package app.service.booking.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.StringTokenizer;

import app.service.booking.Instagram.InstagramApp;
import app.service.booking.MainActivity;
import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.model.api.SignupResponse;
import app.service.booking.model.api.SocialSignupLoginResponse;
import app.service.booking.utils.GlobalMethods;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;

public class SignUpActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    /*
        @BindView(R.id.edt_firstname)
        EditText edtFirstname;
        @BindView(R.id.edt_lastname)
        EditText edtLastname;*/
    @BindView(R.id.edt_emailaddress)
    EditText edtEmailaddress;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    /* @BindView(R.id.edt_mobileno)
     EditText edtMobileno;
     @BindView(R.id.edt_location)
     EditText edtLocation;
     @BindView(R.id.edt_lookingfor)
     EditText edtLookingfor;*/
    @BindView(R.id.checkbox_workdone)
    ImageView checkboxWorkdone;
    @BindView(R.id.checkbox_earnmoney)
    ImageView checkboxEarnmoney;
    @BindView(R.id.txt_terms_condition)
    TextView txtTermsCondition;
    @BindView(R.id.btn_signup)
    Button btnSignup;
    @BindView(R.id.txt_backto_login)
    TextView txtBacktoLogin;
    int check_status = 0;
    int check_box = 0;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.img_one)
    ImageView imgOne;
    @BindView(R.id.facebook)
    ImageView facebook;
    @BindView(R.id.google)
    ImageView google;
    @BindView(R.id.twitter)
    ImageView twitter;
    @BindView(R.id.login_button_fb)
    LoginButton loginButtonFb;
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;
    ApiService apiService;
    private static final int RC_SIGN_IN = 007;
    String google_name, google_email_address, google_gmail_id;
    String token, fb_your_name, fb_email_address, fb_id;
    private static final String[] PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,


    };
    ProgressDialog progressDialog;

    String region,email,password,work_check,earn_check,type="1",languageid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        apiService = RetrofitSingleton.creaservice(ApiService.class);

        SharedPreferences prefs = getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        languageid = prefs.getString("languageid", "1");//"No name defined" is the default value.
        Log.e("signuplanguid",languageid);

        checkAllPermission();
        if (getIntent() != null) {
            region = getIntent().getStringExtra("region");

        }

        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
        }


        try {
            token = FirebaseInstanceId.getInstance().getToken();
            Log.e("Token", token + "");
        } catch (Exception e) {
            e.printStackTrace();
        }


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(SignUpActivity.this)
                .enableAutoManage(SignUpActivity.this, SignUpActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        new Login().execute();


       /* edtEmailaddress.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });


        edtPassword.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/

    }

    public void checkAllPermission() {

        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)) {


        } else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    public boolean hasPermissions(Context context , String... permissions) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && SignUpActivity.this != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(SignUpActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
                Log.d("return==", "permission");
            }

        }
        return true;
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CLEAR_APP_CACHE,
                Manifest.permission.ACCESS_FINE_LOCATION}, 101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            /*case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                break;*/
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
            Log.e("result", String.valueOf(result.getStatus()));
            Log.e("result", String.valueOf(result.getSignInAccount()));

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }


    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.e("GoogleSignInResult ", String.valueOf(result));
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            google_name = acct.getDisplayName();
            google_gmail_id = acct.getId();
            google_email_address = acct.getEmail();


            Log.e("str_google_name ", google_name);
            Log.e("str_google_gmail_id ", google_gmail_id);
            Log.e("_email_address", google_email_address);

            StringTokenizer tokens = new StringTokenizer(google_name, " ");
           String fsname= tokens.nextToken();// this will contain "Fruit"
            String lsname = tokens.nextToken();
            callSocialSignup(google_email_address,fsname,lsname);

        } else {
            GlobalMethods.Toast(SignUpActivity.this,"Google Syncing, Try again");
            Log.e("login failed", "google");
        }
    }

    public class Login extends AsyncTask {

        @SuppressLint("WrongThread")
        @Override
        protected Object doInBackground(Object[] objects) {
            loginButtonFb.setReadPermissions(Arrays.asList("email"));
            // FirebaseInstanceId.getInstance();
            loginButtonFb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {

                    if (AccessToken.getCurrentAccessToken() != null) {
                        RequestData();
                    }
                }

                @Override
                public void onCancel() {
                    LoginManager.getInstance().logOut();

                }

                @Override
                public void onError(FacebookException exception) {
                }
            });

            return "";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
        }
    }
    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("Login response", new Gson().toJson(response.getJSONObject()));
                        Log.e("Login response getError", new Gson().toJson(response.getError()));

                        JSONObject json = response.getJSONObject();
                        Log.e("json", String.valueOf(json));
                        try {
                            if (json != null) {

                                fb_email_address = json.getString("email");
                                fb_your_name = json.getString("name");
                                fb_id = json.getString("id");
                                String fsname = json.getString("first_name");
                                String lsname = json.getString("last_name");
                                Log.e("name", fb_your_name);
                                Log.e("email", fb_email_address);
                                Log.e("id", fb_id);

                                callSocialSignup(fb_email_address,fsname,lsname);




                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,first_name,last_name,email,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();


    }

    private void callSocialSignup(String email_address, String fsname, String lsname) {
        Log.e("ReqSocialsignup",email_address+"::"+fsname+"::"+lsname+"::"+token);
        progressDialog = ProgressDialog.show(SignUpActivity.this, "", "Loading...", true);

        Call<SocialSignupLoginResponse> call = apiService.SOCIAL_SIGNUP_LOGIN_RESPONSE_CALL(fsname,lsname,"",email_address,"1",token,languageid,region);
        call.enqueue(new Callback<SocialSignupLoginResponse>() {
            @Override
            public void onResponse(Call<SocialSignupLoginResponse> call, Response<SocialSignupLoginResponse> response) {
                Log.e("sucesssocial",new Gson().toJson(response.body()));
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    SocialSignupLoginResponse resp = response.body();
                    if(resp!=null){
                        String status = resp.getStatus();
                        if(status.equals("1")){
                            String name = resp.getData().getFirstName() +" "+resp.getData().getLastName();

                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.USERID,resp.getData().getId());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.USERNAME,name);
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.FIRSTNAME,resp.getData().getFirstName());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.LASTNAME,resp.getData().getLastName());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.EMAIL,resp.getData().getEmail());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.LOCATION,resp.getData().getLocation());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.LATUSER,resp.getData().getLat());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.LONGUSER,resp.getData().getLon());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.PHONENO,resp.getData().getMobileNumber());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.JOINEDDATE,resp.getData().getJoined());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.RATING,resp.getData().getRating());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.REGION,resp.getData().getRegion());


                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.EARNMONEY,resp.getData().getEarn());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.WORKDONE,resp.getData().getWork());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.PROFILEIMAGE,resp.getData().getProfie());
                            PrefConnect.writeString(SignUpActivity.this,PrefConnect.PROFILESTATUS,resp.getData().getProfileStatus());
                            Intent login = new Intent(SignUpActivity.this, MainActivity.class);
                            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(login);
                            finish();
                        }else {
                            GlobalMethods.Toast(SignUpActivity.this,resp.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SocialSignupLoginResponse> call, Throwable t) {

                progressDialog.dismiss();
            }
        });

    }







    @OnClick({R.id.facebook,R.id.google, R.id.twitter,R.id.txt_backto_login, R.id.btn_signup, R.id.txt_terms_condition, R.id.checkbox_workdone, R.id.checkbox_earnmoney, R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.facebook:
                if (GlobalMethods.isNetworkAvailable(SignUpActivity.this)) {
                    loginButtonFb.performClick();
                } else {
                    GlobalMethods.Toast(SignUpActivity.this, getString(R.string.internet));
                }
                break;
            case R.id.google:
                if (GlobalMethods.isNetworkAvailable(SignUpActivity.this)) {
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                } else {
                    GlobalMethods.Toast(SignUpActivity.this, getString(R.string.internet));
                }
                break;
            case R.id.twitter:


                break;
            case R.id.txt_backto_login:
                onBackPressed();
                break;
            case R.id.btn_signup:
                validator();
              /*  Intent login = new Intent(SignUpActivity.this, MainActivity.class);
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(login);
                finish();*/
                break;
            case R.id.txt_terms_condition:
                Intent facebook = new Intent(SignUpActivity.this, TermsAndConditionActivity.class);
                startActivity(facebook);
                break;
            case R.id.checkbox_workdone:
                if (check_status == 0) {
                    check_status = 1;
                    checkboxWorkdone.setImageResource(R.drawable.spchecked);

                } else if (check_status == 1) {
                    check_status = 0;
                    checkboxWorkdone.setImageResource(R.drawable.spuncheck);

                }

                break;
            case R.id.checkbox_earnmoney:
                if (check_box == 0) {
                    check_box = 1;
                    checkboxEarnmoney.setImageResource(R.drawable.spchecked);

                } else if (check_box == 1) {
                    check_box = 0;
                    checkboxEarnmoney.setImageResource(R.drawable.spuncheck);

                }
                break;
            case R.id.img_back:
                finish();
                break;

        }
    }
    public  boolean validator(){
        boolean cancel=false;
        email=edtEmailaddress.getText().toString();
        password=edtPassword.getText().toString();


        if (email.equals("")){
            GlobalMethods.Toast(SignUpActivity.this,"Enter Your EmailId");
            return  false;
        }else  if (!email.contains("@")){
            GlobalMethods.Toast(SignUpActivity.this,"Enter Valid EmailId");
            return  false;
        }else  if (password.equals("")){
            GlobalMethods.Toast(SignUpActivity.this,"Enter Password");
            return  false;
        }else  if (password.length()<6){
            GlobalMethods.Toast(SignUpActivity.this,"Password must minimum 6 Character");
            return  false;
        }

        if(!cancel){

            if(GlobalMethods.isNetworkAvailable(SignUpActivity.this)) {

                callRegisterMethod();

            } else {
                GlobalMethods.Toast(SignUpActivity.this,getResources().getString(R.string.internet));

            }
        }



        return  true;
    }

    private void callRegisterMethod() {
        Log.e("RegisterRequest", "\nRegion:" + region + "\nemail:" + email + "\npassword:" + password + "\nwork:" + check_status + "\nearn:" + check_box + "\ntype:" + type + "\ntoken:" + token + "\nlanguage:" + languageid);
        progressDialog = ProgressDialog.show(SignUpActivity.this, "", "Loading...", true);
        Call<SignupResponse> call = apiService.callSignUpAPI(region,email,password,check_status+"",check_box+"",type,token,languageid);
        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                Log.e("RegisterResp", new Gson().toJson(response.body()));
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    SignupResponse resp = response.body();
                    if (resp.getStatus().equals("1")) {


                        PrefConnect.writeString(SignUpActivity.this,PrefConnect.USERID,resp.getData().getUserId());
                        PrefConnect.writeString(SignUpActivity.this,PrefConnect.EMAIL,resp.getData().getEmail());
                        PrefConnect.writeString(SignUpActivity.this,PrefConnect.REGION,resp.getData().getRegion());
                        PrefConnect.writeString(SignUpActivity.this,PrefConnect.PROFILESTATUS,resp.getData().getProfileStatus());

                        Intent login = new Intent(SignUpActivity.this, MainActivity.class);
                        login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(login);
                        finish();

                    } else {
                        GlobalMethods.Toast(SignUpActivity.this, resp.getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                Log.e("failure", t.getMessage());
                progressDialog.dismiss();

            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
