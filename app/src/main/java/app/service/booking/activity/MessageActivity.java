package app.service.booking.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.service.booking.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MessageActivity extends AppCompatActivity {


    Context context;
    @BindView(R.id.btn_back_arrow)
    Button btnBackArrow;
    @BindView(R.id.img_back)
    ImageView imgBack;
   /* @BindView(R.id.txt_title)
    */
   TextView txtTitle;
    @BindView(R.id.txtMsg)
    TextView txtMsg;
    @BindView(R.id.edt_type_message)
    EditText edtTypeMessage;
    @BindView(R.id.img_send)
    ImageView imgSend;
    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        ButterKnife.bind(this);
        context = MessageActivity.this;
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtTitle = (TextView) myToolbar.findViewById(R.id.txt_title);
        txtTitle.setText("James Cameron");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings: {
                // do your sign-out stuff
                break;
            }
            // case blocks for other MenuItems (if any)
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.btn_back_arrow)
    public void onViewClicked() {
        onBackPressed();
    }
}
