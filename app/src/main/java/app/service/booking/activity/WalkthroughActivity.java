package app.service.booking.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.androidpagecontrol.PageControl;

import java.util.ArrayList;

import app.service.booking.R;
import app.service.booking.adapter.WalkthroughAdapter;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WalkthroughActivity extends AppCompatActivity {

    private static int currentPage = 0;
    Context context;
    int status = 0;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.view_pagerlayout)
    LinearLayout viewPagerlayout;
    @BindView(R.id.page_control)
    PageControl pageControl;
    @BindView(R.id.btn_get_started)
    Button btn_get_started;
    @BindView(R.id.bottomlayout)
    LinearLayout bottomlayout;
    ArrayList<String> walk_text;
    ArrayList<String> walk_description;
    ArrayList<Integer> walk_image;
    @BindView(R.id.btn_english)
    Button btnEnglish;
    @BindView(R.id.btn_arabic)
    Button btnArabic;
    public static String MYPREFLANG = "mypreflangu";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_walkthrough);
        ButterKnife.bind(this);
        context = WalkthroughActivity.this;

        //txtSkip.setText(Html.fromHtml("<u>Skip</u>"));

        walk_text = new ArrayList<>();
        walk_text.add("Post your Task");
        walk_text.add("Review &\nAccept an offers");
        walk_text.add("Get\nyour work done");

        walk_description = new ArrayList<>();
        walk_description.add("Post your work task with easy steps");
        walk_description.add("Get offers from talented taskers and\n review best offers");
        walk_description.add("Choose the best offer and get your work\n done. Release the payment and\n leave a review.");

        walk_image = new ArrayList<>();
        walk_image.add(R.drawable.walk1);
        walk_image.add(R.drawable.walk2);
        walk_image.add(R.drawable.walk3);

        viewPager.setClipChildren(false);
        viewPager.setClipToPadding(false);
        viewPager.setAdapter(new WalkthroughAdapter(WalkthroughActivity.this, walk_text, walk_image, walk_description));
        pageControl.setViewPager(viewPager);
        viewPager.setCurrentItem(currentPage, true);

       /* txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });*/
    }

    @OnClick({R.id.btn_get_started,R.id.btn_english, R.id.btn_arabic})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_get_started:
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.btn_english:
                Log.e("english","english");
                setIntentMethod("1");
                break;
            case R.id.btn_arabic:
                Log.e("arabic","arabic");
                setIntentMethod("2");
                break;
        }
    }


    private void setIntentMethod(String lang_id) {
        Log.e("LanguageId", lang_id);

        SharedPreferences.Editor editor = getSharedPreferences(MYPREFLANG, MODE_PRIVATE).edit();
        editor.putString("languageid", lang_id);
        editor.apply();

        Intent walk = new Intent(context, LoginActivity.class);
        startActivity(walk);
    }
}
