package app.service.booking.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import app.service.booking.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PostingNewTaskActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.btn_go)
    Button btnGo;
    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posting_new_task);
        ButterKnife.bind(this);
        context=PostingNewTaskActivity.this;
    }

    @OnClick({R.id.img_back, R.id.btn_go})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_go:
                Intent new_task=new Intent(context, PostTaskStep1.class);
                startActivity(new_task);
                break;
        }
    }
}
