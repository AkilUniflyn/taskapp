package app.service.booking.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.model.api.CommonResponse;
import app.service.booking.utils.GlobalMethods;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;

public class HelpandSupportActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.btn_back)
    Button btnBack;
    @BindView(R.id.txt_titles)
    TextView txtTitles;
    @BindView(R.id.edt_msg)
    EditText edtMsg;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    ApiService apiService;
    String userid,languageid,message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helpand_support);
        ButterKnife.bind(this);
        apiService = RetrofitSingleton.creaservice(ApiService.class);
        userid = PrefConnect.readString(HelpandSupportActivity.this, PrefConnect.USERID, "");
        SharedPreferences prefs = getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        languageid = prefs.getString("languageid", "1");//"No name defined" is the default value.
        Log.e("mainlanguid", languageid);

    }

    @OnClick({R.id.img_back, R.id.btn_back,R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.btn_submit:
                message = edtMsg.getText().toString();
                if(TextUtils.isEmpty(message)){
                    GlobalMethods.Toast(HelpandSupportActivity.this,"Type anything...");
                }else {

                    if (GlobalMethods.isNetworkAvailable(HelpandSupportActivity.this)) {
                        callHelpSupport();
                    } else {
                        GlobalMethods.Toast(HelpandSupportActivity.this, getString(R.string.internet));
                    }
                }
                break;
        }
    }

    private void callHelpSupport() {
        Call<CommonResponse> call = apiService.callHelp(languageid,userid,message);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Log.e("succssMai",new Gson().toJson(response.body()));
                if(response.isSuccessful()){
                    CommonResponse resp = response.body();
                    if(resp!=null){
                        String status = resp.getStatus();
                        GlobalMethods.Toast(HelpandSupportActivity.this,resp.getMessage());
                        if(status.equalsIgnoreCase("1")){

                            edtMsg.setText("");
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
