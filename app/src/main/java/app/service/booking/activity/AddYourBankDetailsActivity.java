package app.service.booking.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import app.service.booking.MainActivity;
import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.model.api.CommonResponse;
import app.service.booking.model.api.ViewBankDetailsResponse;
import app.service.booking.utils.GlobalMethods;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;

public class AddYourBankDetailsActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.btn_accountsubmit)
    Button btnAccountsubmit;
    Context context;
    ProgressDialog progressDialog;
    @BindView(R.id.edt_accno)
    EditText edtAccno;
    @BindView(R.id.edt_routingno)
    EditText edtRoutingno;
    @BindView(R.id.edt_bankname)
    EditText edtBankname;
    @BindView(R.id.edt_benefiary_address)
            EditText edtBenefiaryAddress;

    ApiService apiService;
    String languageid,user_id,name,address,accno,routingno;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addyourbank_details);
        ButterKnife.bind(this);
        context=AddYourBankDetailsActivity.this;
        txtTitle.setText("Add Your bank Details");
        apiService = RetrofitSingleton.creaservice(ApiService.class);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        user_id= PrefConnect.readString(context,PrefConnect.USERID,"");
        Log.e("USERID",user_id);
        SharedPreferences prefs = getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        languageid = prefs.getString("languageid", "1");
        Log.e("loginlanguid",languageid);
        if(GlobalMethods.isNetworkAvailable(AddYourBankDetailsActivity.this)) {
            callviewbankdetails();
        }else {
            GlobalMethods.Toast(AddYourBankDetailsActivity.this,getString(R.string.internet));
        }

    }
    private void callviewbankdetails() {
        progressDialog = ProgressDialog.show(AddYourBankDetailsActivity.this,"","Loading...",false);
        Call<ViewBankDetailsResponse> call=apiService.callviewbankdetails(user_id,languageid);
        Log.e("VIEW BANK DETAILS",user_id+" "+languageid);
        call.enqueue(new Callback<ViewBankDetailsResponse>() {
            @Override
            public void onResponse(Call<ViewBankDetailsResponse> call, Response<ViewBankDetailsResponse> response) {
                Log.e("sucessbandetail",new Gson().toJson(response.body()));
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    ViewBankDetailsResponse resp = response.body();

                    if(resp!=null){

                        String status = resp.getStatus();
                        if(status.equals("1"))
                        {
                            name=response.body().getData().getName();
                            address=response.body().getData().getAddress();
                            accno=response.body().getData().getAccountNumber();
                            routingno=response.body().getData().getRoutingNumber();
                            edtBankname.setText(name);
                            edtAccno.setText(accno);
                            edtBenefiaryAddress.setText(address);
                            edtRoutingno.setText(routingno);

                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();

                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<ViewBankDetailsResponse> call, Throwable t) {
                Log.e("TAG","FAILURE");
                progressDialog.dismiss();
            }
        });
    }

    @OnClick({R.id.img_back, R.id.btn_accountsubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_accountsubmit:
               validation();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }



    private void validation() {
        name=edtBankname.getText().toString();
        address=edtBenefiaryAddress.getText().toString();
        accno=edtAccno.getText().toString();
        routingno=edtRoutingno.getText().toString();
        boolean cancel = false;
        if(TextUtils.isEmpty(name)){
            GlobalMethods.Toast(AddYourBankDetailsActivity.this,"Bank Name is Empty");
            cancel = true;
        }else if(TextUtils.isEmpty(address)){
            cancel = true;
            GlobalMethods.Toast(AddYourBankDetailsActivity.this,"Bank Address is Empty");
        }
        else if(TextUtils.isEmpty(accno)){
            cancel = true;
            GlobalMethods.Toast(AddYourBankDetailsActivity.this,"Account Number is Empty");
        }
        else if(TextUtils.isEmpty(routingno)){
            cancel = true;
            GlobalMethods.Toast(AddYourBankDetailsActivity.this,"Routing Number is Empty");
        }

        if(!cancel){
            if(GlobalMethods.isNetworkAvailable(AddYourBankDetailsActivity.this)){
                callbankdetails();
            }else {
                GlobalMethods.Toast(AddYourBankDetailsActivity.this,getString(R.string.internet));
            }
        }

    }

    private void callbankdetails() {
        Log.e("BANK DETAILS RESPONSE",user_id+"::"+name+"::"+address+"::"+accno+"::"+routingno+"::"+languageid);
        progressDialog = ProgressDialog.show(AddYourBankDetailsActivity.this,"","Loading...",false);
        Call<CommonResponse> call=apiService.callbankdetails(user_id,name,address,accno,routingno,languageid);

        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Log.e("resposucess",new Gson().toJson(response.body()));
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    CommonResponse resp = response.body();

                    if(resp!=null){
                        String status=resp.getStatus();
                        if(status.equalsIgnoreCase("1"))
                        {
                            onBackPressed();
                   /* Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(AddYourBankDetailsActivity.this, MainActivity.class);
                    intent.putExtra("page","4");
                    startActivity(intent);*/
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();

                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                Log.e("TAG","FAILURE");
                progressDialog.dismiss();
            }
        });
    }



}


