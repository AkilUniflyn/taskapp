package app.service.booking.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.model.ContactResponse;
import app.service.booking.utils.GlobalMethods;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CotactUsActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.btn_back)
    Button btnBack;
    @BindView(R.id.txt_titles)
    TextView txtTitles;
    @BindView(R.id.txt_email)
    TextView txtEmail;
    @BindView(R.id.txt_mobile)
    TextView txtMobile;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.txt_website)
    TextView txtWebsite;
    ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotact_us);
        ButterKnife.bind(this);
        apiService = RetrofitSingleton.creaservice(ApiService.class);
        if(GlobalMethods.isNetworkAvailable(CotactUsActivity.this)){
            callContact();
        }else {
            GlobalMethods.Toast(CotactUsActivity.this,getString(R.string.internet));
        }
    }

    private void callContact() {
        Call<ContactResponse> call = apiService.callContactDetails();
        call.enqueue(new Callback<ContactResponse>() {
            @Override
            public void onResponse(Call<ContactResponse> call, Response<ContactResponse> response) {
                Log.e("success",new Gson().toJson(response.body()));
                if(response.isSuccessful()){
                    ContactResponse resp = response.body();
                    if(resp!=null){
                        String status = resp.getStatus();
                        if(status.equalsIgnoreCase("1")){

                            txtEmail.setText(resp.getData().getEmail());
                            txtAddress.setText(resp.getData().getAddress());
                            txtMobile.setText(resp.getData().getMobile());
                            txtWebsite.setMovementMethod(LinkMovementMethod.getInstance());
                            txtWebsite.setText(Html.fromHtml(resp.getData().getWebsite()));
                        }else {

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ContactResponse> call, Throwable t) {

            }
        });

    }

    @OnClick({R.id.img_back, R.id.btn_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        fileList();
    }
}
