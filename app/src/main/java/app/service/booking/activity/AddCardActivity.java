package app.service.booking.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import app.service.booking.MainActivity;
import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.model.api.CommonResponse;
import app.service.booking.utils.GlobalMethods;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddCardActivity extends AppCompatActivity implements TextWatcher {
    private static final int CARD_DATE_TOTAL_SYMBOLS = 5; // size of pattern MM/YY
    private static final int CARD_DATE_TOTAL_DIGITS = 4; // max numbers of digits in pattern: MM + YY
    private static final int CARD_DATE_DIVIDER_MODULO = 3; // means divider position is every 3rd symbol beginning with 1
    private static final int CARD_DATE_DIVIDER_POSITION = CARD_DATE_DIVIDER_MODULO - 1; // means divider position is every 2nd symbol beginning with 0
    private static final char CARD_DATE_DIVIDER = '/';

    private static final int CARD_CVC_TOTAL_SYMBOLS = 3;

    private static final int CARD_NUMBER_TOTAL_SYMBOLS = 19; // size of pattern 0000-0000-0000-0000
    private static final int CARD_NUMBER_TOTAL_DIGITS = 16; // max numbers of digits in pattern: 0000 x 4
    private static final int CARD_NUMBER_DIVIDER_MODULO = 5; // means divider position is every 5th symbol beginning with 1
    private static final int CARD_NUMBER_DIVIDER_POSITION = CARD_NUMBER_DIVIDER_MODULO - 1; // means divider position is every 4th symbol beginning with 0
    private static final char CARD_NUMBER_DIVIDER = '-';

    String card_date, card_year, card_month, card_cvv, card_num, card_type = "debitcard";

    @BindView(R.id.btn_add_card)
    Button btn_add_card;
    Context context;
    ProgressDialog progressDialog;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.cardno)
    EditText cardno;
    @BindView(R.id.cardDateEditText)
    EditText cardDateEditText;
    @BindView(R.id.cardCVCEditText)
    EditText cardCVCEditText;
   /* @BindView(R.id.progressBar_cyclic)
    ProgressBar progressBarCyclic;*/
    @BindView(R.id.txt_title)
    TextView txtTitle;
    String user_id, appointment_id, package_id, language_id,page,user_name;
    String split_string[];
    Dialog dialog_done;
    ApiService apiService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        ButterKnife.bind(this);
        context = AddCardActivity.this;
        apiService= RetrofitSingleton.creaservice(ApiService.class);
        user_id=PrefConnect.readString(context,PrefConnect.USERID,"");
        user_name=PrefConnect.readString(context, PrefConnect.USERNAME,"");

        txtTitle.setText("Add Card");
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        cardno.addTextChangedListener(this);
        cardDateEditText.addTextChangedListener(this);
        try{
            if(getIntent()!=null){
                 page = getIntent().getStringExtra("page");//0-New task , 1-From payment history


            }

        }catch (Exception e){
            e.printStackTrace();
        }

        /*cardno.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });

        cardDateEditText.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });
        cardCVCEditText.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/


    }
    private void setDoneDialog() {

        dialog_done = new Dialog(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);
        dialog_done.setContentView(R.layout.dialog_done);
        dialog_done.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_done.setCancelable(false);
        dialog_done.show();
        Button btn_view_ur_task = (Button) dialog_done.findViewById(R.id.btn_view_ur_task);
        btn_view_ur_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_done.dismiss();

                Intent verification = new Intent(context, MainActivity.class);
                verification.putExtra("page","3");
                verification.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(verification);
                finish();
            }
        });


    }



    private void validation() {
        try {
            card_num=cardno.getText().toString();
            card_date=cardDateEditText.getText().toString();
            split_string=  card_date.split("/");
            card_month=split_string[0];
            card_year=split_string[1];
            Log.e("CARD DATE",card_date+"::"+card_month+"::"+card_year);

            card_cvv=cardCVCEditText.getText().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean cancel=false;
        if(TextUtils.isEmpty(card_num)){
            GlobalMethods.Toast(AddCardActivity.this,"Card Number is Empty");
            cancel = true;
        }else if(TextUtils.isEmpty(card_date)){
            cancel = true;
            GlobalMethods.Toast(AddCardActivity.this,"Expiry Date is Empty");
        }
        else if(TextUtils.isEmpty(card_cvv)){
            cancel = true;
            GlobalMethods.Toast(AddCardActivity.this,"Cvv is Empty");
        }
        if(!cancel){
            if(GlobalMethods.isNetworkAvailable(AddCardActivity.this)){
                callcarddetails();
            }else {
                GlobalMethods.Toast(AddCardActivity.this,getString(R.string.internet));
            }
        }

    }

    private void callcarddetails() {
        progressDialog = ProgressDialog.show(AddCardActivity.this,"","Loading...",false);
        Call<CommonResponse> call=apiService.calladdcard(user_id,language_id,card_num,user_name,card_month,card_year,card_cvv);
        Log.e("CARD DETAILS",user_id+"::"+language_id+"::"+card_num+"::"+user_name+"::"+card_month+"::"+card_year+"::"+card_cvv);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    CommonResponse resp = response.body();
                    if(resp!=null){
                        String status = resp.getStatus();
                        if(status.equalsIgnoreCase("1")){
                            Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(AddCardActivity.this,MainActivity.class);
                            intent.putExtra("page","4");
                            startActivity(intent);
                        }else {
                            Toast.makeText(getApplicationContext(),response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                Log.e("TAG","FAILURE");
                progressDialog.dismiss();
            }
        });
    }


    @OnClick({R.id.img_back, R.id.btn_add_card})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_add_card:
                if(page.equalsIgnoreCase("0")){
                    setDoneDialog();
                }else {
                   validation();
                }
                break;
        }
    }









    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }



    private boolean isInputCorrect(Editable s, int size, int dividerPosition, char divider) {
        boolean isCorrect = s.length() <= size;
        for (int i = 0; i < s.length(); i++) {
            if (i > 0 && (i + 1) % dividerPosition == 0) {
                isCorrect &= divider == s.charAt(i);
            } else {
                isCorrect &= Character.isDigit(s.charAt(i));
            }
        }
        return isCorrect;
    }

    private String concatString(char[] digits, int dividerPosition, char divider) {
        final StringBuilder formatted = new StringBuilder();

        for (int i = 0; i < digits.length; i++) {
            if (digits[i] != 0) {
                formatted.append(digits[i]);
                if ((i > 0) && (i < (digits.length - 1)) && (((i + 1) % dividerPosition) == 0)) {
                    formatted.append(divider);
                }
            }
        }

        return formatted.toString();
    }

    private char[] getDigitArray(final Editable s, final int size) {
        char[] digits = new char[size];
        int index = 0;
        for (int i = 0; i < s.length() && index < size; i++) {
            char current = s.charAt(i);
            if (Character.isDigit(current)) {
                digits[index] = current;
                index++;
            }
        }
        return digits;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        card_num = String.valueOf(s);

        //Log.i("TAG", "Card Date:" + s);
        //  Log.i("TAG", "Card Date :" + cardDateEditText.getEditableText());

        if (s == cardno.getEditableText()) {
            if (!isInputCorrect(s, CARD_NUMBER_TOTAL_SYMBOLS, CARD_NUMBER_DIVIDER_MODULO, CARD_NUMBER_DIVIDER)) {
                s.replace(0, s.length(), concatString(getDigitArray(s, CARD_NUMBER_TOTAL_DIGITS), CARD_NUMBER_DIVIDER_POSITION, CARD_NUMBER_DIVIDER));

            }
            // DO STH
        } else if (s == cardDateEditText.getEditableText()) {
            if (!isInputCorrect(s, CARD_DATE_TOTAL_SYMBOLS, CARD_DATE_DIVIDER_MODULO, CARD_DATE_DIVIDER)) {
                s.replace(0, s.length(), concatString(getDigitArray(s, CARD_DATE_TOTAL_DIGITS), CARD_DATE_DIVIDER_POSITION, CARD_DATE_DIVIDER));

            } else if (s == cardCVCEditText.getEditableText()) {
                if (s.length() > CARD_CVC_TOTAL_SYMBOLS) {
                    s.delete(CARD_CVC_TOTAL_SYMBOLS, s.length());

                }

            }


        }
    }

}
