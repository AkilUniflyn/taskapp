package app.service.booking.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import app.service.booking.R;

/**
 * Created by: Anton Shkurenko (tonyshkurenko)
 * Project: ClusterManagerDemo
 * Date: 6/7/16
 * Code style: SquareAndroid (https://github.com/square/java-code-styles)
 * Follow me: @tonyshkurenko
 */
public class CustomClusterRenderer extends DefaultClusterRenderer<BrowseTaskFragment.StringClusterItem> {

  private final IconGenerator mClusterIconGenerator;
  private final Context mContext;
  private final ImageView mClusterImageView;
  //private final TextView mClusterTextView;
 // private final ImageView circleImageView;
 // private final RatingBar ratingBar;
  private static final Drawable TRANSPARENT_DRAWABLE = new ColorDrawable(Color.TRANSPARENT);

  public CustomClusterRenderer(Context context, GoogleMap map,
                               ClusterManager<BrowseTaskFragment.StringClusterItem> clusterManager) {
    super(context, map, clusterManager);

    mContext = context;
    mClusterIconGenerator = new IconGenerator(mContext.getApplicationContext());


// Make the background of marker transparent
    mClusterIconGenerator.setBackground(TRANSPARENT_DRAWABLE);

    View multiProfile = LayoutInflater.from(mContext).inflate(R.layout.item_cluster_marker, null);

    mClusterIconGenerator.setContentView(multiProfile);
    mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);
   // mClusterTextView = (TextView) multiProfile.findViewById(R.id.text);
   //circleImageView=(ImageView)multiProfile.findViewById(R.id.circle_image);
  // ratingBar=(RatingBar)multiProfile.findViewById(R.id.ratingBar);

  }

  @Override
  protected void onBeforeClusterItemRendered(BrowseTaskFragment.StringClusterItem item,
                                             MarkerOptions markerOptions) {

   mClusterImageView.setImageResource(item.image);
   // mClusterTextView.setText(item.title);
    //Picasso.with(mContext).load(item.image).into(circleImageView);
   // ratingBar.setRating(Float.parseFloat(item.rating));
    try {
      Bitmap icon = mClusterIconGenerator.makeIcon();
      markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
    } catch (Exception e) {
      e.printStackTrace();
    }catch (OutOfMemoryError e){
      e.printStackTrace();
    }

   /* final BitmapDescriptor markerDescriptor =
        BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE);
    final BitmapDescriptor j=BitmapDescriptorFactory.fromResource(item.draw);
    markerOptions.icon(j).snippet(item.getTitle());*/
  }

  @Override
  protected void onBeforeClusterRendered(Cluster<BrowseTaskFragment.StringClusterItem> cluster,
                                         MarkerOptions markerOptions) {

    //mClusterIconGenerator.setBackground(ContextCompat.getDrawable(mContext, R.drawable.background_circle));

    //mClusterIconGenerator.setTextAppearance(R.style.AppTheme_WhiteTextAppearance);



    /*final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));*/
  }
}