package app.service.booking.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;

import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.activity.CotactUsActivity;
import app.service.booking.activity.HelpandSupportActivity;
import app.service.booking.activity.LoginActivity;
import app.service.booking.activity.PrivacyPolicyActivity;
import app.service.booking.activity.TermsAndConditionActivity;
import app.service.booking.activity.UserProfileActivity;
import app.service.booking.model.api.CommonResponse;
import app.service.booking.utils.GlobalMethods;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class SettingFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.notification_switch)
    Switch notificationSwitch;
    @BindView(R.id.linearChangePswd)
    LinearLayout linearChangePswd;
    @BindView(R.id.linearReferFriends)
    LinearLayout linearReferFriends;
    @BindView(R.id.linearHelpAndSupport)
    LinearLayout linearHelpAndSupport;
    @BindView(R.id.linearContactUs)
    LinearLayout linearContactUs;
    @BindView(R.id.linearTermsandCondition)
    LinearLayout linearTermsandCondition;
    @BindView(R.id.linearPrivacyPolicy)
    LinearLayout linearPrivacyPolicy;
    @BindView(R.id.linearLogout)
    LinearLayout linearLogout;
    @BindView(R.id.linearProfile)
    LinearLayout linearProfile;
    Unbinder unbinder;
    Dialog dialog;
    ApiService apiService;
    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.txt_joineddate)
    TextView txtJoineddate;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String userid, languageid, notfication, fsname, lastname,joinedate;
    ProgressDialog progressDialog;


    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder = ButterKnife.bind(this, view);
        apiService = RetrofitSingleton.creaservice(ApiService.class);
        userid = PrefConnect.readString(getContext(), PrefConnect.USERID, "");

        fsname = PrefConnect.readString(getContext(), PrefConnect.FIRSTNAME, "");
        lastname = PrefConnect.readString(getContext(), PrefConnect.LASTNAME, "");
        joinedate = PrefConnect.readString(getContext(),PrefConnect.JOINEDDATE,"");
        Log.e("joinedda",joinedate+fsname + " " + lastname);

        String date  = GlobalMethods.Datetwo(joinedate);
        Log.e("dateconvert",date+"::");

        txtName.setText(fsname + " " + lastname);
        txtJoineddate.setText(date);


        SharedPreferences prefs = getActivity().getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        languageid = prefs.getString("languageid", "1");//"No name defined" is the default value.
        Log.e("mainlanguid", languageid);


        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    callnotication("1");
                } else {
                    callnotication("0");
                }
            }
        });
        notfication = PrefConnect.readString(getContext(), PrefConnect.NOTIFICATION, "");
        Log.e("notificatiost",notfication);

        if (notfication.equalsIgnoreCase("0")) {
            notificationSwitch.setChecked(false);
        } else {
            notificationSwitch.setChecked(true);
        }

        return view;
    }

    private void callnotication(final String s) {
        Call<CommonResponse> call = apiService.callNotificstion(languageid, userid, s);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Log.e("suceesnoti", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    CommonResponse resp = response.body();
                    if (resp != null) {
                        String status = resp.getStatus();
                        PrefConnect.writeString(getContext(), PrefConnect.NOTIFICATION, s);
                        if (status.equalsIgnoreCase("1")) {


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

            }
        });
    }

    private void callLogout() {
        Call<CommonResponse> call = apiService.callLogout(languageid, userid);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Log.e("sucesslogout", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    CommonResponse resp = response.body();
                    if (resp != null) {

                        String status = resp.getStatus();
                        Intent intentlogin = new Intent(getContext(), LoginActivity.class);
                        intentlogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intentlogin);


                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.linearProfile, R.id.linearChangePswd, R.id.linearReferFriends, R.id.linearHelpAndSupport, R.id.linearContactUs, R.id.linearTermsandCondition, R.id.linearPrivacyPolicy, R.id.linearLogout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linearProfile:
                Intent intent = new Intent(getContext(), UserProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.linearChangePswd:
                dialog = new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_change_password);
                // camera_dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Button btn_cancel = (Button) dialog.findViewById(R.id.btnChangePswd);
                final EditText edtOldPswd = (EditText) dialog.findViewById(R.id.edt_oldPswd);
                final EditText edtNewPeswd = (EditText) dialog.findViewById(R.id.edt_newPswd);
                final EditText edtConfrmPswd = (EditText) dialog.findViewById(R.id.edt_confirmPswd);
                dialog.show();
                btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String oldpswd = edtOldPswd.getText().toString();
                        String newpswd = edtNewPeswd.getText().toString();
                        String confirmpswd = edtConfrmPswd.getText().toString();
                        boolean cancel = false;

                        if (TextUtils.isEmpty(oldpswd)) {
                            cancel = true;
                            GlobalMethods.Toast(getContext(), "Old password is empty ");
                        } else if (TextUtils.isEmpty(newpswd)) {
                            cancel = true;
                            GlobalMethods.Toast(getContext(), "New password is empty ");
                        } else if (TextUtils.isEmpty(newpswd)) {
                            cancel = true;
                            GlobalMethods.Toast(getContext(), "Confirm password is empty ");
                        } else if (!newpswd.equalsIgnoreCase(confirmpswd)) {
                            cancel = true;
                            GlobalMethods.Toast(getContext(), " Password mismatch ");
                        }
                        if (!cancel) {
                            if (GlobalMethods.isNetworkAvailable(getContext())) {
                                dialog.dismiss();
                                callChangePswd(oldpswd, newpswd);
                            } else {
                                GlobalMethods.Toast(getContext(), getString(R.string.internet));
                            }
                        }

                        //dialog.dismiss();
                    }
                });

                break;
            case R.id.linearReferFriends:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "Service Booking");
                // String sAux = "\n Glo App Invite code : "+ tripcode +"\n\n";
                //sAux = sAux + "https://play.google.com/store/apps/details?id=the.package.id \n\n";
                i.putExtra(Intent.EXTRA_TEXT, "Service Booking");
                startActivity(Intent.createChooser(i, "Share Invite..."));
                break;
            case R.id.linearHelpAndSupport:
                Intent intenthhelp = new Intent(getContext(), HelpandSupportActivity.class);
                startActivity(intenthhelp);
                break;
            case R.id.linearContactUs:
                Intent intentcontactus = new Intent(getContext(), CotactUsActivity.class);
                startActivity(intentcontactus);
                break;
            case R.id.linearTermsandCondition:
                Intent intentterms = new Intent(getContext(), TermsAndConditionActivity.class);
                startActivity(intentterms);
                break;
            case R.id.linearPrivacyPolicy:
                Intent intentprivacy = new Intent(getContext(), PrivacyPolicyActivity.class);
                startActivity(intentprivacy);
                break;
            case R.id.linearLogout:
                Dialog dialoglogout = new AlertDialog.Builder(getContext())

                        .setMessage(getString(R.string.logout_toast))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callLogout();

                            }
                        })
                        .setNegativeButton(getString(R.string.no), null)
                        .show();
                break;
        }
    }

    private void callChangePswd(String oldpswd, String newpswd) {
        progressDialog = ProgressDialog.show(getContext(), "", "Loading....", false);

        Call<CommonResponse> call = apiService.callChangePassword(languageid, userid, oldpswd, newpswd);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Log.e("sucesslogout", new Gson().toJson(response.body()));
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    CommonResponse resp = response.body();
                    if (resp != null) {

                        String status = resp.getStatus();

                        if (status.equalsIgnoreCase("1")) {


                        }


                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                progressDialog.dismiss();

            }
        });

    }
}
