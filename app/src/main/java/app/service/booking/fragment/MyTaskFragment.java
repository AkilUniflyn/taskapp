package app.service.booking.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.subFragment.AllFragment;
import app.service.booking.subFragment.AssignedFragment;
import app.service.booking.subFragment.OfferPostedFragment;
import app.service.booking.subFragment.PostedFragment;
import app.service.booking.subFragment.SummaryFragment;
import app.service.booking.subFragment.SummaryPosterFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link MyTaskFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyTaskFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    Unbinder unbinder;
    @BindView(R.id.linear_as_tasker)
    LinearLayout linearAsTasker;
    @BindView(R.id.linear_as_poster)
    LinearLayout linearAsPoster;
    @BindView(R.id.txt_as_tasker)
    TextView txtAsTasker;
    @BindView(R.id.txt_as_poster)
    TextView txtAsPoster;
    @BindView(R.id.linearTasker)
    LinearLayout linearTasker;
    @BindView(R.id.tabsPost)
    TabLayout tabsPost;
    @BindView(R.id.viewpagerPost)
    ViewPager viewpagerPost;
    @BindView(R.id.linearPoster)
    LinearLayout linearPoster;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ViewPagerAdapter adapter;
    int[] tabicon = {R.drawable.mytask_all, R.drawable.mytask_post, R.drawable.mytask_assigned, R.drawable.mytask_offerpost};
    ViewPagerAdapterPost viewPagerAdapterPost;

    public MyTaskFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyTaskFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyTaskFragment newInstance(String param1, String param2) {
        MyTaskFragment fragment = new MyTaskFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_task, container, false);
        unbinder = ButterKnife.bind(this, view);

        tabs.setSelectedTabIndicatorColor(Color.parseColor("#FFE04C4B"));
        tabs.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        tabs.setTabTextColors(Color.parseColor("#FFF4F4F4"), Color.parseColor("#ffffff"));

        tabsPost.setSelectedTabIndicatorColor(Color.parseColor("#FFE04C4B"));
        tabsPost.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        tabsPost.setTabTextColors(Color.parseColor("#FFF4F4F4"), Color.parseColor("#ffffff"));


        setupViewPager(viewpager);
        tabs.setupWithViewPager(viewpager);

        setupViewPagerPoster(viewpagerPost);
        tabsPost.setupWithViewPager(viewpagerPost);

        for (int i = 0; i < tabs.getTabCount(); i++) {
            TabLayout.Tab tab = tabs.getTabAt(i);
            tab.setCustomView(adapter.getTabView(i));
            Log.e("tabview", i + "::");
        }

        for (int i = 0; i < tabsPost.getTabCount(); i++) {
            TabLayout.Tab tab = tabsPost.getTabAt(i);
            tab.setCustomView(viewPagerAdapterPost.getTabView(i));
            Log.e("tabview", i + "::");
        }
        //setupTabIcons();
        return view;
    }

    private void setupViewPager(ViewPager viewpager) {

        adapter = new ViewPagerAdapter(getChildFragmentManager());

        adapter.addFragment(new SummaryFragment());
        adapter.addFragment(new AllFragment());
        adapter.addFragment(new PostedFragment());
        adapter.addFragment(new OfferPostedFragment());

      /*  adapter.addFragment(new AssignedFragment());
        adapter.addFragment(new OfferPostedFragment());
        adapter.addFragment(new AllFragment());*/
        adapter.addFragment(new AllFragment());

        viewpager.setAdapter(adapter);


    }

    private void setupViewPagerPoster(ViewPager viewpager) {

         viewPagerAdapterPost = new ViewPagerAdapterPost(getChildFragmentManager());

         viewPagerAdapterPost.addFragment(new SummaryPosterFragment());
         viewPagerAdapterPost.addFragment(new AllFragment());
         viewPagerAdapterPost.addFragment(new PostedFragment());
         viewPagerAdapterPost.addFragment(new AllFragment());
        viewPagerAdapterPost.addFragment(new AllFragment());
//         viewPagerAdapterPost.addFragment(new AllFragment());

         viewpager.setAdapter(viewPagerAdapterPost);


    }

    private void setupTabIcons() {
        tabs.getTabAt(0).setIcon(tabicon[0]);
        tabs.getTabAt(1).setIcon(tabicon[1]);
        tabs.getTabAt(2).setIcon(tabicon[2]);
        tabs.getTabAt(3).setIcon(tabicon[3]);
    }

    @OnClick({R.id.linear_as_tasker, R.id.linear_as_poster})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linear_as_tasker:
             /*   viewpager.setCurrentItem(0, true);
                setupViewPager(viewpager);
                tabs.setupWithViewPager(viewpager);*/
                tabs.setSelectedTabIndicatorColor(Color.parseColor("#FFE04C4B"));
                tabs.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
                tabs.setTabTextColors(Color.parseColor("#FFF4F4F4"), Color.parseColor("#ffffff"));

                linearAsTasker.setBackgroundColor(getContext().getResources().getColor(R.color.blue_dark));
                linearAsPoster.setBackgroundColor(getContext().getResources().getColor(R.color.blue));
                linearTasker.setVisibility(View.VISIBLE);
                linearPoster.setVisibility(View.GONE);
                // txtAsTasker.setTextColor(getContext().getResources().getColor(R.color.colorBlack));
                // txtAsPoster.setTextColor(getContext().getResources().getColor(R.color.colorWhite));
                break;
            case R.id.linear_as_poster:

        tabsPost.setSelectedTabIndicatorColor(Color.parseColor("#FFE04C4B"));
        tabsPost.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        tabsPost.setTabTextColors(Color.parseColor("#FFF4F4F4"), Color.parseColor("#ffffff"));

                linearPoster.setVisibility(View.VISIBLE);
                linearTasker.setVisibility(View.GONE);
                linearAsTasker.setBackgroundColor(getContext().getResources().getColor(R.color.blue));
                linearAsPoster.setBackgroundColor(getContext().getResources().getColor(R.color.blue_dark));
                //txtAsTasker.setTextColor(getContext().getResources().getColor(R.color.colorWhite));
                //txtAsPoster.setTextColor(getContext().getResources().getColor(R.color.colorBlack));
                break;
        }
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        private String tabTitles[] = new String[]{"TASKER SUMMARY", "ALL TASK", "ONGOING TASK", "OFFER PENDING", "TASK COMPLETED"};
        private int[] imageResId = {R.drawable.mytask_all, R.drawable.mytask_post, R.drawable.mytask_assigned, R.drawable.mytask_offerpost};


        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(getContext()).inflate(R.layout.custom_tab, null);
            TextView tv = (TextView) v.findViewById(R.id.textView);
            tv.setText(tabTitles[position]);
            ImageView img = (ImageView) v.findViewById(R.id.imgView);
            // img.setImageResource(imageResId[position]);
            return v;
        }

        @Override
        public Fragment getItem(int position) {
            // PrefConnect.writeString(getContext(), PrefConnect.CATEGORY_ID, resp.getCategories().get(position).getCategoryId());

            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
//            mFragmentTitleList.add(title);
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return super.getItemPosition(object);
        }
        /*  @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }*/
/*
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            mFragmentList.remove(position);
            super.destroyItem(container, position, object);
        }*/
    }

    public class ViewPagerAdapterPost extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        private String tabTitles[] = new String[]{"TASKER SUMMARY", "ALL TASK","POSTED TASK", "ONGOING TASK", "TASK COMPLETED"};
        private int[] imageResId = {R.drawable.mytask_all, R.drawable.mytask_post, R.drawable.mytask_assigned, R.drawable.mytask_offerpost};


        public ViewPagerAdapterPost(FragmentManager manager) {
            super(manager);
        }

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(getContext()).inflate(R.layout.custom_tab, null);
            TextView tv = (TextView) v.findViewById(R.id.textView);
            tv.setText(tabTitles[position]);
            ImageView img = (ImageView) v.findViewById(R.id.imgView);
            // img.setImageResource(imageResId[position]);
            return v;
        }

        @Override
        public Fragment getItem(int position) {
            // PrefConnect.writeString(getContext(), PrefConnect.CATEGORY_ID, resp.getCategories().get(position).getCategoryId());

            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
//            mFragmentTitleList.add(title);
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return super.getItemPosition(object);
        }
        /*  @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }*/
/*
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            mFragmentList.remove(position);
            super.destroyItem(container, position, object);
        }*/
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
