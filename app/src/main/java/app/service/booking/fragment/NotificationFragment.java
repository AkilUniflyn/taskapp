package app.service.booking.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.adapter.NotificationAdapter;
import app.service.booking.model.NotificationModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class NotificationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.recycler_notification)
    RecyclerView recyclerNotification;
    Unbinder unbinder;
    NotificationAdapter notificationAdapter;
    List<NotificationModel> notification_list;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    public NotificationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NotificationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotificationFragment newInstance(String param1, String param2) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_notification, container, false);
        unbinder = ButterKnife.bind(this, view);
        notification_list = new ArrayList<>();
        notification_list.add(new NotificationModel(R.drawable.notification, "Today", "Dec 24", "Offer Request", "You have got an offer from Mr.John for your post on cleaning it out.."));
        notification_list.add(new NotificationModel(R.drawable.notification, "Today", "Dec 24", "Offer Request", "You have got an offer from Mr.John for your post on cleaning it out.."));
        notification_list.add(new NotificationModel(R.drawable.notification, "Today", "Dec 24", "Offer Request", "You have got an offer from Mr.John for your post on cleaning it out.."));
        notificationAdapter = new NotificationAdapter(getContext(), notification_list);
        recyclerNotification.setAdapter(notificationAdapter);
        recyclerNotification.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
