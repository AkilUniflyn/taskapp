package app.service.booking.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.activity.SendOfferActivity;
import app.service.booking.adapter.AllAdapter;
import app.service.booking.model.MyTaskModel;
import app.service.booking.model.api.BrowsTask;
import app.service.booking.utils.GlobalMethods;
import app.service.booking.utils.PrefConnect;
import app.service.booking.utils.Tracker;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static app.service.booking.activity.BrowsFilterActivity.doneby;
import static app.service.booking.activity.BrowsFilterActivity.location;
import static app.service.booking.activity.BrowsFilterActivity.log;
import static app.service.booking.activity.BrowsFilterActivity.maxprice;
import static app.service.booking.activity.BrowsFilterActivity.minprice;
import static app.service.booking.activity.BrowsFilterActivity.opentask;
import static app.service.booking.activity.BrowsFilterActivity.page;
import static app.service.booking.activity.BrowsFilterActivity.sortype;
import static app.service.booking.activity.BrowsFilterActivity.distance;
import static app.service.booking.activity.BrowsFilterActivity.lat;
import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;

public class BrowseTaskFragment extends Fragment implements OnMapReadyCallback, Tracker.LocationCallback {

    View root_view;
    @BindView(R.id.linear_map)
    LinearLayout linearMap;
    @BindView(R.id.linear_list)
    LinearLayout linearList;
    @BindView(R.id.view_map)
    View viewMap;
    @BindView(R.id.view_list)
    View viewList;
    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.recycleList)
    RecyclerView recycleList;
    Unbinder unbinder;
    @BindView(R.id.linearView)
    LinearLayout linearView;
    @BindView(R.id.linearbrowstask)
    LinearLayout linearbrowstask;

    @BindView(R.id.lineBrowsMap)
    LinearLayout lineBrowsMap;
    @BindView(R.id.linearBrowsList)
    LinearLayout linearBrowsList;

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    @BindView(R.id.edt_search)
    EditText edtSearch;
    private Tracker tracker;
    GoogleMap my_map;

    LatLng current_latLng;
    Double latitude, longitude;
    int count;
    AllAdapter allAdapter;
    List<MyTaskModel> myTaskModels;
    ApiService apiService;
    BrowsTask.Datum.TaskDetail taskDetails;

    List<BrowsTask.Datum> datumList;

    private ClusterManager<StringClusterItem> mClusterManager;
    String userid,languageid,searchkey=" ";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_browse_task, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        tracker = new Tracker(getActivity(), this);
        mapView.onCreate(savedInstanceState);

        userid = PrefConnect.readString(getContext(), PrefConnect.USERID, "");

        SharedPreferences prefs = getActivity().getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        languageid = prefs.getString("languageid", "1");//"No name defined" is the default value.
        Log.e("loginlanguid", languageid);


        apiService = RetrofitSingleton.creaservice(ApiService.class);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()>0) {
                    searchkey = s.toString();

                    if (GlobalMethods.isNetworkAvailable(getContext())) {
                        callBrowsTask();
                    } else {
                        GlobalMethods.Toast(getContext(), getString(R.string.internet));
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


      /*  myTaskModels = new ArrayList<>();
        myTaskModels.add(new MyTaskModel("Seat Belts Fitted", "Beijing China", "Dec 25,2018", "40", "12 offers"));
        myTaskModels.add(new MyTaskModel("Seat Belts Fitted", "Beijing China", "Dec 25,2018", "40", "12 offers"));
        myTaskModels.add(new MyTaskModel("Seat Belts Fitted", "Beijing China", "Dec 25,2018", "40", "12 offers"));

        allAdapter = new AllAdapter(myTaskModels, getContext(), "1");
        recycleList.setLayoutManager(new LinearLayoutManager(getContext()));
        recycleList.setAdapter(allAdapter);*/
        setGoogleMap();
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            callMultiplePermissions();
        } else {
            // Pre-Marshmallow
        }


        return root_view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!TextUtils.isEmpty(location)){
            Log.e("onstart",location+"::"+lat+"::"+log+"::"+maxprice+"::"+distance+"::"+sortype+"::"+opentask);
        }

    }

    private void callBrowsTask() {


        Log.e("browstaskreques", languageid + "::" + location + "::" + latitude + "::" + longitude + "::" + distance + "::" + minprice + "::" + maxprice + "::" + sortype + "::" + opentask + "::" + doneby + "::" + userid + "::" + searchkey + "::" + page);

        Call<BrowsTask> call = apiService.callBrowstask(languageid, location, lat, log, distance, minprice, maxprice, sortype, opentask, doneby, userid, searchkey, page);
        call.enqueue(new Callback<BrowsTask>() {
            @Override
            public void onResponse(Call<BrowsTask> call, Response<BrowsTask> response) {
                Log.e("sucessrespose", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    BrowsTask resp = response.body();
                    if (resp != null) {
                        String status = resp.getStatus();
                        if (status.equalsIgnoreCase("1")) {
                            datumList = resp.getData();

                            if (datumList.size() > 0) {
                                for (int i = 0; i < datumList.size(); i++) {
                                    double lat = Double.parseDouble(datumList.get(i).getTaskDetail().getLat());
                                    double log = Double.parseDouble(datumList.get(i).getTaskDetail().getLon());

                                    current_latLng = new LatLng(lat,
                                            log);


                                    mClusterManager.addItem(new StringClusterItem(i,
                                            "nan",
                                            current_latLng,
                                            R.mipmap.map_marker_icon,
                                            "0",
                                            i + ""));

                                }

                                allAdapter = new AllAdapter(datumList, getContext(), "1");
                                recycleList.setLayoutManager(new LinearLayoutManager(getContext()));
                                recycleList.setAdapter(allAdapter);
                            }

                        }
                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call<BrowsTask> call, Throwable t) {
                Log.e("failure", t.getMessage());

            }
        });
    }

    private void callMultiplePermissions() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
       /* if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("NETWORK STATE");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("WRITE EXTERNAL STORAGE");*/

        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("ACCESS COARSE LOCATION");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("ACCESS FINE LOCATION");


        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                } else {
                    // Pre-Marshmallow
                }

                return;
            }
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            } else {
                // Pre-Marshmallow
            }

            return;
        }

    }

    /**
     * add Permissions
     *
     * @param permissionsList
     * @param permission
     * @return
     */
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (getActivity().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        } else {
            // Pre-Marshmallow
        }

        return true;
    }

    /**
     * Permissions results
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                // Initial
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION and others

              /*  perms.get(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        &&*/

                if (perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted

                } else {
                    // Permission Denied
                    Toast.makeText(getContext(), "Permission is Denied", Toast.LENGTH_SHORT)
                            .show();

                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @OnClick({R.id.linear_map, R.id.linear_list})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linear_map:
                viewMap.setBackgroundColor(getResources().getColor(R.color.red));
                viewList.setBackgroundColor(getResources().getColor(R.color.transparent));
                lineBrowsMap.setVisibility(View.VISIBLE);
                linearBrowsList.setVisibility(View.GONE);
                break;
            case R.id.linear_list:
                viewMap.setBackgroundColor(getResources().getColor(R.color.transparent));
                viewList.setBackgroundColor(getResources().getColor(R.color.red));
                lineBrowsMap.setVisibility(View.GONE);
                linearBrowsList.setVisibility(View.VISIBLE);
                break;

        }
    }

    @Override
    public void onLocationReceived(Location location) {

        if (location != null) {
            try {

                if (count == 0) {
                    Log.e("Current Lat", location.getLatitude() + " nan");
                    Log.e("Current Long", location.getLongitude() + " nan");
                    current_latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    my_map.animateCamera(CameraUpdateFactory.newLatLngZoom(current_latLng, 6));

                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    getAddress(latitude, longitude);


                    if (GlobalMethods.isNetworkAvailable(getContext())) {
                        callBrowsTask();
                    } else {
                        GlobalMethods.Toast(getContext(), getString(R.string.internet));
                    }


                   /* for (int i = 0; i < 5; i++) {

                        current_latLng = new LatLng(location.getLatitude() + i,
                                location.getLongitude() + i);


                        mClusterManager.addItem(new StringClusterItem(i,
                                "nan",
                                current_latLng,
                                R.mipmap.map_marker_icon,
                                "0",
                                i + ""));
                        Log.e("i am in", "if" + i);
                    }*/

                    count++;

                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            tracker.stopLocationUpdates();
        }

    }

    private String getAddress(double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                result.append(address.getLocality()).append("\n");
                result.append(address.getCountryName());

                location = address.getAddressLine(0);
                Log.e("location", location);
            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return result.toString();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        my_map = googleMap;

        mClusterManager = new ClusterManager<>(getActivity(), googleMap);

        my_map.getUiSettings().setMapToolbarEnabled(false);
        my_map.getUiSettings().setZoomControlsEnabled(false);

        final CustomClusterRenderer renderer = new CustomClusterRenderer(getContext(), googleMap, mClusterManager);
        mClusterManager.setRenderer(renderer);

        mClusterManager.setOnClusterItemClickListener(
                new ClusterManager.OnClusterItemClickListener<StringClusterItem>() {
                    @Override
                    public boolean onClusterItemClick(StringClusterItem clusterItem) {

                        Log.e("provider_id", clusterItem.getProvider_id() + " " + clusterItem.position);

                        current_latLng = new LatLng(latitude, longitude);
                        my_map.animateCamera(CameraUpdateFactory.newLatLngZoom(current_latLng, 8 + 0.001f));

                        Intent intent = new Intent(getContext(), SendOfferActivity.class);
                        startActivity(intent);

                        return false;
                    }
                });

        googleMap.setOnCameraIdleListener(mClusterManager);
        googleMap.setOnMarkerClickListener(mClusterManager);
        mClusterManager.cluster();
    }

    private void setGoogleMap() {

        mapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(this.getActivity());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Map error", e.getMessage() + "nsn");
        }
        mapView.getMapAsync(this);
    }

    public class StringClusterItem implements ClusterItem {

        final String title;
        final LatLng latLng;
        final int position;
        final int image;
        final String rating;
        final String provider_id;

        public StringClusterItem(int position, String title, LatLng latLng, int image, String rating, String provider_id) {
            this.title = title;
            this.latLng = latLng;
            this.position = position;
            this.image = image;
            this.rating = rating;
            this.provider_id = provider_id;
        }

        @Override
        public LatLng getPosition() {
            return latLng;
        }

        @Override
        public String getTitle() {
            return null;
        }

        @Override
        public String getSnippet() {
            return null;
        }


        public LatLng getLatLng() {
            return latLng;
        }

        public String getProvider_id() {
            return provider_id;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        tracker.connectClient();
        if (mapView != null) {
            mapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        tracker.disConnectClient();
        if (mapView != null) {
            mapView.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null) {
            mapView.onDestroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null) {
            mapView.onLowMemory();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mapView != null) {
            unbinder.unbind();
        }


    }


}
