package app.service.booking.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.adapter.MessageAdapter;
import app.service.booking.model.Message;
import app.service.booking.model.api.ChatList;
import app.service.booking.utils.GlobalMethods;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link MessageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MessageFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recycleViewMesage)
    RecyclerView recycleViewMesage;
    Unbinder unbinder;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    MessageAdapter messageAdapter;
    List<Message> messageList;
    String userId,languageid;
    ApiService apiService;
    List<ChatList.Datum> datumList;



    public MessageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MessageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MessageFragment newInstance(String param1, String param2) {
        MessageFragment fragment = new MessageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_message, container, false);
        unbinder = ButterKnife.bind(this, view);

        apiService = RetrofitSingleton.creaservice(ApiService.class);

        userId = PrefConnect.readString(getContext(),PrefConnect.USERID,"");
        datumList = new ArrayList<>();
        SharedPreferences prefs = getActivity().getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        languageid = prefs.getString("languageid", "1");//"No name defined" is the default value.
        Log.e("messagelanguid", languageid);

        if(GlobalMethods.isNetworkAvailable(getContext())){
            callChatList();
        }else {
            GlobalMethods.Toast(getContext(),getString(R.string.internet));
        }

        messageList = new ArrayList<>();

      /*  messageList.add(new Message("James Jhonsons","20 mins ago",R.drawable.count_icon_blue,"4"));
        messageList.add(new Message("Chelsea","20 mins ago",R.drawable.count_icon_orange,"2"));
        messageList.add(new Message("Fueles Team","20 mins ago",R.drawable.count_icon_green,"1"));
        messageList.add(new Message("joe","20 mins ago",R.drawable.count_icon_red,"6"));

        messageAdapter = new MessageAdapter(getContext(),messageList);
        recycleViewMesage.setAdapter(messageAdapter);
        recycleViewMesage.setLayoutManager(new LinearLayoutManager(getContext()));*/

      //  getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    private void callChatList() {
        Call<ChatList> call = apiService.callChat(userId,languageid);
        call.enqueue(new Callback<ChatList>() {
            @Override
            public void onResponse(Call<ChatList> call, Response<ChatList> response) {
                Log.e("successs",new Gson().toJson(response.body()));
                if(response.isSuccessful()){
                    ChatList resp = response.body();
                    if(resp!=null){
                        String status = resp.getStatus();
                        if(status.equalsIgnoreCase("1")){
                            datumList = resp.getData();
                            if(datumList.size()>0){

                                messageAdapter = new MessageAdapter(getContext(),datumList);
                                recycleViewMesage.setAdapter(messageAdapter);
                                recycleViewMesage.setLayoutManager(new LinearLayoutManager(getContext()));

                            }

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatList> call, Throwable t) {

            }
        });
    }

}
