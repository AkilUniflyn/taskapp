package app.service.booking.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.activity.AddCardActivity;
import app.service.booking.activity.AddYourBankDetailsActivity;
import app.service.booking.adapter.AmountSpenErandAdapter;
import app.service.booking.adapter.CardAdapter;
import app.service.booking.model.CardModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link PaymentHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PaymentHistoryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.linearAmontTranfer)
    LinearLayout linearAmontTranfer;
    @BindView(R.id.recycleCard)
    RecyclerView recycleCard;
    @BindView(R.id.txt_amount_eraned)
    TextView txtAmountEraned;
    @BindView(R.id.linear_amount_erand)
    LinearLayout linearAmountErand;
    @BindView(R.id.txt_amount_spent)
    TextView txtAmountSpent;
    @BindView(R.id.linear_amount_spend)
    LinearLayout linearAmountSpend;
    @BindView(R.id.recycleAmountHistory)
    RecyclerView recycleAmountHistory;
    Unbinder unbinder;
    List<CardModel> cardModelList;
    List<CardModel> amountHistory;
    CardAdapter cardAdapter;
    AmountSpenErandAdapter amountSpenErandAdapter;
    @BindView(R.id.btn_add)
    Button btnAdd;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public PaymentHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PaymentHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PaymentHistoryFragment newInstance(String param1, String param2) {
        PaymentHistoryFragment fragment = new PaymentHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_history, container, false);
        unbinder = ButterKnife.bind(this, view);
        cardModelList = new ArrayList<>();
        amountHistory = new ArrayList<>();
        cardModelList.add(new CardModel(R.drawable.visa, "1234 5644 7888 9666"));
        cardModelList.add(new CardModel(R.drawable.mastercard, "8785 2221 7667 7222"));
        cardModelList.add(new CardModel(R.drawable.visa, "4578 7888 7863 2213"));
        cardModelList.add(new CardModel(R.drawable.mastercard, "5453 5333 7888 9666"));
        cardAdapter = new CardAdapter(cardModelList, getContext());
        recycleCard.setLayoutManager(new LinearLayoutManager(getContext()));;
        recycleCard.setAdapter(cardAdapter);

        amountHistory.add(new CardModel(R.drawable.placeholder, "Cleaning"));
        amountHistory.add(new CardModel(R.drawable.placeholder, "Computer"));
        amountHistory.add(new CardModel(R.drawable.placeholder, "Removals"));
        amountHistory.add(new CardModel(R.drawable.placeholder, "Two Wheeler"));
        amountSpenErandAdapter = new AmountSpenErandAdapter(amountHistory, getContext());
        recycleAmountHistory.setAdapter(amountSpenErandAdapter);
        recycleAmountHistory.setLayoutManager(new LinearLayoutManager(getContext()));


        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_add,R.id.linearAmontTranfer, R.id.linear_amount_erand, R.id.linear_amount_spend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_add:
                Intent login = new Intent(getContext(), AddCardActivity.class);
                login.putExtra("page","1");
                startActivity(login);
                break;
            case R.id.linearAmontTranfer:

                final Dialog amounttransfer = new Dialog(getContext());
                amounttransfer.requestWindowFeature(Window.FEATURE_NO_TITLE);
                amounttransfer.setContentView(R.layout.dialog_amount_transfer);
                amounttransfer.setCancelable(false);
                amounttransfer.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                amounttransfer.show();
                Button btnok = (Button)amounttransfer.findViewById(R.id.btn_ok);
                btnok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        amounttransfer.dismiss();
                    }
                });
                break;
            case R.id.linear_amount_erand:
                amountHistory = new ArrayList<>();
                linearAmountErand.setBackground(getContext().getResources().getDrawable(R.drawable.selcted_blue_fill_left));
                linearAmountSpend.setBackground(getContext().getResources().getDrawable(R.drawable.unselcted_blue_line_right));

                txtAmountEraned.setTextColor(getContext().getResources().getColor(R.color.colorWhite));
                txtAmountSpent.setTextColor(getContext().getResources().getColor(R.color.colorBlack));

                amountHistory.add(new CardModel(R.drawable.placeholder, "Cleaning"));
                amountHistory.add(new CardModel(R.drawable.placeholder, "Computer"));
                amountHistory.add(new CardModel(R.drawable.placeholder, "Removals"));
                amountHistory.add(new CardModel(R.drawable.placeholder, "Two Wheeler"));
                amountSpenErandAdapter = new AmountSpenErandAdapter(amountHistory, getContext());
                recycleAmountHistory.setAdapter(amountSpenErandAdapter);
                recycleAmountHistory.setLayoutManager(new LinearLayoutManager(getContext()));
                break;
            case R.id.linear_amount_spend:
                amountHistory = new ArrayList<>();
                linearAmountErand.setBackground(getContext().getResources().getDrawable(R.drawable.unselcted_blue_line_left));
                linearAmountSpend.setBackground(getContext().getResources().getDrawable(R.drawable.selcted_blue_fill_right));
                txtAmountEraned.setTextColor(getContext().getResources().getColor(R.color.colorBlack));
                txtAmountSpent.setTextColor(getContext().getResources().getColor(R.color.colorWhite));

                amountHistory.add(new CardModel(R.drawable.placeholder, "Cleaning"));
                amountHistory.add(new CardModel(R.drawable.placeholder, "Computer"));
                amountHistory.add(new CardModel(R.drawable.placeholder, "Removals"));
                amountHistory.add(new CardModel(R.drawable.placeholder, "Two Wheeler"));  amountSpenErandAdapter = new AmountSpenErandAdapter(amountHistory, getContext());
                recycleAmountHistory.setAdapter(amountSpenErandAdapter);
                recycleAmountHistory.setLayoutManager(new LinearLayoutManager(getContext()));
                break;
        }
    }
}
