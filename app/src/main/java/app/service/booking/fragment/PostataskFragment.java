package app.service.booking.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.activity.PostTaskStep1;
import app.service.booking.activity.PostingNewTaskActivity;
import app.service.booking.adapter.DashboardAdapter;
import app.service.booking.model.DashboardModel;
import app.service.booking.model.api.Dashboard;
import app.service.booking.utils.GlobalMethods;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link PostataskFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PostataskFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.btn_go)
    Button btnGo;
    Unbinder unbinder;
    DashboardAdapter dashboardAdapter;
    DashboardAdapter.CallBack callBack;
    List<DashboardModel> list_dashboard;
    @BindView(R.id.recycler_dashboard)
    RecyclerView recyclerDashboard;
    ApiService apiService;
    String languageid;

    List<Dashboard.Datum> datumList;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public PostataskFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PostataskFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PostataskFragment newInstance(String param1, String param2) {
        PostataskFragment fragment = new PostataskFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_postatask, container, false);
        unbinder = ButterKnife.bind(this, view);

        apiService = RetrofitSingleton.creaservice(ApiService.class);
        SharedPreferences prefs = getActivity().getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        languageid = prefs.getString("languageid", "1");//"No name defined" is the default value.
        Log.e("dashlanguid",languageid);

        if(GlobalMethods.isNetworkAvailable(getContext())){
            callDashboard();
        }else {
            GlobalMethods.Toast(getContext(),getString(R.string.internet));
        }


        callBack = new DashboardAdapter.CallBack() {
            @Override
            public void call() {
                Intent new_task=new Intent(getContext(), PostingNewTaskActivity.class);
                startActivity(new_task);
            }
        };



        return view;
    }

    private void callDashboard() {
        datumList = new ArrayList<>();
        Call<Dashboard> call = apiService.callDashboardList(languageid);
        call.enqueue(new Callback<Dashboard>() {
            @Override
            public void onResponse(Call<Dashboard> call, Response<Dashboard> response) {
                Log.e("successdash",new Gson().toJson(response.body()));
                if(response.isSuccessful()){
                    Dashboard resp = response.body();
                    if(resp!=null){
                        String status = resp.getStatus();
                        if(status.equalsIgnoreCase("1")){
                            datumList = resp.getData();
                            if(datumList.size()>0){
                                dashboardAdapter = new DashboardAdapter(getContext(), datumList,callBack);
                                recyclerDashboard.setLayoutManager(new GridLayoutManager(getContext(), 2));
                                recyclerDashboard.setAdapter(dashboardAdapter);
                            }

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Dashboard> call, Throwable t) {

            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

   /* @OnClick(R.id.btn_go)
    public void onViewClicked() {
        Intent new_task=new Intent(getContext(), PostTaskStep1.class);
        startActivity(new_task);
    }*/
}
