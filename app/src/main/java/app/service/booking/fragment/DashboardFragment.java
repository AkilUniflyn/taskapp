package app.service.booking.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.MainActivity;
import app.service.booking.R;
import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.activity.PostTaskStep1;
import app.service.booking.activity.PostingNewTaskActivity;
import app.service.booking.adapter.DashboardAdapter;
import app.service.booking.model.DashboardModel;
import app.service.booking.model.api.Dashboard;
import app.service.booking.utils.GlobalMethods;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;

public class DashboardFragment extends Fragment {

    View root_view;
    @BindView(R.id.btn_post_your_task)
    TextView btnPostYourTask;
    @BindView(R.id.recycler_dashboard)
    RecyclerView recyclerDashboard;
    Unbinder unbinder;

    DashboardAdapter dashboardAdapter;
    DashboardAdapter.CallBack callBack;
    List<DashboardModel> list_dashboard;
    Context context;
    @BindView(R.id.linearDashboard)
    LinearLayout linearDashboard;
    @BindView(R.id.btn_go)
    Button btnGo;
    @BindView(R.id.linearPostTaskHint)
    LinearLayout linearPostTaskHint;
    int status = 0;
    MainActivity mainActivity;
    ApiService apiService;
    String languageid;
    List<Dashboard.Datum> datumList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        context = getContext();

        apiService = RetrofitSingleton.creaservice(ApiService.class);

        SharedPreferences prefs = getActivity().getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        languageid = prefs.getString("languageid", "1");//"No name defined" is the default value.
        Log.e("dashlanguid",languageid);

        if(GlobalMethods.isNetworkAvailable(getContext())){
            callDashboard();
        }else {
            GlobalMethods.Toast(getContext(),getString(R.string.internet));
        }

        mainActivity.btnMenuOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(status==1){
                    mainActivity.imagBack.setImageResource(R.drawable.nav_menu_icon);
                    status=0;
                    linearDashboard.setVisibility(View.VISIBLE);
                    linearPostTaskHint.setVisibility(View.GONE);
                    mainActivity.txtTitle.setText("Dashboard");
                    mainActivity.drawerLayoutNew.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    mainActivity.drawerLayoutNew.closeDrawers();
                    mainActivity.imgMenu.setVisibility(View.VISIBLE);
                    mainActivity.imgMenu.setImageResource(R.drawable.notification_menu);
                }else {
                    mainActivity.drawerLayoutNew.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    mainActivity.drawerLayoutNew.openDrawer(GravityCompat.START);
                }
            }
        });



        callBack = new DashboardAdapter.CallBack() {
            @Override
            public void call() {
                /*mainActivity.imagBack.setImageResource(R.drawable.back_white_icon);
                mainActivity.txtTitle.setText("Posting New Task");
                status=1;
                linearDashboard.setVisibility(View.GONE);
                linearPostTaskHint.setVisibility(View.VISIBLE);
                mainActivity.imgMenu.setVisibility(View.GONE);*/
                Intent new_task=new Intent(getContext(), PostingNewTaskActivity.class);
                startActivity(new_task);
            }
        };


        return root_view;

    }

    private void callDashboard() {
        datumList = new ArrayList<>();
        Call<Dashboard> call = apiService.callDashboardList(languageid);
        call.enqueue(new Callback<Dashboard>() {
            @Override
            public void onResponse(Call<Dashboard> call, Response<Dashboard> response) {
                Log.e("successdash",new Gson().toJson(response.body()));
                if(response.isSuccessful()){
                    Dashboard resp = response.body();
                    if(resp!=null){
                        String status = resp.getStatus();
                        if(status.equalsIgnoreCase("1")){
                            datumList = resp.getData();
                            if(datumList.size()>0){
                                dashboardAdapter = new DashboardAdapter(context, datumList,callBack);
                                recyclerDashboard.setLayoutManager(new GridLayoutManager(context, 2));
                                recyclerDashboard.setAdapter(dashboardAdapter);
                            }

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Dashboard> call, Throwable t) {

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity)context;
    }

    @OnClick({R.id.btn_post_your_task,R.id.btn_go})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_post_your_task:
               /* mainActivity.imagBack.setImageResource(R.drawable.back_white_icon);
                mainActivity.txtTitle.setText("Posting New Task");
                status=1;
                linearDashboard.setVisibility(View.GONE);
                linearPostTaskHint.setVisibility(View.VISIBLE);
                mainActivity.imgMenu.setVisibility(View.GONE);
*/
                /*Intent new_task = new Intent(context, PostingNewTaskActivity.class);
                startActivity(new_task);*/
                break;
            case R.id.btn_go:
                Intent new_task=new Intent(context, PostTaskStep1.class);
                startActivity(new_task);
                break;

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
