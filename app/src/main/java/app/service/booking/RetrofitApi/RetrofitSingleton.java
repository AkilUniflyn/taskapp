package app.service.booking.RetrofitApi;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by uniflyn on 24/7/18.
 */

public class RetrofitSingleton {
    private static HttpLoggingInterceptor interceptor;
    private static Retrofit retrofit;
    private static OkHttpClient.Builder okhttpclient;






    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(EndPoint.BASE_URL).addConverterFactory(GsonConverterFactory.create());





    public static <T> T creaservice(Class<T> serviceclass){

        if(interceptor==null){

            interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }
        if(okhttpclient==null){

            okhttpclient = new OkHttpClient.Builder();

            okhttpclient.addInterceptor(interceptor);
            okhttpclient.retryOnConnectionFailure(true);
            okhttpclient.connectTimeout(3, TimeUnit.MINUTES);
            okhttpclient.readTimeout(3,TimeUnit.MINUTES);
        }

        retrofit = builder.client(okhttpclient.retryOnConnectionFailure(true).build()).build();

        return retrofit.create(serviceclass);

    }






}
