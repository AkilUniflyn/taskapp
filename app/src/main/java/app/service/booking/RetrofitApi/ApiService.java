package app.service.booking.RetrofitApi;



import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

import java.util.Map;

import app.service.booking.model.ContactResponse;
import app.service.booking.model.api.BrowsTask;
import app.service.booking.model.api.ChatList;
import app.service.booking.model.api.CommonResponse;
import app.service.booking.model.api.Dashboard;
import app.service.booking.model.api.ImageUploadResponse;
import app.service.booking.model.api.RegionlistResponse;
import app.service.booking.model.api.SignupResponse;
import app.service.booking.model.api.SocialSignupLoginResponse;
import app.service.booking.model.api.ViewBankDetailsResponse;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

/**
 * Created by uniflyn on 24/7/18.
 */

public interface ApiService {

    //http://mobwebapps.com/Taskapp/index.php/api/SocailLoginRegister
    @FormUrlEncoded
    @POST("SocailLoginRegister")
    Call<SocialSignupLoginResponse> SOCIAL_SIGNUP_LOGIN_RESPONSE_CALL (@Field("first_name")String firstname,
                                                                       @Field("last_name")String lastname,
                                                                       @Field("mobile_number")String mobilenumber,
                                                                       @Field("email")String email,
                                                                       @Field("type")String type,
                                                                       @Field("token")String token,
                                                                       @Field("language")String language,
                                                                       @Field("region")String regin);

    //http://mobwebapps.com/Taskapp/index.php/api/UserLogin
    @FormUrlEncoded
    @POST("UserLogin")
    Call<SocialSignupLoginResponse> LOGIN_RESPONSE_CALL (  @Field("email")String email,
                                                           @Field("type")String type,
                                                           @Field("token")String token,
                                                           @Field("language")String language,
                                                           @Field("password")String password);

    //http://mobwebapps.com/Taskapp/index.php/api/ForgotPassword
    @FormUrlEncoded
    @POST("ForgotPassword")
    Call<CommonResponse> calForgot(@Field("email")String email,
                                   @Field("language")String language);

    //1. Region List
    //http://mobwebapps.com/Taskapp/index.php/api/RegoinList
    @FormUrlEncoded
    @POST("RegoinList")
    Call<RegionlistResponse> callRegionlistAPI(@Field("language")String language);

    //1. SignUp
    //http://mobwebapps.com/Taskapp/index.php/api/UserRegister
    @FormUrlEncoded
    @POST("UserRegister")
    Call<SignupResponse> callSignUpAPI(@Field("region")String region,
                                       @Field("email")String email,
                                       @Field("password")String password,
                                       @Field("work")String work,
                                       @Field("earn")String earn,
                                       @Field("type")String type,
                                       @Field("token")String token,
                                       @Field("language")String language);

    //http://mobwebapps.com/Taskapp/index.php/api/Upload
    @Multipart
    @POST("Upload")
    Call<ImageUploadResponse> callImageUpload(@PartMap Map<String , RequestBody> param);

    //http://mobwebapps.com/Taskapp/index.php/api/ProfileUpdate
    @FormUrlEncoded
    @POST("ProfileUpdate")
    Call<SocialSignupLoginResponse> callProfileUpdate(@Field("user_id")String userId,
                                           @Field("first_name")String firstname,
                                           @Field("last_name")String lastname,
                                           @Field("email")String email,
                                           @Field("mobile_number")String mobilenumber,
                                           @Field("profie")String profile,
                                           @Field("location")String location,
                                           @Field("lat")String lat,
                                           @Field("lon")String lon,
                                           @Field("earn")String earn,
                                           @Field("work")String work,
                                           @Field("type")String type,
                                           @Field("token")String token,
                                           @Field("language")String languageid);

    //http://mobwebapps.com/Taskapp/index.php/api/Logout
    @FormUrlEncoded
    @POST("Logout")
    Call<CommonResponse> callLogout(@Field("language")String language,
                                    @Field("user_id")String userid);


    //http://mobwebapps.com/Taskapp/index.php/api/NotificationUpdate
    @FormUrlEncoded
    @POST("NotificationUpdate")
    Call<CommonResponse> callNotificstion(@Field("language")String language,
                                          @Field("user_id")String userid,
                                          @Field("notification")String notification);


    //http://mobwebapps.com/Taskapp/index.php/api/ChangePassword
    @FormUrlEncoded
    @POST("ChangePassword")
    Call<CommonResponse> callChangePassword(@Field("language")String language,
                                            @Field("user_id")String userid,
                                            @Field("current_password")String currentpaswd,
                                            @Field("new_password")String newpswd);

    //http://mobwebapps.com/Taskapp/index.php/api/Help
    @FormUrlEncoded
    @POST("Help")
    Call<CommonResponse> callHelp(@Field("language")String language,
                                  @Field("user_id")String userid,
                                  @Field("message")String message);

    //http://mobwebapps.com/Taskapp/index.php/api/ContactDetail

    @GET("ContactDetail")
    Call<ContactResponse> callContactDetails();


    //http://mobwebapps.com/Taskapp/index.php/api/ChatList
    @FormUrlEncoded
    @POST("ChatList")
    Call<ChatList> callChat(@Field("user_id")String userid,
                            @Field("language")String languageid); // whant do


    //http://mobwebapps.com/Taskapp/index.php/api/ServicesList
    @FormUrlEncoded
    @POST("ServicesList")
    Call<Dashboard> callDashboardList(@Field("language")String language);


    //http://mobwebapps.com/Taskapp/index.php/api/AddEditBankDetail
    @FormUrlEncoded
    @POST("AddEditBankDetail")
    Call<CommonResponse> callbankdetails(@Field("user_id")String user_id,
                                         @Field("name")String name,
                                         @Field("address")String address,
                                         @Field("account_number")String account_number,
                                         @Field("routing_number")String routing_number,
                                         @Field("language")String language);

    ///http://mobwebapps.com/Taskapp/index.php/api/AccountDetail
    @FormUrlEncoded
    @POST("AccountDetail")
    Call<ViewBankDetailsResponse> callviewbankdetails(@Field("user_id")String user_id,
                                                      @Field("language")String language);

    //http://mobwebapps.com/Taskapp/index.php/api/AddCard
    @FormUrlEncoded
    @POST("AddCard")
    Call<CommonResponse> calladdcard(@Field("user_id")String user_id,
                                     @Field("language")String language,
                                     @Field("card_number")String card_number,
                                     @Field("name")String name,
                                     @Field("date")String date,
                                     @Field("year")String year,
                                     @Field("cvv")String cvv);

    //http://mobwebapps.com/Taskapp/index.php/api/BrowseTask
    @FormUrlEncoded
    @POST("BrowseTask")
    Call<BrowsTask> callBrowstask(@Field("language")String languagre,
                                  @Field("location")String location,
                                  @Field("lat")String lat,
                                  @Field("lon")String log,
                                  @Field("distance")String distance,
                                  @Field("min_price")String minPrice,
                                  @Field("max_price")String maxprice,
                                  @Field("sort")String sorttype,
                                  @Field("opened_task")String opentask,
                                  @Field("done_by")String doneby,
                                  @Field("user_id")String userid,
                                  @Field("search")String search,
                                  @Field("page")String page);

    //http://mobwebapps.com/Taskapp/index.php/api/PaymentHitory


}
