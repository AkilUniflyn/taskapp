package app.service.booking;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;
import com.google.gson.Gson;
import com.testfairy.TestFairy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.service.booking.RetrofitApi.ApiService;
import app.service.booking.RetrofitApi.RetrofitSingleton;
import app.service.booking.activity.AddYourBankDetailsActivity;
import app.service.booking.activity.BrowsFilterActivity;
import app.service.booking.activity.LoginActivity;
import app.service.booking.activity.UserProfileActivity;
import app.service.booking.adapter.LeftNavAdapter;
import app.service.booking.fragment.BrowseTaskFragment;
import app.service.booking.fragment.DashboardFragment;
import app.service.booking.fragment.MessageFragment;
import app.service.booking.fragment.MyTaskFragment;
import app.service.booking.fragment.NotificationFragment;
import app.service.booking.fragment.PaymentHistoryFragment;
import app.service.booking.fragment.PostataskFragment;
import app.service.booking.fragment.SettingFragment;
import app.service.booking.model.ClassLeftDrawer;
import app.service.booking.model.api.CommonResponse;
import app.service.booking.utils.PrefConnect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.service.booking.activity.BrowsFilterActivity.log;
import static app.service.booking.activity.BrowsFilterActivity.doneby;
import static app.service.booking.activity.BrowsFilterActivity.location;
import static app.service.booking.activity.BrowsFilterActivity.log;
import static app.service.booking.activity.BrowsFilterActivity.maxprice;
import static app.service.booking.activity.BrowsFilterActivity.minprice;
import static app.service.booking.activity.BrowsFilterActivity.opentask;
import static app.service.booking.activity.BrowsFilterActivity.page;
import static app.service.booking.activity.BrowsFilterActivity.sortype;
import static app.service.booking.activity.BrowsFilterActivity.distance;
import static app.service.booking.activity.BrowsFilterActivity.lat;
import static app.service.booking.activity.WalkthroughActivity.MYPREFLANG;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

  //  @BindView(R.id.btn_menu_open)
    public Button btnMenuOpen;
   // @BindView(R.id.txt_title)
   public TextView txtTitle;
    @BindView(R.id.btn_menu)
    Button btnMenu;
    @BindView(R.id.frame_container)
    FrameLayout frameContainer;
    @BindView(R.id.img_header)
    CircleImageView imgHeader;
    @BindView(R.id.txt_header_name)
    TextView txtHeaderName;
    @BindView(R.id.list_nav)
    ListView listNav;
   // @BindView(R.id.drawer_layout_new)
    public DrawerLayout drawerLayoutNew;
    @BindView(R.id.linearUserDetails)
    LinearLayout linearUserDetails;
    //@BindView(R.id.img_menu)
    public  ImageView imgMenu;


   // @BindView(R.id.btn_menu_two)
   public Button btnMenuTwo;
    @BindView(R.id.img_menu_two)
    ImageView imgMenuTwo;
    @BindView(R.id.reltaiveTwo)
    RelativeLayout relativeTwo;

    public ImageView imagBack;

    private ActionBarDrawerToggle mDrawerToggle;
    LeftNavAdapter leftNavAdapter;
    List<ClassLeftDrawer> rowItems;
    public static final String[] titles = new String[]{"Dashboard", "Browse Task", "Post a Task", "My Task", "Payment History", "Messages", "Notifications", "Settings", "Logout"};
    private float lastTranslate = 0.0f;
    public static final Integer[] images = {R.drawable.dashboard_menu, R.drawable.browse_menu, R.drawable.posttask_menu, R.drawable.mytask_menu, R.drawable.payment_menu, R.drawable.message_menu, R.drawable.notification_menu, R.drawable.setting_menu, R.drawable.logout_menu};

    String languageid,statusmenu,fsname,lastname,userid;
    ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TestFairy.begin(this, "SDK-CBWoABNL");
        ButterKnife.bind(this);
        apiService = RetrofitSingleton.creaservice(ApiService.class);

        imagBack = (ImageView)findViewById(R.id.img_back);
        btnMenuOpen = (Button)findViewById(R.id.btn_menu_open);
        txtTitle = (TextView)findViewById(R.id.txt_title);
        imgMenu = (ImageView)findViewById(R.id.img_menu);
        btnMenuTwo = (Button)findViewById(R.id.btn_menu_two);
        fsname = PrefConnect.readString(MainActivity.this,PrefConnect.FIRSTNAME,"");
        lastname = PrefConnect.readString(MainActivity.this,PrefConnect.LASTNAME,"");
        userid = PrefConnect.readString(MainActivity.this,PrefConnect.USERID,"");


        SharedPreferences prefs = getSharedPreferences(MYPREFLANG, MODE_PRIVATE);
        languageid = prefs.getString("languageid", "1");//"No name defined" is the default value.
        Log.e("mainlanguid",languageid);

        txtHeaderName.setText(fsname+" "+lastname);
        drawerLayoutNew = (DrawerLayout)findViewById(R.id.drawer_layout_new);

        btnMenuOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lat ="";
                log="";
                location="" ;
                distance = "1000";
                minprice = "0";
                maxprice = "1000";
                sortype = "1";
                opentask = "0";
                doneby = "1";
                drawerLayoutNew.openDrawer(GravityCompat.START);
            }
        });


        setupDrawer();
        setupnavigation();
        try {
            if(getIntent()!=null){
                String page = getIntent().getStringExtra("page");

                if(page.equals("3")){
                    txtTitle.setText(titles[3]);
                    imgMenu.setVisibility(View.GONE);
                    relativeTwo.setVisibility(View.GONE);
                    beginTransction(new MyTaskFragment());
                }
            }


        }catch (Exception e){
            e.printStackTrace();

            txtTitle.setText(titles[0]);
            statusmenu = "notification";
            imgMenu.setImageResource(R.drawable.notification_menu);
            relativeTwo.setVisibility(View.GONE);
            beginTransction(new DashboardFragment());
        }




    }

    @OnClick({R.id.btn_menu_two, R.id.btn_menu, R.id.linearUserDetails, R.id.img_menu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_menu_two:
                break;

            case R.id.btn_menu:
                Log.e("menu",statusmenu);
                switch (statusmenu){

                    case "notification":
                        txtTitle.setText(titles[6]);
                        beginTransction(new NotificationFragment());
                        imgMenu.setVisibility(View.GONE);
                        relativeTwo.setVisibility(View.GONE);

                        break;
                    case "filter":

                        Intent intent = new Intent(MainActivity.this, BrowsFilterActivity.class);
                        startActivity(intent);
                        break;
                    case "bankhome":
                        Intent intent1 = new Intent(MainActivity.this, AddYourBankDetailsActivity.class);
                        startActivity(intent1);
                        break;

                }
                break;
            case R.id.linearUserDetails:
                Intent intent = new Intent(MainActivity.this, UserProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.img_menu:


                break;
        }
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayoutNew, R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);


                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                invalidateOptionsMenu();
                 fsname = PrefConnect.readString(MainActivity.this,PrefConnect.FIRSTNAME,"");
                 lastname = PrefConnect.readString(MainActivity.this,PrefConnect.LASTNAME,"");

                 txtHeaderName.setText(fsname+" "+lastname);
                lat ="";
                log="";
                location="" ;
                distance = "1000";
                minprice = "0";
                maxprice = "1000";
                sortype = "1";
                opentask = "0";
                doneby = "1";
                // creates call to onPrepareOptionsMenu()
                // relativeBack.setVisibility(View.GONE);

            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                //relativeBack.setVisibility(View.GONE);

            }

        };


        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayoutNew.setDrawerListener(mDrawerToggle);
        // drawerLayoutNew.addDrawerListener(mDrawerToggle);
    }

    private void setupnavigation() {

        rowItems = new ArrayList<ClassLeftDrawer>();
        for (int i = 0; i < titles.length; i++) {
            ClassLeftDrawer item = new ClassLeftDrawer(images[i], titles[i]);
            rowItems.add(item);
        }

        leftNavAdapter = new LeftNavAdapter(this,
                R.layout.activity_main_left_drawer_menu, rowItems);
        listNav.setAdapter(leftNavAdapter);
        listNav.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Log.e("position", position + "");
        if (position == 0) {
            txtTitle.setText(titles[0]);
            beginTransction(new DashboardFragment());
            imgMenu.setImageResource(R.drawable.notification_menu);
            relativeTwo.setVisibility(View.GONE);
            imgMenu.setVisibility(View.VISIBLE);
            statusmenu = "notification";
            //relavitiveMenu.setVisibility(View.VISIBLE);
            //imgHomeMenu.setImageResource(R.drawable.search_white_icon);
        } else if (position == 1) {
            txtTitle.setText(titles[1]);
            beginTransction(new BrowseTaskFragment());
            imgMenu.setImageResource(R.drawable.browsr_filter_icon);
            imgMenu.setVisibility(View.VISIBLE);
            relativeTwo.setVisibility(View.GONE);
            statusmenu = "filter";
        } else if (position == 2) {
            txtTitle.setText(titles[2]);
            imgMenu.setVisibility(View.GONE);
            relativeTwo.setVisibility(View.GONE);
            beginTransction(new PostataskFragment());

            //beginTransction(new ActivityLogFragment());
        } else if (position == 3) {
            txtTitle.setText(titles[3]);
            imgMenu.setVisibility(View.GONE);
            relativeTwo.setVisibility(View.GONE);
            beginTransction(new MyTaskFragment());
            //beginTransction(new ProfileFragment());
        } else if (position == 4) {
            txtTitle.setText(titles[4]);
            beginTransction(new PaymentHistoryFragment());
            imgMenu.setVisibility(View.VISIBLE);
            imgMenu.setImageResource(R.drawable.payhist_icon_home);
            relativeTwo.setVisibility(View.GONE);

            statusmenu = "bankhome";
            //beginTransction(new CommunicationFragment());
        } else if (position == 5) {
            txtTitle.setText(titles[5]);
            beginTransction(new MessageFragment());
            imgMenu.setVisibility(View.GONE);
            relativeTwo.setVisibility(View.GONE);
        } else if (position == 6) {
            txtTitle.setText(titles[6]);
            beginTransction(new NotificationFragment());
            imgMenu.setVisibility(View.GONE);
            relativeTwo.setVisibility(View.GONE);

        }else if(position == 7){
            txtTitle.setText(titles[7]);
            beginTransction(new SettingFragment());
            imgMenu.setVisibility(View.GONE);
            relativeTwo.setVisibility(View.GONE);

        }else if(position == 8){
            Dialog dialoglogout = new AlertDialog.Builder(MainActivity.this)

                    .setMessage(getString(R.string.logout_toast))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            callLogout();

                           /* Intent intentlogin = new Intent(MainActivity.this, LoginActivity.class);
                            startActivity(intentlogin);
                            finish();*/

                        }
                    })
                    .setNegativeButton(getString(R.string.no), null)
                    .show();
        }

    }

    private void callLogout() {
        Call<CommonResponse> call = apiService.callLogout(languageid,userid);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Log.e("sucesslogout",new Gson().toJson(response.body()));
                if(response.isSuccessful()){
                    CommonResponse resp = response.body();
                    if (resp != null) {

                        String status = resp.getStatus();
                        Intent intentlogin = new Intent(MainActivity.this, LoginActivity.class);
                        intentlogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intentlogin);
                        finish();

                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

            }
        });
    }

    public void beginTransction(final Fragment fragment) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.frame_container, fragment);
                    //transaction.addToBackStack(null);
                    transaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, 250);
        drawerLayoutNew.closeDrawers();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        if (drawerLayoutNew.isDrawerOpen(GravityCompat.START)) {
            drawerLayoutNew.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            finish();
        }

    }

    //************** Multiple permissions ****************//

    /**
     * Call multiple Permissions
     */



}
