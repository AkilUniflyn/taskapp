package app.service.booking.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.activity.OtherUserProfileActivity;
import app.service.booking.activity.SendOfferActivity;
import app.service.booking.activity.ViewOfferActivity;
import app.service.booking.model.MyTaskModel;
import app.service.booking.model.api.BrowsTask;
import de.hdodenhof.circleimageview.CircleImageView;

public class AllAdapter extends RecyclerView.Adapter<AllAdapter.DataObjectHolder> {


    Context context;
    List<BrowsTask.Datum> mData;
    String type;
    int count;


    public AllAdapter(List<BrowsTask.Datum> mData, Context context,String type) {
        this.mData = mData;
        this.context = context;
        this.type = type;
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder{
        TextView txtOffers,txtAmount,txtDate,txtServiceAddress,txtServiceName;
        CircleImageView img;
        TextView btnopen;

        public DataObjectHolder(View itemView){
            super(itemView);


            btnopen = (TextView)itemView.findViewById(R.id.btn_open);
            txtOffers = (TextView)itemView.findViewById(R.id.txt_offeres);
            txtAmount = (TextView)itemView.findViewById(R.id.txt_amount);
            txtDate = (TextView)itemView.findViewById(R.id.txt_date);
            txtServiceAddress = (TextView)itemView.findViewById(R.id.txt_service_address);
            txtServiceName = (TextView)itemView.findViewById(R.id.txt_service_name);
            img = (CircleImageView)itemView.findViewById(R.id.img_service);


        }
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_all_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {

        holder.txtServiceName.setText(mData.get(position).getTaskDetail().getTitle());
        holder.txtServiceAddress.setText(mData.get(position).getTaskDetail().getLocation());
        holder.txtDate.setText(mData.get(position).getTaskDetail().getDateAdded());
        holder.txtAmount.setText(mData.get(position).getTaskDetail().getAmount());
        holder.txtOffers.setText(mData.get(position).getTaskDetail().getOffers());
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(context, OtherUserProfileActivity.class);
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type.equals("1")){
                    Intent intent = new Intent(context, SendOfferActivity.class);
                    context.startActivity(intent);
                }else {
                    Intent intent = new Intent(context, ViewOfferActivity.class);
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


}

