package app.service.booking.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import app.service.booking.R;
import app.service.booking.activity.PostingNewTaskActivity;
import app.service.booking.model.DashboardModel;
import app.service.booking.model.api.Dashboard;


public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DataObjectHolder> {

    Context context;
    List<Dashboard.Datum> mData;
    CallBack callBack;

    public interface CallBack{
        public void call();
    }

    public DashboardAdapter(Context context, List<Dashboard.Datum> mData,CallBack callBack) {
        this.context = context;
        this.mData = mData;
        this.callBack = callBack;
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView textname;

        public DataObjectHolder(View itemView){
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            textname = (TextView)itemView.findViewById(R.id.txt_name);
        }
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {

        holder.textname.setText(mData.get(position).getName());
        Glide.with(context).load(mData.get(position).getImage()).into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,PostingNewTaskActivity.class);
                context.startActivity(intent);
                //callBack.call();

               /* Intent intent = new Intent(context, MessageActivity.class);
                intent.putExtra("name",name);
                intent.putExtra("other_userid", messageList.get(position).getOtherUserId());
                intent.putExtra("other_user_image", messageList.get(position).getImage());
                context.startActivity(intent);*/
            }
        });

    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

}

