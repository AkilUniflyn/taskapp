package app.service.booking.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;



import java.util.List;

import app.service.booking.R;
import app.service.booking.activity.OtherUserProfileActivity;
import app.service.booking.model.ReviewsModel;
import de.hdodenhof.circleimageview.CircleImageView;

public class MoreRewiesAdapter extends RecyclerView.Adapter<MoreRewiesAdapter.DataObjectHolder> {


    Context context;
    List<ReviewsModel> mData;
    String type;
    int count;


    public MoreRewiesAdapter(List<ReviewsModel> mData, Context context) {
        this.mData = mData;
        this.context = context;
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder{
        TextView txtdatetime,txtMsg,txtName;
        CircleImageView img;
        Button btnopen;
        RatingBar ratingBar;

        public DataObjectHolder(View itemView){
            super(itemView);


            txtName = (TextView)itemView.findViewById(R.id.txtName);
            txtdatetime = (TextView)itemView.findViewById(R.id.txtDateTime);
            img = (CircleImageView)itemView.findViewById(R.id.prImg);
            txtMsg = (TextView)itemView.findViewById(R.id.txtMsg);
            ratingBar = (RatingBar)itemView.findViewById(R.id.ratingbar);



        }
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_morereview_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {

        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OtherUserProfileActivity.class);
                context.startActivity(intent);
            }
        });

        holder.txtName.setText(mData.get(position).getName());
        holder.txtdatetime.setText(mData.get(position).getDatetime());
        holder.txtMsg.setText(mData.get(position).getMessage());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


}

