package app.service.booking.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import app.service.booking.activity.MessageActivity;
import app.service.booking.R;
import app.service.booking.activity.OtherUserProfileActivity;
import app.service.booking.model.Message;
import app.service.booking.model.api.ChatList;


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.DataObjectHolder> {


    Context context;
    List<ChatList.Datum> messageList;

    String value;
    int count;
    Dialog dialogPay,dialogPayConform,dialogreminder;
    CallBack callBack;

    public interface CallBack{
        public void call(String name);
    }


    public MessageAdapter(Context context, List<ChatList.Datum> messageList) {

        this.context = context;
        this.messageList = messageList;
        this.callBack = callBack;
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder{


        ImageView imguser;
        TextView textname,texttime,count;
        LinearLayout layout_messageadapter;

        public DataObjectHolder(View itemView){
            super(itemView);

            imguser = (ImageView) itemView.findViewById(R.id.img_user);
            textname = (TextView)itemView.findViewById(R.id.txt_name);
            texttime= (TextView)itemView.findViewById(R.id.txt_time);
            count = (TextView)itemView.findViewById(R.id.count);
            layout_messageadapter= (LinearLayout) itemView.findViewById(R.id.layout_messageadapter);

        }
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_message_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {

       /* if (position % 2 == 0) {

            holder.layout_messageadapter.setBackgroundResource(R.color.colorWhite);

        } else
        {
            holder.layout_messageadapter.setBackgroundResource(R.color.colorPrimaryDark);
        }*/

       holder.imguser.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(context, OtherUserProfileActivity.class);
               context.startActivity(intent);
           }
       });
        holder.textname.setText(messageList.get(position).getLastName() +" "+messageList.get(position).getFirstName());
      /*  holder.texttime.setText(messageList.get(position).getVechname());
        holder.count.setText(messageList.get(position).getCount());
        holder.count.setBackgroundDrawable(context.getResources().getDrawable(messageList.get(position).getImage()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = messageList.get(position).getName();

                Intent intent = new Intent(context, MessageActivity.class);
                intent.putExtra("name",name);
                context.startActivity(intent);
            }
        });*/



    }


    @Override
    public int getItemCount() {
        return messageList.size();
    }


}

