package app.service.booking.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import app.service.booking.R;
import app.service.booking.model.api.RegionlistResponse;


public class RegionAdapter extends RecyclerView.Adapter<RegionAdapter.DataObjectHolder> {


    Context context;
    List<RegionlistResponse.Datum> mData;
    ItemClickListener itemClickListener;

    public interface ItemClickListener{

        public void itemClick(String id);
    }

    public RegionAdapter(Context context, List<RegionlistResponse.Datum> mData, ItemClickListener itemClickListener) {
        this.mData = mData;
        this.context = context;
        this.itemClickListener=itemClickListener;
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder{
        TextView txt_name;
        ImageView img_flag;

        public DataObjectHolder(View itemView){
            super(itemView);

            txt_name = (TextView)itemView.findViewById(R.id.txt_name);
            img_flag = (ImageView)itemView.findViewById(R.id.img_flag);
        }
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_region, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {


        holder.txt_name.setText(mData.get(position).getName());
        Glide.with(context).load(mData.get(position).getImage()).into(holder.img_flag);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.itemClick(mData.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


}

