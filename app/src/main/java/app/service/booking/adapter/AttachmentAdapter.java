package app.service.booking.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.model.ImageModel;

public class AttachmentAdapter extends RecyclerView.Adapter<AttachmentAdapter.MyViewHolder> {
    List<ImageModel> arrayList = new ArrayList<>();
    Context context;
    ImageModel obj;
    String fontPathbold,fontPathsemibold,fontPathregular;
    TextView text;
    ImageView image;
    MenuItemClickListener clickListener;
    public interface MenuItemClickListener{
        public void ItemClick(int position);
    }
    public AttachmentAdapter(Context context, List<ImageModel> list) {

        arrayList = list;
        this.context = context;
        this.clickListener=clickListener;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if (arrayList.get(position) != null) {
            obj = arrayList.get(position);

            try {
                holder.image.setImageDrawable(context.getResources().getDrawable(obj.getImage()));

            /*    holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        clickListener.ItemClick(position);
                    }
                });
*/
                //  Picasso.with(context).load(obj.getImage()).into(holder.image);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView text;
        ImageView image;
        public MyViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.image);

        }
    }
}
