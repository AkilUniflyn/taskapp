package app.service.booking.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.service.booking.R;
import app.service.booking.activity.PostingNewTaskActivity;
import app.service.booking.model.DashboardModel;
import app.service.booking.model.ImageAddModel;


public class NewImageAddAdapter extends RecyclerView.Adapter<NewImageAddAdapter.DataObjectHolder> {

    Context context;
    List<ImageAddModel> mData;
    CallBack callBack;

    public interface CallBack{
        public void call();
    }

    public NewImageAddAdapter(Context context, List<ImageAddModel> mData) {
        this.context = context;
        this.mData = mData;
        this.callBack = callBack;
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder{

        ImageView image,imageDelete;


        public DataObjectHolder(View itemView){
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.img_one);
            imageDelete = (ImageView)itemView.findViewById(R.id.img_delete);

        }
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_newimage_add, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {

        holder.image.setImageBitmap(mData.get(position).getImageBitmap());
        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mData.remove(position);
                notifyDataSetChanged();
            }
        });

    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

}

