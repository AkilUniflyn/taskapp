package app.service.booking.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.model.ImageModel;
import app.service.booking.model.OfferModel;
import app.service.booking.model.QuestionModel;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.MyViewHolder> {
    List<OfferModel> arrayList = new ArrayList<>();
    Context context;
    OfferModel obj;
    TextView text;
    ImageView image;
    Dialog reply_dialog;

    public interface MenuItemClickListener{
        public void ItemClick(int position);
    }
    public OfferAdapter(Context context, List<OfferModel> list) {

        arrayList = list;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offer, parent, false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if (arrayList.get(position) != null) {
            obj = arrayList.get(position);

            try {
                holder.image.setImageDrawable(context.getResources().getDrawable(obj.getImage()));
                holder.name.setText(obj.getName());
                holder.desc.setText(obj.getDesc());
                holder.time.setText(obj.getTime());


                holder.txt_reply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        reply_dialog = new Dialog(context);
                        reply_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        reply_dialog.setContentView(R.layout.dialog_reply);
                        reply_dialog.setCancelable(false);
                        reply_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        Button reply=(Button)reply_dialog.findViewById(R.id.btn_reply);
                        reply_dialog.show();


                        reply.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                reply_dialog.dismiss();
                            }
                        });
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name,time,desc,txt_reply;

        ImageView image;
        public MyViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.img_profile);
            name=(TextView)itemView.findViewById(R.id.txt_name);
            time=(TextView)itemView.findViewById(R.id.txt_time);
            desc=(TextView)itemView.findViewById(R.id.txt_desc);
            txt_reply=(TextView)itemView.findViewById(R.id.txt_reply);


        }
    }
}
