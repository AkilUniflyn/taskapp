package app.service.booking.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import app.service.booking.R;

public class WalkthroughAdapter extends PagerAdapter {
    private ArrayList<String> text_array=new ArrayList<>();
    private ArrayList<String> text_des=new ArrayList<>();
    private ArrayList<Integer> images=new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;

    //int image_bg[]={R.drawable.walknew1,R.drawable.walknew2,R.drawable.walknew3,R.drawable.walknew4,R.drawable.walknew5};

    public WalkthroughAdapter(Context context, ArrayList<String> text_array, ArrayList<Integer> images,ArrayList<String> text_des) {
        this.context = context;
        this.text_array=text_array;
        this.images=images;
        this.text_des=text_des;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        Log.e("TAG",text_array.size()+"");
        return text_array.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.item_walkthrough, view, false);

        TextView txt_title = (TextView) myImageLayout.findViewById(R.id.txt_title);
        txt_title.setText(text_array.get(position));

        TextView txt_description = (TextView) myImageLayout.findViewById(R.id.txt_description);
        txt_description.setText(text_des.get(position));

        ImageView image = (ImageView) myImageLayout.findViewById(R.id.image);
        image.setImageResource(images.get(position));

        view.addView(myImageLayout, position);

        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}