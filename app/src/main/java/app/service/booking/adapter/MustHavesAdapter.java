package app.service.booking.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.service.booking.R;
import app.service.booking.model.MustHavesModel;
import app.service.booking.model.RegionModel;

public class MustHavesAdapter extends RecyclerView.Adapter<MustHavesAdapter.DataObjectHolder> {


    Context context;
    List<MustHavesModel> mData;
    ItemClickListener itemClickListener;

    public interface ItemClickListener{
        public void itemClick();
    }

    public MustHavesAdapter(Context context, List<MustHavesModel> mData,ItemClickListener itemClickListener) {
        this.mData = mData;
        this.context = context;
        this.itemClickListener=itemClickListener;
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder{
        TextView txt_text;
        ImageView img_delete;

        public DataObjectHolder(View itemView){
            super(itemView);

            txt_text = (TextView)itemView.findViewById(R.id.txt_text);
            img_delete = (ImageView)itemView.findViewById(R.id.img_delete);
        }
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_must_haves, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {


        holder.txt_text.setText(mData.get(position).getText());

        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mData.remove(position);
                notifyDataSetChanged();
                itemClickListener.itemClick();

            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


}

