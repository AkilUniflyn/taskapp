package app.service.booking.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import app.service.booking.R;
import app.service.booking.model.CardModel;
import de.hdodenhof.circleimageview.CircleImageView;

public class AmountSpenErandAdapter extends RecyclerView.Adapter<AmountSpenErandAdapter.DataObjectHolder> {


    Context context;
    List<CardModel> mData;
    String type;
    int count;


    public AmountSpenErandAdapter(List<CardModel> mData, Context context) {
        this.mData = mData;
        this.context = context;
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder{
        TextView txtMsg;
        CircleImageView userImag;
        Button btnopen;

        public DataObjectHolder(View itemView){
            super(itemView);


            txtMsg = (TextView)itemView.findViewById(R.id.txt_msg);
            userImag = (CircleImageView)itemView.findViewById(R.id.userImg);


        }
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_amount_spenerand_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {


        holder.txtMsg.setText(mData.get(position).getCardno());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


}

