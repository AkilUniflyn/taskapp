package app.service.booking.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.activity.ViewOfferActivity;
import app.service.booking.model.ImageModel;
import app.service.booking.model.NotificationModel;
import app.service.booking.model.OfferModel;
import app.service.booking.model.QuestionModel;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    List<NotificationModel> arrayList = new ArrayList<>();
    Context context;
    NotificationModel obj;
    TextView text;
    ImageView image;

    public interface MenuItemClickListener{
        public void ItemClick(int position);
    }
    public NotificationAdapter(Context context, List<NotificationModel> list) {

        arrayList = list;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if (arrayList.get(position) != null) {
            obj = arrayList.get(position);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ViewOfferActivity.class);
                    context.startActivity(intent);
                }
            });
            try {
                holder.image.setImageDrawable(context.getResources().getDrawable(obj.getImage()));
                holder.day.setText(obj.getDay());
                holder.date.setText(obj.getDate());
                holder.request.setText(obj.getRequest());
                holder.description.setText(obj.getDescription());



            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView day,date,request,description;

        ImageView image;
        public MyViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.img_notification);
            day=(TextView)itemView.findViewById(R.id.txt_day);
            date=(TextView)itemView.findViewById(R.id.txt_date);
            request=(TextView)itemView.findViewById(R.id.txt_offer_request);
            description=(TextView)itemView.findViewById(R.id.txt_offer_details);


        }
    }
}
