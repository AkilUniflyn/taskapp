package app.service.booking.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;


import java.util.List;

import app.service.booking.R;
import app.service.booking.activity.MessageActivity;
import app.service.booking.activity.OtherUserProfileActivity;
import app.service.booking.model.OfferListModel;
import de.hdodenhof.circleimageview.CircleImageView;

public class OfferListAdapter extends RecyclerView.Adapter<OfferListAdapter.DataObjectHolder> {

    Context context;
    List<OfferListModel> mData;

    public OfferListAdapter(List<OfferListModel> mData, Context context) {
        this.mData = mData;
        this.context = context;
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder{
        TextView txtName,txtAmount,txtDate,txtMsg,txt_accept,txt_message;
        CircleImageView img_profile;

        public DataObjectHolder(View itemView){
            super(itemView);
            txtName = (TextView)itemView.findViewById(R.id.txt_name);
            txtAmount = (TextView)itemView.findViewById(R.id.txt_amount);
            txtDate = (TextView)itemView.findViewById(R.id.txt_date);
            txtMsg = (TextView)itemView.findViewById(R.id.txt_msg);
            txt_accept = (TextView)itemView.findViewById(R.id.txt_accept);
            txt_message = (TextView)itemView.findViewById(R.id.txt_message);
            img_profile = (CircleImageView) itemView.findViewById(R.id.img_profile);
        }
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_offerlist_item, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {

        holder.txtMsg.setText(mData.get(position).getMsg());
        holder.txtDate.setText(mData.get(position).getDate());
        holder.txtAmount.setText(mData.get(position).getAmount());
        holder.txtName.setText(mData.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent next=new Intent(context, OtherUserProfileActivity.class);
                context.startActivity(next);
            }
        });

        holder.txt_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAcceptDialog();
            }
        });

        holder.txt_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent next=new Intent(context, MessageActivity.class);
                context.startActivity(next);
            }
        });

    }

    private void setAcceptDialog() {

        final Dialog dialogAccepte = new Dialog(context);
        dialogAccepte.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAccepte.setContentView(R.layout.dialog_send_message);
        dialogAccepte.show();
        Button btnsendmsg = (Button)dialogAccepte.findViewById(R.id.btn_sendon_msg);
        btnsendmsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAccepte.dismiss();
                Intent intent = new Intent(context, MessageActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


}

