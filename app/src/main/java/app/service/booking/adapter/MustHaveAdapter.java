package app.service.booking.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.model.NotificationModel;

public class MustHaveAdapter extends RecyclerView.Adapter<MustHaveAdapter.MyViewHolder> {

    Context context;
    ArrayList<String> stringArrayList;



    public MustHaveAdapter(Context context,  ArrayList<String> stringArrayList) {

       this. stringArrayList = stringArrayList;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.must_have_item, parent, false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.txtcomment.setText(stringArrayList.get(position));


    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtcomment;

        ImageView image;
        public MyViewHolder(View itemView) {
            super(itemView);


            txtcomment=(TextView)itemView.findViewById(R.id.txt_comment);


        }
    }
}
