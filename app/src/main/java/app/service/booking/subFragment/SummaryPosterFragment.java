package app.service.booking.subFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.service.booking.R;

public class SummaryPosterFragment extends Fragment {

    View root_view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root_view=inflater.inflate(R.layout.fragment_summary_poster,container,false);
        return  root_view;
    }
}
