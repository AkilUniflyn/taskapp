package app.service.booking.subFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.service.booking.R;
import app.service.booking.adapter.AllAdapter;
import app.service.booking.model.MyTaskModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link AssignedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AssignedFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recycleAssigned)
    RecyclerView recycleAssigned;
    Unbinder unbinder;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    AllAdapter allAdapter;
    List<MyTaskModel> myTaskModels;


    public AssignedFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AssignedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AssignedFragment newInstance(String param1, String param2) {
        AssignedFragment fragment = new AssignedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_assigned, container, false);
        unbinder = ButterKnife.bind(this, view);
        myTaskModels = new ArrayList<>();
        myTaskModels.add(new MyTaskModel("Seat Belts Fitted","Beijing China","Dec 25,2018","40","12 offers"));
        myTaskModels.add(new MyTaskModel("Seat Belts Fitted","Beijing China","Dec 25,2018","40","12 offers"));
        myTaskModels.add(new MyTaskModel("Seat Belts Fitted","Beijing China","Dec 25,2018","40","12 offers"));

      /*  allAdapter = new AllAdapter(myTaskModels,getContext(),"2");
        recycleAssigned.setLayoutManager(new LinearLayoutManager(getContext()));
        recycleAssigned.setAdapter(allAdapter);*/
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
