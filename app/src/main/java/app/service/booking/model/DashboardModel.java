package app.service.booking.model;

import android.graphics.drawable.Drawable;

public class DashboardModel {

    String name;
    int image;

    public DashboardModel(String name, int image) {
        this.name = name;
        this.image = image;
    }


    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }
}
