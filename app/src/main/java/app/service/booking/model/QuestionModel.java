package app.service.booking.model;

public class QuestionModel {
    int image;
    String name;
    String time;
    String desc;
    String reply;
    public QuestionModel(int image,String name,String time,String desc,String reply) {
        this.image = image;
        this.name=name;
        this.time=time;
        this.desc=desc;
        this.reply=reply;;
    }

    public int getImage() {
        return image;
    }
    public String getName() {
        return name;
    }

    public String getTime() {
        return time;
    }

    public String getDesc() {
        return desc;
    }

    public String getReply() {
        return reply;
    }
}
