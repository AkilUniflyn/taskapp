package app.service.booking.model;

public class OfferListModel {
    String Name;
    String date;
    String msg;
    String amount;

    public OfferListModel(String name, String date, String msg, String amount) {
        Name = name;
        this.date = date;
        this.msg = msg;
        this.amount = amount;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
