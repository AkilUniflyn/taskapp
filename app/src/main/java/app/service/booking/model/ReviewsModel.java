package app.service.booking.model;

public class ReviewsModel {
    String name;
    String datetime;
    String message;

    public ReviewsModel(String name, String datetime, String message) {
        this.name = name;
        this.datetime = datetime;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
