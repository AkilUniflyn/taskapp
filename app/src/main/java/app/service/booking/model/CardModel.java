package app.service.booking.model;

public class CardModel {
    int cardimg;
    String cardno;

    public CardModel(int cardimg, String cardno) {
        this.cardimg = cardimg;
        this.cardno = cardno;
    }

    public int getCardimg() {
        return cardimg;
    }

    public String getCardno() {
        return cardno;
    }
}
