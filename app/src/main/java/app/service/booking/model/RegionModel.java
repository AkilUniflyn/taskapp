package app.service.booking.model;

public class RegionModel {

    int img_falg;
    String flag_name;


    public RegionModel(int img_falg, String flag_name) {
        this.img_falg = img_falg;
        this.flag_name = flag_name;
    }

    public int getImg_falg() {
        return img_falg;
    }

    public String getFlag_name() {
        return flag_name;
    }
}
