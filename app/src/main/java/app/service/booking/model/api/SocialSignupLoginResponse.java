package app.service.booking.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocialSignupLoginResponse {


    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("mobile_number")
        @Expose
        private String mobileNumber;
        @SerializedName("profie")
        @Expose
        private String profie;
        @SerializedName("rating")
        @Expose
        private String rating;
        @SerializedName("region")
        @Expose
        private String region;
        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lon")
        @Expose
        private String lon;
        @SerializedName("earn")
        @Expose
        private String earn;
        @SerializedName("work")
        @Expose
        private String work;
        @SerializedName("notification")
        @Expose
        private String notification;
        @SerializedName("profile_status")
        @Expose
        private String profileStatus;
        @SerializedName("joined")
        @Expose
        private String joined;

        public String getId() {
            return id;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getEmail() {
            return email;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public String getProfie() {
            return profie;
        }

        public String getRating() {
            return rating;
        }

        public String getRegion() {
            return region;
        }

        public String getLocation() {
            return location;
        }

        public String getLat() {
            return lat;
        }

        public String getLon() {
            return lon;
        }

        public String getEarn() {
            return earn;
        }

        public String getWork() {
            return work;
        }

        public String getNotification() {
            return notification;
        }

        public String getProfileStatus() {
            return profileStatus;
        }

        public String getJoined() {
            return joined;
        }
    }
}
