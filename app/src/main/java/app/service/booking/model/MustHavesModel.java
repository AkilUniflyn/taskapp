package app.service.booking.model;

public class MustHavesModel {
    String text;

    public MustHavesModel(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
