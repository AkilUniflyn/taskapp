package app.service.booking.model;

public class OfferModel {

    int image;
    String name;
    String time;
    String desc;
    public OfferModel(int image,String name,String time,String desc) {
        this.image = image;
        this.name=name;
        this.time=time;
        this.desc=desc;
    }

    public int getImage() {
        return image;
    }
    public String getName() {
        return name;
    }

    public String getTime() {
        return time;
    }

    public String getDesc() {
        return desc;
    }


}
