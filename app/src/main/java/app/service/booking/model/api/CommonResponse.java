package app.service.booking.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;


    @SerializedName("notification")
    @Expose
    private String notification;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getNotification() {
        return notification;
    }

}
