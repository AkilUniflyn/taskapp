package app.service.booking.model;

public class NotificationModel {

    int image;
    String day,date,request,description;

    public NotificationModel(int image,String day,String date,String request,String desc) {
        this.image = image;
        this.day=day;
        this.date=date;
        this.request=request;
        this.description=desc;
    }

    public int getImage() {
        return image;
    }
    public String getDay() {
        return day;
    }

    public String getDate() {
        return date;
    }
    public String getRequest() {
        return request;
    }

    public String getDescription() {
        return description;
    }
}
