package app.service.booking.model;

public class MyTaskModel {
    String servicetype;
    String address;
    String date;
    String amount;
    String offer;
    String name;

    public MyTaskModel(String servicetype, String address, String date, String amount, String offer) {
        this.servicetype = servicetype;
        this.address = address;
        this.date = date;
        this.amount = amount;
        this.offer = offer;

    }

    public String getServicetype() {
        return servicetype;
    }

    public void setServicetype(String servicetype) {
        this.servicetype = servicetype;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getName() {
        return name;
    }
}
