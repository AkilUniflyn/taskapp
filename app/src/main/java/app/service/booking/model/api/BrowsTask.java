package app.service.booking.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BrowsTask {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<Datum> getData() {
        return data;
    }

    public class Datum {

        @SerializedName("task_detail")
        @Expose
        private TaskDetail taskDetail;
        @SerializedName("user_detail")
        @Expose
        private UserDetail userDetail;
        @SerializedName("offers")
        @Expose
        private List<Offer> offers = null;
        @SerializedName("questions")
        @Expose
        private List<Question> questions = null;

        public TaskDetail getTaskDetail() {
            return taskDetail;
        }

        public UserDetail getUserDetail() {
            return userDetail;
        }

        public List<Offer> getOffers() {
            return offers;
        }

        public List<Question> getQuestions() {
            return questions;
        }


        public class TaskDetail {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("user_id")
            @Expose
            private String userId;
            @SerializedName("service_id")
            @Expose
            private String serviceId;
            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("description")
            @Expose
            private String description;
            @SerializedName("media_id")
            @Expose
            private String mediaId;
            @SerializedName("must_haves")
            @Expose
            private String mustHaves;
            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("location")
            @Expose
            private String location;
            @SerializedName("lat")
            @Expose
            private String lat;
            @SerializedName("lon")
            @Expose
            private String lon;
            @SerializedName("date")
            @Expose
            private String date;
            @SerializedName("need_in_time")
            @Expose
            private String needInTime;
            @SerializedName("start_time")
            @Expose
            private String startTime;
            @SerializedName("end_time")
            @Expose
            private String endTime;
            @SerializedName("budget_type")
            @Expose
            private String budgetType;
            @SerializedName("amount")
            @Expose
            private String amount;
            @SerializedName("hour")
            @Expose
            private String hour;
            @SerializedName("total_amount")
            @Expose
            private String totalAmount;
            @SerializedName("post_status")
            @Expose
            private String postStatus;
            @SerializedName("offers")
            @Expose
            private String offers;
            @SerializedName("date_added")
            @Expose
            private String dateAdded;

            public String getId() {
                return id;
            }

            public String getUserId() {
                return userId;
            }

            public String getServiceId() {
                return serviceId;
            }

            public String getTitle() {
                return title;
            }

            public String getDescription() {
                return description;
            }

            public String getMediaId() {
                return mediaId;
            }

            public String getMustHaves() {
                return mustHaves;
            }

            public String getType() {
                return type;
            }

            public String getLocation() {
                return location;
            }

            public String getLat() {
                return lat;
            }

            public String getLon() {
                return lon;
            }

            public String getDate() {
                return date;
            }

            public String getNeedInTime() {
                return needInTime;
            }

            public String getStartTime() {
                return startTime;
            }

            public String getEndTime() {
                return endTime;
            }

            public String getBudgetType() {
                return budgetType;
            }

            public String getAmount() {
                return amount;
            }

            public String getHour() {
                return hour;
            }

            public String getTotalAmount() {
                return totalAmount;
            }

            public String getPostStatus() {
                return postStatus;
            }

            public String getOffers() {
                return offers;
            }

            public String getDateAdded() {
                return dateAdded;
            }
        }

        public class UserDetail {

            @SerializedName("user_id")
            @Expose
            private String userId;
            @SerializedName("first_name")
            @Expose
            private String firstName;
            @SerializedName("last_name")
            @Expose
            private String lastName;
            @SerializedName("email")
            @Expose
            private String email;
            @SerializedName("mobile_number")
            @Expose
            private String mobileNumber;
            @SerializedName("profie")
            @Expose
            private String profie;
            @SerializedName("region")
            @Expose
            private String region;
            @SerializedName("profile_status")
            @Expose
            private String profileStatus;
            @SerializedName("rating")
            @Expose
            private String rating;
            @SerializedName("distance")
            @Expose
            private String distance;

            public String getUserId() {
                return userId;
            }

            public String getFirstName() {
                return firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public String getEmail() {
                return email;
            }

            public String getMobileNumber() {
                return mobileNumber;
            }

            public String getProfie() {
                return profie;
            }

            public String getRegion() {
                return region;
            }

            public String getProfileStatus() {
                return profileStatus;
            }

            public String getRating() {
                return rating;
            }

            public String getDistance() {
                return distance;
            }
        }



        public class Question {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("user_id")
            @Expose
            private String userId;
            @SerializedName("question")
            @Expose
            private String question;
            @SerializedName("date_added")
            @Expose
            private String dateAdded;
            @SerializedName("reply")
            @Expose
            private String reply;
            @SerializedName("userid")
            @Expose
            private String userid;
            @SerializedName("first_name")
            @Expose
            private String firstName;
            @SerializedName("last_name")
            @Expose
            private String lastName;
            @SerializedName("profile")
            @Expose
            private String profile;
            @SerializedName("location")
            @Expose
            private String location;
            @SerializedName("rating")
            @Expose
            private String rating;
            @SerializedName("joined")
            @Expose
            private String joined;
            @SerializedName("review_tasker")
            @Expose
            private List<Object> reviewTasker = null;
            @SerializedName("review_poster")
            @Expose
            private List<Object> reviewPoster = null;

            public String getId() {
                return id;
            }

            public String getUserId() {
                return userId;
            }

            public String getQuestion() {
                return question;
            }

            public String getDateAdded() {
                return dateAdded;
            }

            public String getReply() {
                return reply;
            }

            public String getUserid() {
                return userid;
            }

            public String getFirstName() {
                return firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public String getProfile() {
                return profile;
            }

            public String getLocation() {
                return location;
            }

            public String getRating() {
                return rating;
            }

            public String getJoined() {
                return joined;
            }

            public List<Object> getReviewTasker() {
                return reviewTasker;
            }

            public List<Object> getReviewPoster() {
                return reviewPoster;
            }
        }

        public class Offer {

            @SerializedName("offer_id")
            @Expose
            private String offerId;
            @SerializedName("amount")
            @Expose
            private String amount;
            @SerializedName("description")
            @Expose
            private String description;
            @SerializedName("date_added")
            @Expose
            private String dateAdded;
            @SerializedName("replys")
            @Expose
            private String replys;
            @SerializedName("userid")
            @Expose
            private String userid;
            @SerializedName("first_name")
            @Expose
            private String firstName;
            @SerializedName("last_name")
            @Expose
            private String lastName;
            @SerializedName("profile")
            @Expose
            private String profile;
            @SerializedName("location")
            @Expose
            private String location;
            @SerializedName("rating")
            @Expose
            private String rating;
            @SerializedName("joined")
            @Expose
            private String joined;
            @SerializedName("review_tasker")
            @Expose
            private List<Object> reviewTasker = null;
            @SerializedName("review_poster")
            @Expose
            private List<Object> reviewPoster = null;

            public String getOfferId() {
                return offerId;
            }

            public String getAmount() {
                return amount;
            }

            public String getDescription() {
                return description;
            }

            public String getDateAdded() {
                return dateAdded;
            }

            public String getReplys() {
                return replys;
            }

            public String getUserid() {
                return userid;
            }

            public String getFirstName() {
                return firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public String getProfile() {
                return profile;
            }

            public String getLocation() {
                return location;
            }

            public String getRating() {
                return rating;
            }

            public String getJoined() {
                return joined;
            }

            public List<Object> getReviewTasker() {
                return reviewTasker;
            }

            public List<Object> getReviewPoster() {
                return reviewPoster;
            }
        }
        }

}
