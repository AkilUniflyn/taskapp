package app.service.booking.model;

public class Message {
    String name;
    String vechname;
    int image;
    String count;

    public Message(String name, String vechname,int image,String count) {
        this.name = name;
        this.vechname = vechname;
        this.image=image;
        this.count=count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVechname() {
        return vechname;
    }

    public void setVechname(String vechname) {
        this.vechname = vechname;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
